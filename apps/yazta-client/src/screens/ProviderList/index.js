import React from 'react';
import { Text } from 'react-native';
import Container from '../../../../libs/ui/src/component/container';
import Candidates from '../../components/candidates';
import styles from './styles';

const ProviderList = (props) => {
  return (
    <Container
      scrollEnabled
      showHeader
      label="Search Results"
      showMenu
      showBack
      extraStyles={styles.container}
    >
      <Text style={styles.header}>Best matches for your needs</Text>
      <Candidates data={props.route.params} />
    </Container>
  );
};

export default ProviderList;
