import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  ratioWidth,
  fonts,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
    paddingHorizontal: 20 * ratioHeight,
  },
  buttonStyle: {
    paddingHorizontal: 10 * ratioWidth,
    paddingVertical: 8 * ratioHeight,
    marginHorizontal: 20 * ratioWidth,
    marginTop: 22 * ratioHeight,
    borderRadius: 50,
  },
  header: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font17,
    marginBottom: 10 * ratioHeight,
    marginTop: 5 * ratioHeight,
  },
});

export default styles;
