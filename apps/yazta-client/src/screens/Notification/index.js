import React from 'react';
import { useNavigation } from '@react-navigation/core';
import Container from '../../../../libs/ui/src/component/container';

import Notification from '../../components/notification';

const NotificationScreen = () => {
  const navigation = useNavigation();
  return (
    <Container scrollEnabled showHeader label="Notifications" showMenu>
      <Notification />
    </Container>
  );
};

export default NotificationScreen;
