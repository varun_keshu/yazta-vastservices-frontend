import React from 'react';
import Signup from '../../../../libs/ui/src/screens/Signup';

const Register = (props) => {
  return <Signup userRole="CUSTOMER" {...props} />;
};

export default Register;
