import React from 'react';
import { Text } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import Container from '../../../../libs/ui/src/component/container';
import CandidateDetail from '../../components/candidateDetail';
import styles from './styles';

const ProviderDetail = () => {
  const navigation = useNavigation();

  return (
    <Container
      scrollEnabled
      showHeader
      label="Submit Offer"
      showMenu
      showBack
      extraStyles={styles.container}
    >
      <Text style={styles.header}>Best match for your needs</Text>
      <CandidateDetail />
      <ButtonPrimary
        label="Make An Offer"
        extraStyle={styles.buttonStyle}
        onPress={() => navigation.navigate('Arrangement')}
      />
    </Container>
  );
};

export default ProviderDetail;
