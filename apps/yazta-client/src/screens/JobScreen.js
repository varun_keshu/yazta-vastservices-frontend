import React from 'react';
import { Text, StyleSheet } from 'react-native';
import Container from '../../../../../libs/ui/src/component/container';
import { colors, ratioHeight } from '../../../../../libs/ui/src/themes';

const JobScreen = () => {
  return (
    <Container
      scrollEnabled
      showHeader
      label="Jobs"
      showBack
      showMenu
      role="CUSTOMER"
      extraStyles={styles.container}
    >
      {/* <Text>Job Screen</Text> */}
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
    paddingHorizontal: 20 * ratioHeight,
  },
});

export default JobScreen;
