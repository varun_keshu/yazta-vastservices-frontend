import React from 'react';
import Login from '../../../../libs/ui/src/screens/Login';

const LoginScreen = (props) => {
  return <Login userRole="CUSTOMER" {...props} />;
};

export default LoginScreen;
