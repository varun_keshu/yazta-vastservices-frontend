import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  ratioWidth,
  fonts,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
    paddingHorizontal: 20 * ratioHeight,
  },
  buttonStyle: {
    paddingHorizontal: 10 * ratioWidth,
    paddingVertical: 8 * ratioHeight,
    marginHorizontal: 20 * ratioWidth,
    marginTop: 22 * ratioHeight,
    borderRadius: 50,
  },
  chatStyle: {
    backgroundColor: colors.pagination,
    borderRadius: 50,
    marginHorizontal: 20 * ratioWidth,
    paddingVertical: 8 * ratioHeight,
    paddingHorizontal: 10 * ratioWidth,
    marginTop: 22 * ratioHeight,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

export default styles;
