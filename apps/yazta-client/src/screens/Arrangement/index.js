import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { useSelector } from 'react-redux';
import Container from '../../../../../libs/ui/src/component/container';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import Commute from '../../components/commute';
import LiveIn from '../../components/liveIn';
import Recurring from '../Recurring';
import styles from './styles';

const Arrangement = () => {
  const arrangement = useSelector((state) => state.booking.arrangement);
  const recurring = useSelector((state) => state.booking.recurring);
  const navigation = useNavigation();

  return (
    <Container
      scrollEnabled
      showHeader
      label="Submit Offer"
      showMenu
      showBack
      extraStyles={styles.container}
    >
      {arrangement === 'COMMUTE' && recurring === 'ONE OFF' ? (
        <Commute />
      ) : arrangement === 'LIVE-IN' ? (
        <LiveIn />
      ) : arrangement === 'COMMUTE' && recurring === 'RECURRING' ? (
        <>
          <Commute />
          {/* <Recurring /> */}
        </>
      ) : (
        <>
          <Commute />
          <LiveIn />
        </>
      )}
      <ButtonPrimary
        label="Continue"
        extraStyle={styles.buttonStyle}
        onPress={() => navigation.navigate('OfferDetail')}
      />
      <ButtonPrimary
        label="Chat for more details"
        extraStyle={styles.chatStyle}
        showRightIcon
        iconType="MaterialCommunityIcons"
        iconName="message-text"
        iconSize={25}
      />
    </Container>
  );
};

export default Arrangement;
