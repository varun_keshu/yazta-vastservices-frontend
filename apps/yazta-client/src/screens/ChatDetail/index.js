import React from 'react';
import { SectionList, Text, View,ScrollView } from 'react-native';
import Header from '../../../../../libs/ui/src/component/header';
import ChatContainer from '../../../../../libs/ui/src/component/ChatContainer';
import MessagerInput from '../../../../../libs/ui/src/component/MessagerInput';
import {images } from '../../../../../libs/ui/src/themes';
import styles from './styles';


const sections = [
  {
    title: 'Monday, March 29', data: [
      { image: `${images.cleaning}`, time: '2:45 AM', lastMessage: 'see you', userType: 1 },
      { image: `${images.cleaning}`, time: '1:20 AM', lastMessage: 'Thangsrhysrhsdh k you Thangsrhysrhsdh k you Thangsrhysrhsdh k you Thangsrhysrhsdh k you Thangsrhysrhsdh k you Thangsrhysrhsdh k you', userType: 0 },
      { image: `${images.cleaning}`, time: '2:45 AM', lastMessage: 'see you', userType: 1 },
    ]
  },
  {
    title: 'Monday, March 28', data: [
      { image: `${images.cleaning}`, time: '2:45 AM', lastMessage: 'see you', userType: 1 },
      { image: `${images.cleaning}`, time: '1:20 AM', lastMessage: 'Thank you', userType: 0 },
      { image: `${images.cleaning}`, time: '2:45 AM', lastMessage: 'see you', userType: 1 },
    ]
  },
  {
    title: 'Monday, March 29', data: [
      { image: `${images.cleaning}`, time: '2:45 AM', lastMessage: 'see you', userType: 1 },
      { image: `${images.cleaning}`, time: '1:20 AM', lastMessage: 'Thangsrhysrhsdh k you', userType: 0 },
      { image: `${images.cleaning}`, time: '2:45 AM', lastMessage: 'see you', userType: 1 },
    ]
  },
  {
    title: 'Monday, March 28', data: [
      { image: `${images.cleaning}`, time: '2:45 AM', lastMessage: 'see you', userType: 1 },
      { image: `${images.cleaning}`, time: '1:20 AM', lastMessage: 'Thank you', userType: 0 },
      { image: `${images.cleaning}`, time: '2:45 AM', lastMessage: 'see you', userType: 1 },
    ]
  },
]
const ChatDetail = ({ navigation }) => {

  const [message, setMessage] = React.useState('');


  function _renderSwipebleList(item, index) {

    return (
      <ChatContainer
        image={item.image}
        description={item.lastMessage}
        time={item.time}
        userType={item.userType}

      />
    );
  }
  return (
    <View style={styles.flex1}>
      <Header
        label={'Chat'}
        showBack={false}
        showMenu={true}

      />
      <ScrollView
        scrollEnabled
      >
        <SectionList
          sections={sections}
          keyExtractor={(item, index) => item + index}
          renderItem={({ item, index }) => _renderSwipebleList(item, index)}
          renderSectionHeader={({ section: { title } }) => (<Text style={styles.header}>{title}</Text>)}
        />

      </ScrollView>
      <MessagerInput
        value={message}
        multiline={true}
        placeholder={'Type Message Here'}
        onChange={(value) => { setMessage(value) }}
        onPress={() => { setMessage('') }}
      />
    </View>
  );
};

export default ChatDetail;
