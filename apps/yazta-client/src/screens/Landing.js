import React from 'react';
import Walkthrough from '../../../../libs/ui/src/screens/Walkthrough';
import { Walk1, Walk2, Walk3 } from '../../../../libs/ui/src/themes/svgs';

const data = [
  {
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna',
    image: <Walk1 height="80%" width="100%" />,
  },
  {
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna',
    image: <Walk2 height="80%" width="100%" />,
  },
  {
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna',
    image: <Walk3 height="80%" width="100%" />,
  },
];

const Landing = () => {
  return <Walkthrough data={data} skip="Register" />;
};

export default Landing;
