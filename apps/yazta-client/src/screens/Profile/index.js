import React from 'react';
import Container from '../../../../../libs/ui/src/component/container';
import styles from './styles';
import ProfileFooter from '../../components/profile/profileFooter';

const Profile = () => {
  return (
    <Container
      scrollEnabled
      showHeader
      label="Profile"
      showBack
      showMenu
      role="CUSTOMER"
      extraStyles={styles.container}
    >
      <ProfileFooter />
    </Container>
  );
};

export default Profile;
