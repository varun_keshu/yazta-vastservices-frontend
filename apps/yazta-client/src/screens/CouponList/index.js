import React from 'react';
import Container from '../../../../libs/ui/src/component/container';
import CL from '../../components/CouponList';
import styles from './styles';

const CouponList = () => {
  return (
    <Container
      scrollEnabled
      showHeader
      label="Apply Coupons"
      showMenu
      extraStyles={styles.container}
    >
      <CL />
    </Container>
  );
};

export default CouponList;
