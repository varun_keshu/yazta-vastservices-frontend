import React from 'react';
import Container from '../../../../libs/ui/src/component/container';
import FavoriteList from '../../../../libs/ui/src/screens/FavoriteList';

const FavoriteProvider = () => {
  return (
    <Container showHeader showBack showMenu label="Favorite Providers">
      <FavoriteList role="CUSTOMER" />
    </Container>
  );
};

export default FavoriteProvider;
