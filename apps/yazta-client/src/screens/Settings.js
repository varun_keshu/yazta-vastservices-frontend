import React from 'react';
import Listing from '../../../../libs/ui/src/helper/listing';

const list = [
  { id: 1, label: 'General', route: 'General' },
  { id: 2, label: 'Notifications', route: 'General' },
  { id: 3, label: 'Privacy', route: 'Privacy' },
  { id: 4, label: 'Blocked Providers', route: 'General' },
  { id: 5, label: 'Help', route: 'Help' },
];

const Settings = () => {
  return (
    <Listing
      label="Settings"
      listData={list}
      iconType="Ionicons"
      iconName="play"
    />
  );
};

export default Settings;
