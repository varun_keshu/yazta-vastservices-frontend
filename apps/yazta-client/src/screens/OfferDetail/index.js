import React from 'react';
import { useNavigation } from '@react-navigation/core';
import Container from '../../../../libs/ui/src/component/container';
import OfferDetail from '../../components/OfferDetail';

import styles from './styles';

const OfferDetailScreen = () => {
  const navigation = useNavigation();
  return (
    <Container
      scrollEnabled
      showHeader
      label="Offer Details"
      showMenu
      showBack
      extraStyles={styles.container}
    >
      <OfferDetail />
    </Container>
  );
};

export default OfferDetailScreen;
