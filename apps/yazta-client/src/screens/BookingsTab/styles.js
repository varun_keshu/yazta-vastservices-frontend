import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  ratioWidth,
  fonts,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
    paddingHorizontal: 20 * ratioHeight,
  },
  titleBox: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'

  },
  titleTxt: {
    fontSize: fonts.size.font16,
    fontFamily: fonts.type.montserratMedium,
    color: colors.black,
  },
  icon: {
    paddingLeft: 10 * ratioHeight,
    alignSelf: 'flex-end',

  },
  card: {
    backgroundColor: colors.white,
    borderRadius: 10,
    marginVertical: 12 * ratioHeight,

  },
  cardContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomWidth: 0.25,
    marginHorizontal: 15 * ratioWidth,
    borderBottomColor: colors.grey,
  },
  lastCard: {
    borderBottomWidth: 0,

  },
  cardTxt: {
    fontSize: fonts.size.font14,
    fontFamily: fonts.type.montserratMedium,
    color: colors.grey,
    paddingVertical: 10,
    // paddingHorizontal:10,

  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 0.50,
    borderBottomColor: colors.grey,
    paddingVertical: 10 * ratioHeight,
    marginHorizontal: 10 * ratioHeight,
  },
  row2: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  staricon: {
    marginRight: 5 * ratioWidth,
  },
  label: {
    fontSize: fonts.size.font11,
    fontFamily: fonts.type.montserratMedium,
    color: colors.grey,
  },
  valueText: {
    fontSize: fonts.size.font11,
    fontFamily: fonts.type.montserratMedium,
    color: colors.black,
  },
  card2: {
    backgroundColor: colors.white,
    borderRadius: 20,
    marginBottom: 20 * ratioHeight,
    marginHorizontal: 1 * ratioHeight,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  playicon:{
    backgroundColor:colors.textColor,
    width:30*ratioWidth,
    height:30*ratioWidth,
    borderRadius:50,
    marginVertical:5*ratioHeight
  },
  card3:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 0.50,
    borderBottomColor: colors.grey,
    paddingVertical: 10 * ratioHeight,
    marginVertical: 10 * ratioHeight,
    marginHorizontal: 1 * ratioHeight,
  paddingHorizontal:10*ratioHeight,
    backgroundColor: colors.white,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 2,
    borderWidth:1,
    borderColor:colors.pagination
  },
  upcomming:{

  },
  upcomming2:{
// textAlign:'center',
alignItems:'center'
  },
name:{

  fontSize: fonts.size.font12,
  fontFamily: fonts.type.montserratMedium,
  color: colors.black,
  lineHeight:fonts.size.font15,

},
timeTxt:{

  fontSize: fonts.size.font11,
  fontFamily: fonts.type.montserratMedium,
  color: colors.black,
  lineHeight:fonts.size.font15,
},
status:{
  
  fontSize: fonts.size.font11,
  fontFamily: fonts.type.montserratMedium,
  color: colors.black,
  lineHeight:fonts.size.font15,

},
});

export default styles;
