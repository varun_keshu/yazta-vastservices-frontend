import React from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import Container from '../../../../libs/ui/src/component/container';
import { BookingCard } from '../../../../../libs/ui/src/component/BookingCard';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import styles from './styles';
import { colors } from '../../../../../libs/ui/src/themes';

const BookingTypeList = [
  { id: 0, name: 'Past Bookings', isOpen: false },
  { id: 1, name: 'Upcoming Bookings', isOpen: false },
  { id: 2, name: 'Cancelled Bookings', isOpen: false },
  { id: 3, name: 'Pending Bookings', isOpen: false },
];
const pastBookingList = [
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Cleaning', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', rating: '4.5' },
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Cleaning', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', rating: '4.5' },
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Cooking', payRateType: 'Weekly', price: '$20', jobId: 'XYZ145', rating: '4.5' },
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Cleaning', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', rating: '4.5' },
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Cleaning', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', rating: '4.5' },
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Cleaning', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', rating: '4.5' },
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Cleaning', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', rating: '4.1' },
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Babysitting', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', rating: '4.5' },
];
const bookingList = [
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Cleaning', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', },
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Cleaning', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', },
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Cleaning', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', },
  { id: 0, name: 'Adele', date: '19 Aug,2021', serviceName: 'Babysitting', payRateType: 'Hourly', price: '$20', jobId: 'XYZ145', },
];
const upcomingBookingList = [
  { id: 0, name: 'Yalitza Hernandez', status: 'COMMUTE', duration: '1 Week', timeDuration: '08:00-11:30AM', },
  { id: 1, name: 'Yalitza Hernandez', status: 'COMMUTE', duration: '1 Week', timeDuration: '08:00-11:30AM', },
  { id: 2, name: 'Yalitza Hernandez', status: 'COMMUTE', duration: '1 Week', timeDuration: '08:00-11:30AM', },
  { id: 3, name: 'Yalitza Hernandez', status: 'COMMUTE', duration: '1 Week', timeDuration: '08:00-11:30AM', },
  { id: 4, name: 'Yalitza Hernandez', status: 'COMMUTE', duration: '1 Week', timeDuration: '08:00-11:30AM', },
  { id: 5, name: 'Yalitza Hernandez', status: 'COMMUTE', duration: '1 Week', timeDuration: '08:00-11:30AM', },
  { id: 6, name: 'Yalitza Hernandez', status: 'COMMUTE', duration: '1 Week', timeDuration: '08:00-11:30AM', },
  { id: 7, name: 'Yalitza Hernandez', status: 'COMMUTE', duration: '1 Week', timeDuration: '08:00-11:30AM', },

];
const BookingsTab = () => {
  const navigation = useNavigation();
  const [cardList, setCardList] = React.useState(BookingTypeList);
  const [cardList2, setCardList2] = React.useState(pastBookingList);
  const [title, setTitle] = React.useState(' Bookings');
  const [currantTab, setCurrantTab] = React.useState(4);
  const [refresh, setRefresh] = React.useState(false);


  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', (e) => {
      const result = BookingTypeList.filter(value => value.id !== 0);

      let tl = cardList[0]?.name

      setCardList(result)
      setTitle(tl)
    });
    return unsubscribe;
  });
  React.useEffect(() => {

  }, []);
  function onClick(i, j) {
    setCurrantTab(i.id)
    const result = BookingTypeList.filter(value => value.id !== i.id);
    setCardList(result)
    setTitle(i.name)
    if (i.id !== 0) {
      setCardList2(bookingList)
    } else {
      setCardList2(pastBookingList)

    }

  }
  const upcomingBooking = ({ item, index }) => {
    return (
      <View style={styles.card3}>
        <View style={styles.upcomming}>
          <Text style={styles.name} >{item.name}</Text>
          <Text style={styles.timeTxt} >{item.duration}</Text>
          <Text style={styles.timeTxt} >{item.timeDuration}</Text>
        </View>
        <View style={styles.upcomming2}>

          <Text style={styles.status} >{item.status}</Text>
          <Icon type={'FontAwesome'} name={'play'} color={colors.white} extraStyles={styles.playicon} size={12} />


        </View>
      </View>
    )
  }
  const pastBooking = ({ item, index }) => {
    return (
      <View>
        <BookingCard
          nameLabel={'Provider Name'}
          serviceLabel={'Service Name'}
          dateLabel={'Service Date'}
          priceLabel={'Amount'}
          jobidLabel={'Job Id'}
          rateBasisLabel={'Pay Rate Basis'}
          onPress={()=>{}}
          type={1}
          rating={item.rating}
          date={item.date}
          serviceName={item.serviceName}
          payRateType={item.payRateType}
          price={item.price}
          description={item.description}
          jobId={item.jobId}
          name={item.name}
        />

      </View>
    )
  };
  const ItemView = ({ item, index }) => {
    let lenght = cardList.length - 1
    return (
      <>
        {item.id !== currantTab &&
          <TouchableOpacity style={[styles.cardContainer, index === lenght ? styles.lastCard : {}]} onPress={() => { onClick(item) }}>
            <Text style={styles.cardTxt} >{item.name}</Text>
          </TouchableOpacity>}
      </>
    )
  };

  return (
    <Container
      scrollEnabled
      showHeader
      label={title}
      showBack
      showMenu
      role="CUSTOMER"
      extraStyles={styles.container}
    >
      <View style={styles.titleBox} >
        <Text style={styles.titleTxt} >{title}</Text>
        <Icon type={'Feather'} name={'chevron-down'} color={colors.black} extraStyles={styles.icon} size={30} />
      </View>
      <View style={styles.card} >

        <FlatList
          data={cardList || []}
          nestedScrollEnabled
          renderItem={ItemView}
          keyExtractor={(item) => item.id}
          extraData={refresh}
        // contentContainerStyle={styles.contentContainerStyle}
        />
      </View>

      {currantTab !== 1 && (
        <FlatList
          data={cardList2 || []}
          nestedScrollEnabled
          renderItem={pastBooking}
          keyExtractor={(item) => item.id}
          extraData={refresh}
          contentContainerStyle={styles.contentContainerStyle}
        />
      )}
      {currantTab === 1 && (
        <FlatList
          data={upcomingBookingList || []}
          nestedScrollEnabled
          renderItem={upcomingBooking}
          keyExtractor={(item) => item.id}
          extraData={refresh}
          contentContainerStyle={styles.contentContainerStyle}
        />
      )}

    </Container>
  );
};

export default BookingsTab;
