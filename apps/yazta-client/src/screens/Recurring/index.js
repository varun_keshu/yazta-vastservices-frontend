import React from 'react';
import { Text, View } from 'react-native';
import Arrangement from '../../components/recurringArrangement';
import styles from './styles';

const Recurring = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Recurring</Text>
      <Arrangement />
    </View>
  );
};

export default Recurring;
