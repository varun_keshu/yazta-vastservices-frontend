import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  ratioWidth,
  fonts,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
  },
  header: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font18,
    marginBottom: 10 * ratioHeight,
    marginTop: 5 * ratioHeight,
    textAlign: 'center',
  },
});

export default styles;
