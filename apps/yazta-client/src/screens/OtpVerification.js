import React from 'react';
import OtpVerify from '../../../../libs/ui/src/screens/OtpVerify';

const OtpVerification = ({ route }) => {
  const { value, type } = route.params;

  return <OtpVerify value={value} type={type} />;
};

export default OtpVerification;
