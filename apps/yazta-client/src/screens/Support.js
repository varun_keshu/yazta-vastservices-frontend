import React from 'react';
import Listing from '../../../../libs/ui/src/helper/listing';

const list = [
  { id: 1, label: 'FAQ', route: 'Faq' },
  { id: 2, label: 'Terms & Conditions', route: 'TermsConditions' },
  { id: 3, label: 'Send Feedback', route: 'Feedback' },
  { id: 4, label: 'Contact Us', route: 'ContactUs' },
];

const Support = () => {
  return (
    <Listing
      label="Support"
      listData={list}
      iconType="Ionicons"
      iconName="play"
    />
  );
};

export default Support;
