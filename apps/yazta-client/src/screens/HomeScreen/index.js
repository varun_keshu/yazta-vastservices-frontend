import React, { useEffect } from 'react';
import { Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/core';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import Container from '../../../../libs/ui/src/component/container';
import { findProvider } from '../../../../../libs/store/src/client/booking/action';
import Preferences from '../../components/preferences';
import Skills from '../../components/skills';
import styles from './styles';
import asyncStore from '../../../../../libs/store/src/asyncStore';
import { saveClientData } from '../../../../../libs/store/src/client/user/action';
import { getServiceList } from '../../../../../libs/store/src/common/core/action';

const HomeScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getServiceList());
    retrieveData();
  }, []);

  const retrieveData = async () => {
    try {
      const userData = await asyncStore.getClientData('clientData');
      if (userData !== null) {
        dispatch(saveClientData(userData));
      }
    } catch (error) {
      console.log('retrieveData error :', error);
    }
  };

  const arrangement = useSelector((state) => state.booking.arrangement);
  const time = useSelector((state) => state.booking.time);
  const skills = useSelector((state) => state.booking.skills);

  const onSubmit = () => {
    const payload = {
      living_arrangement: arrangement ? arrangement.toUpperCase() : '',
      preferred_time: time ? time.toUpperCase() : '',
      skill: skills ? skills.toUpperCase() : '',
    };
    dispatch(findProvider(payload));
    navigation.navigate('ProviderList');
  };

  return (
    <Container
      role="CUSTOMER"
      scrollEnabled
      showHeader
      label="Find Your Provider"
      showMenu
      extraStyles={styles.container}
      onNotification={() => {
        navigation.navigate('Notification');
      }}
    >
      <Preferences />
      <Skills />
      <ButtonPrimary
        label="Submit your Search"
        extraStyle={styles.buttonStyle}
        onPress={() => onSubmit()}
      />
      <Text style={styles.footer}>You need to fill profile first</Text>
    </Container>
  );
};

export default HomeScreen;
