import React from 'react';
import { Text, View, Image } from 'react-native';
import Container from '../../../../../libs/ui/src/component/container';
import styles from './styles';
import Accordion from '../../../../../libs/ui/src/helper/accordion';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import AccordianIn from '../../../../../libs/ui/src/helper/accordionIn';
import { ratioHeight } from '../../../../../libs/ui/src/themes';

const UploadDoc = () => {
  return (
    <Container
      scrollEnabled
      showHeader
      showBack
      showMenu
      label="Upload Documents"
      extraStyles={styles.container}
    >
      <Accordion label="Upload Photo Id" check={true} />
      <AccordianIn label="Image1" />
      <AccordianIn label="Image2" check />
      <View style={{ marginBottom: 15 * ratioHeight }}></View>
      <Accordion label="Upload References" />
      <Accordion label="Upload Resume" check={true} />
      <Accordion label="Upload Background Check" />
      <ButtonPrimary
        label="Submit"
        extraStyle={styles.buttonStyle}
        onPress={() => alert('Your document was successfully uploaded.')}
      />
    </Container>
  );
};

export default UploadDoc;
