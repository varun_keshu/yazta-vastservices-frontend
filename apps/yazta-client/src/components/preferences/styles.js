import { StyleSheet } from 'react-native';
import { colors, ratioHeight, fonts } from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  header: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font18,
    textAlign: 'center',
    marginBottom: 10 * ratioHeight,
    marginTop: 5 * ratioHeight,
  },
  radioStyle: {
    marginTop: 5 * ratioHeight,
  },
});

export default styles;
