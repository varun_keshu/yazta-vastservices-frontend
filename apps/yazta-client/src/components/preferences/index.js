import React from 'react';
import { View, Text } from 'react-native';
import Radios from '../radios';
import styles from './styles';
import { useSelector } from 'react-redux';

const Preferences = () => {
  const arrangement = useSelector((state) => state.booking.arrangement);

  return (
    <View>
      <Text style={styles.header}>Select your needs below</Text>
      <Radios
        title="Preferred living arrangement"
        arrangement={true}
        option1="LIVE - IN (24/7)"
        option2="COMMUTE"
        option3="EITHER"
        iconName="houzz"
        extraStyle={styles.radioStyle}
      />
      {arrangement === 'LIVE-IN' ? (
        <View></View>
      ) : (
        <>
          <Radios
            title="Preferred Commute Options"
            option1="RECURRING"
            option2="ONE OFF"
            iconName="direction-sign"
            commute={true}
          />
          <Radios
            title="Preferred time of day"
            time={true}
            option1="AM"
            option2="PM"
            option3="EITHER"
            iconName="stopwatch"
          />
        </>
      )}
    </View>
  );
};

export default Preferences;
