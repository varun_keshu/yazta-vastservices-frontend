import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 8 * ratioHeight,
  },
  inputBox: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.white,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.pagination,
    height: 40 * ratioHeight,
  },
  inputField: {
    width: '95%',
    borderBottomWidth: 0,
    color: colors.black,
    marginTop: 5 * ratioHeight,
  },
  button: {
    width: '15%',
  },
  buttonTxt: {
    color: colors.pagination,
    fontSize: fonts.size.font12,
    fontFamily: fonts.type.montserratRegular,
  },
});

export default styles;
