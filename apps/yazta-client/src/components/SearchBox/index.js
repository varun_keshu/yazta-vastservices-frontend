import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import InputField from '../../../../../libs/ui/src/helper/inputs/inputField';

import styles from './styles';

const SearchBox = (props) => {
  const {
    title,
    placeholder,
    value,
    buttonTitle,
    onChange,
    onPress,
    extraStyle,
  } = props;
  return (
    <View style={styles.inputBox}>
      <InputField
        placeholder={placeholder}
        value={value}
        hideField
        onChange={(value) => {
          onChange(value);
        }}
        extraStyle={[styles.inputField, extraStyle]}
      />
      <TouchableOpacity
        onPress={() => {
          onPress();
        }}
        style={styles.button}
      >
        <Text style={styles.buttonTxt}>{buttonTitle}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SearchBox;
