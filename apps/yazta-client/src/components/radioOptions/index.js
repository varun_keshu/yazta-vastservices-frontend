import React, { useState } from 'react';
import { View, Text } from 'react-native';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors } from '../../../../../libs/ui/src/themes';
import { useDispatch } from 'react-redux';
import styles from './styles';
import {
  setArrangement,
  setRecurring,
  setTime,
} from '../../../../../libs/store/src/common/booking/action';

const RadioOptions = (props) => {
  const { option1, option2, option3, arrangement, time } = props;
  const dispatch = useDispatch();

  const [living, setLiving] = useState(false);
  const [jobTime, setJobTime] = useState(false);
  const [recur, setRecur] = useState(false);

  const onRadioPress = (value) => {
    if (time) {
      setJobTime(value);
      dispatch(setTime(value));
    } else if (arrangement) {
      setLiving(value);
      if (value === 'LIVE - IN (24/7)') {
        dispatch(setArrangement('LIVE-IN'));
      } else {
        dispatch(setArrangement(value));
      }
    } else {
      setRecur(value);
      dispatch(setRecurring(value));
    }
  };

  return (
    <View style={styles.subContainer}>
      <View style={styles.innerContainer}>
        <View style={styles.container}>
          <Icon
            type="Fontisto"
            name={
              option1 === jobTime || living === option1 || recur === option1
                ? 'radio-btn-active'
                : 'radio-btn-passive'
            }
            size={16}
            color={colors.pagination}
            onPress={() => onRadioPress(option1)}
          />
          <Text style={styles.options}>{option1}</Text>
        </View>
        <View style={styles.container}>
          <Icon
            type="Fontisto"
            name={
              option2 === jobTime || living === option2 || recur === option2
                ? 'radio-btn-active'
                : 'radio-btn-passive'
            }
            size={16}
            color={colors.pagination}
            onPress={() => onRadioPress(option2)}
          />
          <Text style={styles.options}>{option2}</Text>
        </View>
      </View>
      {option3 && (
        <View style={styles.container}>
          <Icon
            type="Fontisto"
            name={
              option3 === jobTime || living === option3
                ? 'radio-btn-active'
                : 'radio-btn-passive'
            }
            size={16}
            color={colors.pagination}
            onPress={() => onRadioPress(option3)}
          />
          <Text style={styles.options}>{option3}</Text>
        </View>
      )}
    </View>
  );
};

export default RadioOptions;
