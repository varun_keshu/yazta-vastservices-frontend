import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 8 * ratioHeight,
  },
  subContainer: {
    paddingHorizontal: 10 * ratioHeight,
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  options: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font12,
    marginLeft: 8 * ratioWidth,
  },
});

export default styles;
