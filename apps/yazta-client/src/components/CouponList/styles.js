import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  subContainer: {
    paddingHorizontal: 20 * ratioHeight,
    marginTop: 10 * ratioHeight,
  },
  container: {
    borderBottomWidth: 0.5,
    borderColor: colors.pagination,
    borderRadius: 2.5,
  },
  cardContainer: {
    borderBottomColor: colors.pagination,
    borderBottomWidth: 0.5,
    marginVertical: 12 * ratioHeight,
  },
  contentContainerStyle: {
    backgroundColor: colors.detailBackground,
    paddingHorizontal: 20 * ratioWidth,
    paddingVertical: 10 * ratioWidth,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  titleBox: {
    borderColor: colors.pagination,
    borderWidth: 1,
    paddingHorizontal: 10 * ratioHeight,
    paddingVertical: 6 * ratioHeight,
    alignItems: 'flex-start',
    width: '36%',
  },
  titleTxt: {
    color: colors.pagination,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font13,
    textAlign: 'center',
    paddingRight: 10 * ratioWidth,
  },
  titleTxt2: {
    color: '#8e8883',
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font14,
    margin: 20 * ratioHeight,
  },
  priceTxt: {
    color: '#8e8883',
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font12,
    marginVertical: 8 * ratioHeight,
  },
  flatList: {
    paddingVertical: 10,
    backgroundColor: '#dd1',
    marginVertical: 2,
  },
  inputBox: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: colors.pagination,
  },
  inputField: {
    width: '95%',
    borderBottomWidth: 0,
    paddingLeft: 10,
    color: colors.textPrimary,
  },
  button: {
    width: '15%',
    justifyContent: 'center',
  },
  buttonTxt: {
    color: colors.pagination,
    fontSize: fonts.size.font12,
    fontFamily: fonts.type.montserratMedium,
  },
});

export default styles;
