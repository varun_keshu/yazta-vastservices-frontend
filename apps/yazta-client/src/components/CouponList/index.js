import React, { useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import SearchBox from '../SearchBox';
import styles from './styles';

const serviceList = [
  {
    id: 1,
    price: '$10',
    label: 'XYZ100',
  },
  {
    id: 2,
    price: '$10',
    label: 'XYZ100',
  },
  {
    id: 3,
    price: '$10',
    label: 'XYZ100',
  },
  {
    id: 4,
    price: '$10',
    label: 'XYZ100',
  },
];

const CouponList = () => {
  const [serviceData, setServiceData] = useState(serviceList);
  const [refresh, setRefresh] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const navigation = useNavigation();

  const ItemView = ({ item }) => {
    return (
      <View style={styles.cardContainer}>
        <View style={styles.row}>
          <View style={styles.titleBox}>
            <Text style={styles.titleTxt}>{item.label}</Text>
          </View>
          <TouchableOpacity>
            <Text style={styles.titleTxt}>{'Apply'}</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.priceTxt}>{'Get' + item.price}</Text>
      </View>
    );
  };

  return (
    <View style={styles.flex1}>
      <View style={styles.subContainer}>
        <SearchBox
          placeholder={'Enter Coupon Code'}
          value={searchValue}
          hideField
          onChange={(value) => {
            setSearchValue(value);
          }}
          extraStyle={styles.inputField}
          onPress={() => {}}
          buttonTitle={'Apply'}
        />
      </View>

      <Text style={styles.titleTxt2}>{'AVAILABLE COUPONS'}</Text>
      <FlatList
        data={serviceData || []}
        nestedScrollEnabled
        renderItem={ItemView}
        keyExtractor={(item) => item.id}
        extraData={refresh}
        contentContainerStyle={styles.contentContainerStyle}
      />
    </View>
  );
};

export default CouponList;
