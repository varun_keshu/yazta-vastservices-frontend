import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Image } from 'react-native';
import { colors, images } from '../../../../../libs/ui/src/themes';
import styles from './styles';

const Item = [
  {
    id: 1,

    name: 'John',
    date: '24Aug,2021',
    status: 'started',
    job: 'the job',
    time: '9:30AM',
    color: colors.successColor,
  },
  {
    id: 2,
    name: 'Adele',
    date: '24Aug,2021',
    status: 'initiated',
    job: 'the job',
    time: '10:30AM',
    color: colors.initiatedColor,
  },
  {
    id: 3,
    name: 'Adele',
    date: '24Aug,2021',
    status: 'rejected',
    time: '11:00PM',
    color: colors.rejectedColor,
  },
  {
    id: 4,
    name: 'Adele',
    date: '24Aug,2021',
    status: 'completed',
    job: 'the service',
    time: '9:00PM',
    color: colors.successColor,
  },
  {
    id: 5,
    name: 'Adele',
    date: '24Aug,2021',
    status: 'accepted',
    job: 'the job',
    time: '12:00PM',
    color: colors.successColor,
  },
];

const Notification = () => {
  const navigation = useNavigation();

  const renderItem = ({ item }) => {
    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image source={images.person} style={styles.image} />
        </View>

        <View style={styles.topContainer}>
          <Text style={styles.name}>{item.name}</Text>
          <Text style={styles.status}>
            has <Text style={{ color: item.color }}>{item.status}</Text>{' '}
            {item.job}
          </Text>
        </View>
        <View style={styles.middleContainer}>
          <Text style={styles.date}>{item.date}</Text>
          <Text style={styles.time}>{item.time}</Text>
        </View>
      </View>
    );
  };

  return (
    <FlatList
      data={Item}
      keyExtractor={(item) => item.id}
      renderItem={renderItem}
    />
  );
};

export default Notification;
