import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  image: {
    height: 50 * ratioHeight,
    width: 50 * ratioWidth,
    marginLeft: 10 * ratioWidth,
  },
  imageContainer: {
    flex: 3,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 20 * ratioHeight,
    backgroundColor: colors.white,
    elevation: 4,
    borderRadius: 10,
    paddingVertical: 18 * ratioHeight,
    marginHorizontal: 18 * ratioWidth,
  },
  topContainer: {
    flex: 6,
  },
  name: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font14,
  },
  status: {
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
    marginTop: 8 * ratioHeight,
  },
  middleContainer: {
    flex: 3,
  },
  time: {
    textAlign: 'center',
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
    marginTop: 8 * ratioHeight,
  },
  date: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
  },
});

export default styles;
