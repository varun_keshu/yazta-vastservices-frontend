import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors } from '../../../../../libs/ui/src/themes';
import styles from './styles';
import { getProviderDetail } from '../../../../../libs/store/src/client/booking/action';

const Candidates = (props) => {
  const providerList = useSelector((state) => state.clientBooking.providerList);

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const showProviderDetail = (id) => {
    dispatch(getProviderDetail(id));
    navigation.navigate('ProviderDetail');
  };

  const renderList = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.cardContainer}
        onPress={() => showProviderDetail(item.id)}
      >
        <View style={styles.subContainer}>
          <Text style={styles.title}>{item.name}</Text>
          {item.favorite ? (
            <Icon
              name="heart"
              type="AntDesign"
              size={16}
              color={colors.textColor}
              extraStyles={styles.iconStyle}
            />
          ) : (
            <Icon
              name="heart"
              type="Feather"
              size={16}
              color={colors.textColor}
              extraStyles={styles.iconStyle}
            />
          )}
        </View>

        <View style={styles.subContainer}>
          <Text style={styles.skill}>{item.skill}</Text>
          <Icon
            name="play-circle"
            type="Ionicons"
            size={25}
            color={colors.textColor}
            extraStyles={styles.playStyle}
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View>
      {providerList.length ? (
        <FlatList data={providerList} renderItem={renderList} />
      ) : (
        <Text style={styles.no}>No Provider found!</Text>
      )}
    </View>
  );
};

export default Candidates;
