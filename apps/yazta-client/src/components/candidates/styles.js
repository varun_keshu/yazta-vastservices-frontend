import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  subContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cardContainer: {
    borderColor: colors.pagination,
    borderWidth: 0.7,
    borderRadius: 5,
    marginVertical: '1.8%',
    paddingHorizontal: 12,
    paddingVertical: 8 * ratioHeight,
    elevation: 5,
    shadowColor: colors.pagination,
    backgroundColor: colors.appSecondary,
  },
  title: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font15,
    marginBottom: 12 * ratioHeight,
  },
  skill: {
    color: colors.textColor,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
  },
  no: {
    color: colors.textColor,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font15,
    textAlign: 'center',
    marginTop: 120 * ratioHeight,
  },
  iconStyle: {
    marginBottom: 12 * ratioHeight,
  },
  playStyle: {
    marginRight: -4 * ratioHeight,
    marginBottom: -4 * ratioHeight,
  },
});

export default styles;
