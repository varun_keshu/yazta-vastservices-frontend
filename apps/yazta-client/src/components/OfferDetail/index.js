import React, { useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, Modal } from 'react-native';
import { colors, images } from '../../../../../libs/ui/src/themes';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import { Dropdown } from '../../../../../libs/ui/src/component/Dropdown';
import { GeneralPopUp } from '../../../../../libs/ui/src/component/GeneralPopUp';
import { useNavigation } from '@react-navigation/core';
import styles from './styles';
import TextInputBox from '../../helper/inputBox';

const serviceList = [
  {
    id: 1,
    image: `${images.cleaning}`,
    title: 'Name',
    label: 'Yalitza Hernandez',
    checked: false,
  },
  {
    id: 2,
    image: `${images.babysit}`,
    title: 'AGE',
    label: '22',
    checked: false,
  },
  {
    id: 3,
    image: `${images.cooking}`,
    title: 'EXPERIENCE',
    label: '7 Years of Cleaning/Cooking/Babysitting',
    checked: false,
  },
  {
    id: 4,
    image: `${images.service}`,
    title: 'GENDER',
    label: 'Male',
    checked: false,
  },
  {
    id: 5,
    image: `${images.service}`,
    title: 'SERVICE',
    label: 'LIVE-IN',
    checked: false,
  },
  {
    id: 6,
    image: `${images.service}`,
    title: 'EDUCATION',
    label: 'High School Diploma',
    checked: false,
  },
];

const OfferDetail = () => {
  const navigation = useNavigation();

  const [serviceData, setServiceData] = useState(serviceList);
  const [refresh, setRefresh] = useState(false);
  const [modalVisible, setmodalVisible] = useState(false);
  const [confirmVisible, setConfirmVisible] = useState(false);
  const [offerVisible, setOfferVisible] = useState(false);
  const [offerCancelVisible, setOfferCancelVisible] = useState(false);
  const [reason, setReason] = useState(null);

  const [reasonList, setReasonList] = useState([
    { value: 1, label: 'Reason 1' },
    { value: 2, label: 'Reason 2' },
    { value: 3, label: 'Reason 3' },
    { value: 4, label: 'Reason 4' },
    { value: 5, label: 'Reason 5' },
    { value: 6, label: 'Reason 6' },
    { value: 7, label: 'Reason 7' },
  ]);

  const ItemView = ({ item }) => {
    return (
      <>
        {item.id === 1 && (
          <Icon
            type={'AntDesign'}
            name={'user'}
            extraStyles={styles.iconBox}
            size={30}
          />
        )}
        <View style={styles.listRow}>
          <Text style={styles.titleTxt}>{item.title}</Text>
          <Text style={styles.lableTxt}>{item.label}</Text>
        </View>
      </>
    );
  };

  const ReasonView = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.resonRow}
        onPress={() => {
          setReason(item.name);
        }}
      >
        <Text style={styles.reasonTxt}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={
        offerVisible || offerCancelVisible || modalVisible || confirmVisible
          ? styles.flex
          : styles.flex1
      }
    >
      <View style={styles.container}>
        <View style={styles.titleBox}>
          <Text style={styles.title}>{'Date And Length Of Booking'}</Text>
        </View>
        <View style={styles.cardContainer}>
          <Icon
            type={'AntDesign'}
            name={'calendar'}
            extraStyles={styles.iconBox}
            size={30}
          />
          <Text style={styles.infoTxt2}>{'Monday'}</Text>
          <Text style={styles.infoTxt2}>{'March 21st, 2021'}</Text>
          <Text style={[styles.infoTxt2, styles.infoTxt3]}>{'2 weeks'}</Text>
        </View>
        <View style={styles.titleBox2}>
          <Text style={styles.title}>{'Personal Details'}</Text>
        </View>
        <FlatList
          data={serviceData || []}
          nestedScrollEnabled
          renderItem={ItemView}
          keyExtractor={(item) => item.id}
          extraData={refresh}
          contentContainerStyle={styles.contentContainerStyle}
        />

        <View style={styles.titleBox2}>
          <Text style={styles.title}>{'Rate And Length Of Engagement'}</Text>
        </View>

        <View style={styles.infoBox}>
          <View style={styles.rateBox}>
            <Text style={styles.subTitle}>{'AGREED UPON RATE'}</Text>
            <Text style={styles.infoTxt}>{'$2000/Weeks'}</Text>
          </View>
          <View style={styles.rateBox}>
            <Text style={styles.subTitle}>{'Length Of Engagement'}</Text>
            <Text style={styles.infoTxt}>{'2 Weeks'}</Text>
          </View>
          <View style={styles.rateBox}>
            <Text style={styles.subTitle}>{'TOTAL PAYOUT'}</Text>
            <Text style={styles.infoTxt}>{'$2000'}</Text>
          </View>
        </View>
      </View>

      <ButtonPrimary
        label="Apply Coupon"
        extraStyle={styles.coupon}
        labelStyle={styles.couponLabelStyle}
        showIcon
        iconColor={colors.black}
        iconType="Feather"
        iconName="chevron-right"
        iconSize={25}
        onPress={() => {
          navigation.navigate('CouponList');
        }}
      />

      <View style={styles.summaryContainer}>
        <Text style={styles.summaryTitleTxt}>{'Summary'}</Text>
        <View style={styles.row}>
          <Text style={styles.summaryInfoTxt}>{'Total Fee'}</Text>
          <Text style={styles.summaryInfoTxt}>{'$4000'}</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.summaryInfoTxt}>{'Commission'}</Text>
          <Text style={styles.summaryInfoTxt}>{'$100'}</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.discountTxt}>{'Coupon Discount'}</Text>
          <Text style={styles.discountTxt}>{'$100'}</Text>
        </View>
        <View style={[styles.row, styles.lastRow]}>
          <Text style={styles.summaryInfoTxt}>{'Total Payment'}</Text>
          <Text style={styles.summaryInfoTxt}>{'$4100'}</Text>
        </View>
      </View>

      <View style={styles.bottomContainer}>
        <ButtonPrimary
          label="Confirm Offer"
          extraStyle={styles.acceptStyle}
          labelStyle={styles.chatLabelStyle}
          onPress={() => {
            setOfferVisible(true);
          }}
        />
        <ButtonPrimary
          label="Modify Offer"
          extraStyle={styles.declineStyle}
          labelStyle={styles.modifyLabelStyle}
        />
        {/* <ButtonPrimary
          label="Cancel Offer"
          extraStyle={styles.declineStyle}
          onPress={() => {
            setOfferCancelVisible(true);
          }}
          labelStyle={styles.chatLabelStyle}
        /> */}
      </View>
      <ButtonPrimary
        label="Chat for more details"
        extraStyle={styles.dropDwnBox}
        labelStyle={styles.chatLabelStyle}
        showRightIcon
        iconType="MaterialCommunityIcons"
        iconName="message-text"
        iconSize={25}
        onPress={() => {
          navigation.navigate('ChatList');
        }}
      />

      <Modal
        animationOutTiming={700}
        animationInTiming={700}
        animationIn="slideInUp"
        animationOut="slideOutDown"
        transparent={true}
        visible={modalVisible}
        // onBackdropPress={() => { setmodalVisible(false) }}
        onRequestClose={() => {
          setmodalVisible(false);
        }}
        style={styles.modalMain}
      >
        <View style={styles.modal}>
          <View style={styles.modalContainer}>
            <Text style={styles.title2}>{'Reason for Cancellation'}</Text>
            <Dropdown
              setShowList={() => {}}
              selectedValue={reason}
              data={reasonList}
              placeholder={'Select'}
              value={reason}
              isShowList={true}
              inputContainerStyle={styles.dropDownView}
              // placeholderStyle={styles.reasonTxt2}
              // iconStyle={styles.dropdownIcon}
              // listStyle={styles.listView}
              onChangeText={(item) => {
                setReason(item);
              }}
            />
            <TextInputBox
              placeholder="Type Your Reason"
              extraStyle={styles.inputContainer}
            />
            <ButtonPrimary
              label="SUBMIT"
              extraStyle={styles.sumitStyle}
              onPress={() => {
                setmodalVisible(false);
              }}
              labelStyle={styles.chatLabelStyle}
            />
          </View>
        </View>
      </Modal>
      <GeneralPopUp
        isVisible={offerCancelVisible}
        onRequestClose={() => {
          setOfferCancelVisible(false);
        }}
        OnRightPress={() => {
          setOfferCancelVisible(false);
        }}
        OnLeftPress={() => {
          setOfferCancelVisible(false), setmodalVisible(true);
        }}
        rightButtonTxt={'No'}
        leftButtonTxt={'Yes'}
        description={
          'Your offer is binding upon acceptance. Are you sure you want to proceed?'
        }
      />
      <GeneralPopUp
        isVisible={offerVisible}
        onRequestClose={() => {
          setOfferVisible(false);
        }}
        OnRightPress={() => {
          setOfferVisible(false);
        }}
        OnLeftPress={() => {
          setOfferVisible(false), setConfirmVisible(true);
        }}
        rightButtonTxt={'No'}
        leftButtonTxt={'Yes'}
        description={
          'Your offer is binding upon acceptance. Are you sure you want to proceed?'
        }
      />

      <GeneralPopUp
        isVisible={confirmVisible}
        onRequestClose={() => {
          setConfirmVisible(false);
        }}
        OnLeftPress={() => {
          navigation.navigate('HomeScreen'), setConfirmVisible(false);
        }}
        leftButtonTxt={'Tap here to go back to your Home Screen'}
        description={'Your offer has been sent'}
        yesStyle={styles.yesStyle}
        desc={styles.desc}
        showIcon
      />
    </View>
  );
};

export default OfferDetail;
