import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors } from '../../../../../libs/ui/src/themes';
import styles from './styles';

const Section = (props) => {
  const { iconType, iconName, label } = props;
  return (
    <TouchableOpacity style={styles.section}>
      <View style={styles.row}>
        <Icon
          type={iconType}
          name={iconName}
          size={16}
          color={colors.pagination}
        />
        <Text style={styles.sectionTitle}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Section;
