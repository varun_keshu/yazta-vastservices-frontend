import { StyleSheet } from 'react-native';
import {
  colors,
  fonts,
  ratioHeight,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: { flexDirection: 'row' },
  drawerContent: {
    flexDirection: 'row',
    marginTop: 15 * ratioHeight,
    width: '90%',
    alignItems: 'center',
    paddingLeft: 20 * ratioWidth,
  },
  avatar: {
    height: 70 * ratioWidth,
    width: 70 * ratioWidth,
    borderRadius: 50,
  },
  title: {
    fontSize: fonts.size.font16,
    fontFamily: fonts.type.montserratBold,
    paddingHorizontal: 10 * ratioWidth,
    color: colors.textPrimary,
    width: '60%',
    marginLeft: 15 * ratioWidth,
    textAlign: 'center',
    lineHeight: 20 * ratioHeight,
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 0.5,
    borderColor: colors.pagination,
    borderRadius: 4,
    paddingVertical: 10 * ratioHeight,
    paddingHorizontal: 12 * ratioWidth,
    marginHorizontal: 10 * ratioHeight,
    marginTop: 22 * ratioHeight,
    backgroundColor: '#FEFBF8',
  },
  sectionTitle: {
    fontSize: fonts.size.font12,
    fontFamily: fonts.type.montserratBold,
    color: colors.appDark,
    marginLeft: 10 * ratioWidth,
  },
  drawerLabel: {
    color: colors.appDark,
    fontSize: fonts.size.font15,
    fontFamily: fonts.type.montserratMedium,
    marginLeft: -10 * ratioWidth,
  },
  subContainer: {
    marginTop: 16 * ratioHeight,
  },
});

export default styles;
