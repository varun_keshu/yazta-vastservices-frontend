export default [
  {
    name: 'My Profile',
    iconType: 'FontAwesome5',
    iconName: 'user-tie',
    route: 'Home',
  },
  {
    name: 'My Appointment',
    iconType: 'FontAwesome5',
    iconName: 'calendar-alt',
    route: 'Home',
  },
  {
    name: 'My Provider',
    iconType: 'FontAwesome5',
    iconName: 'user-cog',
    route: 'Home',
  },
  {
    name: 'Payment Method',
    iconType: 'FontAwesome5',
    iconName: 'wallet',
    route: 'Home',
  },
  {
    name: 'Support',
    iconType: 'MaterialCommunityIcons',
    iconName: 'face-agent',
    route: 'Support',
  },
  {
    name: 'Settings',
    iconType: 'Ionicons',
    iconName: 'settings-sharp',
    route: 'Settings',
  },
  {
    name: 'Favorite Providers',
    iconType: 'FontAwesome',
    iconName: 'heart',
    route: 'FavoriteProvider',
  },
  {
    name: 'Logout',
    iconType: 'FontAwesome5',
    iconName: 'power-off',
    route: 'Logout',
  },
];
