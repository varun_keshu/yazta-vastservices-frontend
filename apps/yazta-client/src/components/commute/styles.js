import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  header: {
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font18,
    color: colors.textPrimary,
    marginBottom: 10 * ratioHeight,
    textAlign: 'center',
  },
  hourlyView: {
    borderRadius: 5,
    backgroundColor: colors.detailBackground,
    borderWidth: 1,
    borderColor: colors.pagination,
    shadowColor: colors.pagination,
    elevation: 6,
    marginBottom: 15 * ratioHeight,
  },
  subHeader: {
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font13,
    color: colors.textColor,
    padding: 10 * ratioHeight,
    borderBottomColor: colors.appPrimary,
    borderBottomWidth: 1.2,
  },
  row: {
    flexDirection: 'row',
  },
  footer: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
    textAlign: 'center',
    marginTop: 12 * ratioHeight,
  },
  innerContainer: {
    paddingHorizontal: 10 * ratioWidth,
    paddingBottom: 10 * ratioWidth,
    flexDirection: 'row',
  },
  dropContainer: {
    paddingHorizontal: 10 * ratioWidth,
    paddingBottom: 10 * ratioWidth,
  },
  dropStyle: {
    marginLeft: 0,
  },
  upper: {
    width: '60%',
    flexDirection: 'row',
  },
  lower: {
    width: '60%',
  },
  time: { width: '48%' },
  hour: { width: '40%' },
  inputContainer: { width: '95%' },
  weeksContainer: {
    width: '98%',
    paddingVertical: 4 * ratioHeight,
    borderColor: colors.pagination,
    borderWidth: 0.5,
  },
});

export default styles;
