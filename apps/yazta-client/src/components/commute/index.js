import React, { useState } from 'react';
import { View, Text } from 'react-native';
import DatePickerComponent from '../../helper/dateTimePicker';
import TextBox from '../../helper/inputBox';
import styles from './styles';
import DateContainer from '../../helper/dateContainer';
import DropDown from '../../../../../libs/ui/src/utils/dropdown';

const Commute = () => {
  const recurringList = [
    { id: 1, label: 'Daily', value: 'Daily' },
    { id: 2, label: 'Weekly', value: 'Weekly' },
    { id: 3, label: 'Monthly', value: 'Monthly' },
    { id: 4, label: 'Yearly', value: 'Yearly' },
  ];

  const onPicker = (value, field) => {
    console.log('value ---->:', value);
  };

  const [timing, setTiming] = useState({
    startTime: '',
    endTime: '',
    hourlyRate: '',
    date: '',
    totalPay: '',
    from: '',
    to: '',
    weeklyRate: '',
    totalPayout: '',
  });
  const [showDate, setShowDate] = useState(false);
  const [selectedDate, setSelectedDate] = useState(new Date());

  const onDateChange = (date) => {
    setShowDate(false);
    if (date && date.nativeEvent && date.nativeEvent.timestamp) {
      setSelectedDate(date.nativeEvent.timestamp);
    }
  };

  const onChange = (field, value) => {
    setTiming({ ...timing, [field]: value });
  };

  return (
    <>
      <Text style={styles.header}>Commute Arrangement</Text>

      <View style={styles.hourlyView}>
        <View>
          <Text style={styles.subHeader}>Enter Your Hourly Rate Below</Text>
          <View style={styles.innerContainer}>
            <View style={styles.upper}>
              <View style={styles.time}>
                <TextBox
                  fieldName="Time"
                  placeholder="From"
                  keyboardType="numeric"
                  extraStyle={styles.inputContainer}
                  onChange={(value) => onChange('startTime', value)}
                />
              </View>
              <View style={styles.time}>
                <TextBox
                  fieldName=" "
                  placeholder="To"
                  keyboardType="numeric"
                  extraStyle={styles.inputContainer}
                  onChange={(value) => onChange('endTime', value)}
                />
              </View>
            </View>

            <View style={styles.hour}>
              <TextBox
                fieldName="Hourly Rate"
                placeholder="10$"
                keyboardType="numeric"
                extraStyle={styles.inputContainer}
                onChange={(value) => onChange('hourlyRate', value)}
              />
            </View>
          </View>

          <View style={styles.innerContainer}>
            <View style={styles.lower}>
              <DateContainer
                fieldName="Date"
                selectedDate={selectedDate}
                extraStyle={styles.inputContainer}
                icon="calendar-outline"
                type="Ionicons"
                onPress={() => setShowDate(true)}
              />
            </View>
            <View style={styles.hour}>
              <TextBox
                fieldName="Total Payout"
                placeholder="RS.4000"
                keyboardType="numeric"
                extraStyle={styles.inputContainer}
                onChange={(value) => onChange('totalPay', value)}
              />
            </View>
          </View>
        </View>

        <View style={styles.dropContainer}>
          <DropDown
            data={recurringList}
            extraStyle={styles.dropStyle}
            onPick={onPicker}
            fieldName="Select Recurring Duration"
            placeholder="Select Recurring Duration"
          />
          <TextBox
            fieldName="Number of Weeks"
            placeholder="Enter Number of Weeks"
            keyboardType="numeric"
            extraStyle={styles.weeksContainer}
          />
          <Text style={styles.footer}>
            You can cancel recurring service any time.
          </Text>
        </View>
      </View>
      {showDate && (
        <DatePickerComponent
          value={selectedDate}
          mode="date"
          placeholderText="Select Date"
          onDateChange={(date) => onDateChange(date)}
        />
      )}
    </>
  );
};

export default Commute;
