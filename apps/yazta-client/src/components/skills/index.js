import React from 'react';
import { View, Text } from 'react-native';
import Grid from '../grid';
import styles from './styles';

const Skills = () => {
  return (
    <View style={styles.container}>
      <Grid />
    </View>
  );
};

export default Skills;
