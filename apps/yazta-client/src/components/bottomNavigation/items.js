export default [
  {
    name: 'Home',
    iconType: 'Feather',
    iconName: 'home',
  },
  {
    name: 'Bookings',
    iconType: 'Ionicons',
    iconName: 'search-sharp',
  },
  {
    name: 'Profile',
    iconType: 'FontAwesome',
    iconName: 'user-o',
  },
  {
    name: 'My Pros',
    iconType: 'Feather',
    iconName: 'list',
  },
  {
    name: 'Messages',
    iconType: 'AntDesign',
    iconName: 'message1',
  },
];
