import React from 'react';
import { View, Text } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { colors } from '../../../../../libs/ui/src/themes';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import styles from './styles';
import items from './items';
import Profile from '../../screens/Profile';
import Home from '../../screens/HomeScreen';
import JobScreen from '../../screens/JobScreen';
import BookingsTab from '../../screens/BookingsTab';
import ChatList from '../../screens/ChatList';

const Tab = createBottomTabNavigator();

export default function BottomNavigation() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: styles.tab,
      }}
    >
      {items.map((tabs) => (
        <Tab.Screen
          name={tabs.name}
          component={
            tabs.name == 'Home'
              ? Home
              : tabs.name == 'Bookings'
              ? BookingsTab
              : tabs.name == 'Jobs'
              ? JobScreen
              : tabs.name == 'Messages'
              ? ChatList
              : tabs.name == 'Profile'
              ? Profile
              : tabs.name == 'My Clients'
              ? Home
              : JobScreen
          }
          options={{
            tabBarLabel: ({ focused }) => (
              <Text
                style={[
                  styles.label,
                  {
                    color: focused ? colors.pagination : colors.textPrimary,
                  },
                ]}
              >
                {tabs.name}
              </Text>
            ),
            tabBarIcon: ({ focused }) => (
              <View>
                <Icon
                  type={tabs.iconType}
                  name={tabs.iconName}
                  key={tabs.iconName}
                  size={24}
                  color={focused ? colors.pagination : colors.black}
                />
              </View>
            ),
          }}
        />
      ))}
    </Tab.Navigator>
  );
}
