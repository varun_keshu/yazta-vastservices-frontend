import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 8 * ratioHeight,
  },
  header: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font14,
    marginLeft: 8 * ratioWidth,
  },
});

export default styles;
