import React from 'react';
import { View, Text } from 'react-native';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors } from '../../../../../libs/ui/src/themes';
import RadioOptions from '../radioOptions';
import styles from './styles';

const Radios = (props) => {
  const {
    title,
    option1,
    option2,
    option3,
    iconName,
    extraStyle,
    arrangement,
    time
  } = props;

  return (
    <View>
      <View style={[styles.container, extraStyle]}>
        <Icon
          type="Fontisto"
          name={iconName}
          size={20}
          color={colors.pagination}
        />
        <Text style={styles.header}>{title}</Text>
      </View>
      <RadioOptions
        option1={option1}
        option2={option2}
        option3={option3}
        arrangement={arrangement}
        time={time}
      />
    </View>
  );
};

export default Radios;
