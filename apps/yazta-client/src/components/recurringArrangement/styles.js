import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    borderRadius: 5,
    backgroundColor: colors.detailBackground,
    borderWidth: 1,
    borderColor: colors.pagination,
    shadowColor: colors.pagination,
    elevation: 6,
    paddingBottom: 20 * ratioHeight,
  },
  subContainer: {
    width: '50%',
  },
  inputContainer: {
    marginHorizontal: 10 * ratioHeight,
  },
  header: {
    color: colors.textColor,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font13,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.pagination,
    padding: 10 * ratioHeight,
  },
  subHeader: {
    color: colors.textColor,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font13,
    borderTopWidth: 0.5,
    borderTopColor: colors.pagination,
    padding: 10 * ratioHeight,
    marginTop: 10 * ratioHeight,
  },
  footer: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
    textAlign: 'center',
    marginTop: 10 * ratioHeight,
  },
  dropDownView: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.grey,
    paddingHorizontal: 5,
    backgroundColor: colors.white,
    paddingHorizontal: 10 * ratioWidth,
    height: 40,
    justifyContent: 'center',
  },
  modalContainer: {
    margin: 20,
    backgroundColor: colors.detailBackground,
    elevation: 2,
  },
});

export default styles;
