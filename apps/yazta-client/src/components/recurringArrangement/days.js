const days = [
  {
    id: 1,
    title: 'Monday',
    checked: true,
  },
  {
    id: 2,
    title: 'Tuesday',
    checked: false,
  },
  {
    id: 3,
    title: 'Wednesday',
    checked: false,
  },
  {
    id: 4,
    title: 'Thrusday',
    checked: false,
  },
  {
    id: 5,
    title: 'Friday',
    checked: false,
  },
  {
    id: 6,
    title: 'Saturday',
    checked: false,
  },
  {
    id: 7,
    title: 'Sunday',
    checked: false,
  },
];

export default days;
