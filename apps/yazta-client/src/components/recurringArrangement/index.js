import React, { useState } from 'react';
import { View, Text, FlatList } from 'react-native';
import Checkbox from '../../../../../libs/ui/src/utils/checkbox';
import styles from './styles';
import TextBox from '../../helper/inputBox';
import days from './days';

const RecurringArrangment = () => {
  const [weekDays, setWeekDays] = useState(days);
  const [refresh, setRefresh] = useState(false);

  const onCheck = (id) => {
    const data = weekDays;
    const index = data.findIndex((item) => item.id === id);
    data[index].checked = !data[index].checked;
    setWeekDays(data);
    setRefresh(!refresh);
  };

  const ItemView = ({ item }) => {
    return (
      <View style={styles.subContainer}>
        <Checkbox
          label={item?.title}
          check={item?.checked}
          setCheck={() => onCheck(item.id)}
        />
      </View>
    );
  };

  return (
    <>
      <View style={styles.container}>
        <Text style={styles.header}>Select Days for Service</Text>
        <FlatList
          data={weekDays || []}
          numColumns={2}
          nestedScrollEnabled
          renderItem={ItemView}
          keyExtractor={(item) => item.id}
          extraData={refresh}
        />
        <Text style={styles.subHeader}>Number of Weeks</Text>
        <TextBox
          placeholder="Enter Number of Weeks"
          keyboardType="numeric"
          extraStyle={styles.inputContainer}
        />
      </View>
      <Text style={styles.footer}>
        You can cancel recurring service any time.
      </Text>
    </>
  );
};

export default RecurringArrangment;
