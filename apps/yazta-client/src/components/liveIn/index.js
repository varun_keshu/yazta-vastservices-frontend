import React, { useState } from 'react';
import { View, Text } from 'react-native';
import TextBox from '../../helper/inputBox';
import styles from './styles';
import DatePickerComponent from '../../helper/dateTimePicker';
import DateContainer from '../../helper/dateContainer';

const LiveIn = () => {
  const [timing, setTiming] = useState({
    startTime: '',
    endTime: '',
    hourlyRate: '',
    date: '',
    totalPay: '',
    from: '',
    to: '',
    weeklyRate: '',
    totalPayout: '',
  });

  const [showDate, setShowDate] = useState(false);
  const [selectFromDate, setSelectFromDate] = useState(new Date());
  const [selectToDate, setSelectToDate] = useState(new Date());
  const [event, setEvent] = useState('');

  const onDateChange = (date) => {
    setShowDate(false);
    if (date && date.nativeEvent && date.nativeEvent.timestamp) {
      if (event === 'from') {
        setSelectFromDate(date.nativeEvent.timestamp);
      } else {
        setSelectToDate(date.nativeEvent.timestamp);
      }
    }
  };

  const onChange = (field, value) => {
    setTiming({ ...timing, [field]: value });
  };

  const onClick = (clickEvent) => {
    setShowDate(true);
    setEvent(clickEvent);
  };

  return (
    <>
      <Text style={styles.header}>Live-In Arrangement</Text>
      <View style={styles.hourlyView}>
        <Text style={styles.subHeader}>Enter Your Weekly Rate Below</Text>
        <View style={styles.innerContainer}>
          <View style={styles.upper}>
            <View style={styles.from}>
              <DateContainer
                fieldName="From"
                selectedDate={selectFromDate}
                extraStyle={styles.inputContainer}
                icon="calendar-outline"
                type="Ionicons"
                onPress={() => onClick('from')}
              />
            </View>

            <View style={styles.from}>
              <DateContainer
                fieldName="To"
                selectedDate={selectToDate}
                extraStyle={styles.inputContainer}
                icon="calendar-outline"
                type="Ionicons"
                onPress={() => onClick('to')}
              />
            </View>
          </View>
        </View>
        <View style={styles.innerContainer}>
          <View style={styles.upper}>
            <View style={styles.from}>
              <TextBox
                fieldName="Weekly Rate"
                keyboardType="numeric"
                placeholder="RS.7000/W"
                extraStyle={styles.inputContainer}
                onChange={(value) => onChange('weeklyRate', value)}
              />
            </View>
            <View style={styles.from}>
              <TextBox
                fieldName="Total Payout"
                keyboardType="numeric"
                placeholder="RS.4000"
                extraStyle={styles.inputContainer}
                onChange={(value) => onChange('totalPayout', value)}
              />
            </View>
          </View>
        </View>
      </View>
      {showDate && (
        <DatePickerComponent
          value={new Date()}
          mode="date"
          placeholderText="Select Date"
          onDateChange={(date) => onDateChange(date)}
        />
      )}
    </>
  );
};

export default LiveIn;
