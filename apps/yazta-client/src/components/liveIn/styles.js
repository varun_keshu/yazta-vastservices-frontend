import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  header: {
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font18,
    color: colors.textPrimary,
    marginBottom: 10 * ratioHeight,
    marginTop: 20 * ratioHeight,
    textAlign: 'center',
  },
  hourlyView: {
    borderRadius: 5,
    backgroundColor: colors.detailBackground,
    borderWidth: 1,
    borderColor: colors.pagination,
    shadowColor: colors.pagination,
    elevation: 6,
  },
  subHeader: {
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font13,
    color: colors.textColor,
    padding: 10 * ratioHeight,
    borderBottomColor: colors.appPrimary,
    borderBottomWidth: 1.2,
  },
  row: {
    flexDirection: 'row',
  },
  innerContainer: {
    paddingHorizontal: 10 * ratioWidth,
    paddingBottom: 10 * ratioWidth,
    flexDirection: 'row',
  },
  upper: {
    width: '100%',
    flexDirection: 'row',
  },
  from: { width: '50%' },
  inputContainer: { width: '95%' },
});

export default styles;
