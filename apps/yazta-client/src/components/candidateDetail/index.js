import React from 'react';
import { View, Text } from 'react-native';
import { useSelector } from 'react-redux';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors } from '../../../../../libs/ui/src/themes';
import styles from './styles';

const CandidateDetail = () => {

  const providerDetail = useSelector(
    (state) => state.clientBooking.providerDetail
  );

  const Field = ({ label, item }) => {
    return (
      <View style={styles.innerContainer}>
        <Text style={styles.label}>{label}</Text>
        <Text style={styles.description}>{item}</Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Provider Details</Text>
      <View style={styles.subContainer}>
        <Icon
          name="ios-person-circle-sharp"
          type="Ionicons"
          size={45}
          color={colors.textColor}
          extraStyles={styles.iconStyle}
        />
        <View style={styles.fieldStyle}>
          <Field label="Name" item={providerDetail?.name} />
          <Field label="Age" item={`${providerDetail?.age} years old`} />
          <Field label="Education" item={providerDetail?.qualification} />
          <Field label="Skills" item={providerDetail?.skill} />
          <Field label="Gender" item={providerDetail?.gender} />
          <Field label="Service" item="Commute" />
          <Field
            label="Residence"
            item={`${providerDetail?.address1} ${providerDetail?.address2}`}
          />

          <View style={styles.bottomContainer}>
            <Text style={styles.label}>Details</Text>
            <Text style={styles.description}>{providerDetail?.summary_bio}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default CandidateDetail;
