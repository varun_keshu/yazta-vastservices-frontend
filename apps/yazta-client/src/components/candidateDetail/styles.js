import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    borderColor: colors.pagination,
    borderWidth: 0.7,
    borderRadius: 5,
    marginVertical: '1.8%',
    paddingVertical: 8 * ratioHeight,
  },
  title: {
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font14,
    color: colors.appDark,
    textAlign: 'center',
    borderBottomColor: colors.pagination,
    borderBottomWidth: 0.7,
    paddingBottom: 12 * ratioWidth,
  },
  subContainer: {
    paddingVertical: 25 * ratioWidth,
    paddingHorizontal: 12 * ratioWidth,
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  innerContainer: {
    flexDirection: 'row',
    borderBottomColor: colors.appPrimary,
    borderBottomWidth: 1,
    paddingBottom: 12 * ratioWidth,
    marginBottom: 12 * ratioWidth,
  },
  bottomContainer: {
    flexDirection: 'row',
  },
  label: {
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font13,
    color: colors.pagination,
    width: '35%',
  },
  description: {
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font12,
    color: colors.textPrimary,
    width: '65%',
  },
  iconStyle: {
    width: '18%',
    marginLeft: -6 * ratioWidth,
    marginRight: 10 * ratioWidth,
    marginTop: '35%',
  },
  fieldStyle: {
    width: '82%',
  },
});

export default styles;
