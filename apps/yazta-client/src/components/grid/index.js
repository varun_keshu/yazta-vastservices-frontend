import React, { useState, useEffect } from 'react';
import { View, FlatList, TouchableOpacity, Image } from 'react-native';
import Checkbox from '../../../../../libs/ui/src/utils/checkbox';
import styles from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { setSkills } from '../../../../../libs/store/src/common/booking/action';

const Grid = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    setServiceData(serviceList);
  }, []);

  const serviceList = useSelector((state) => state.core.serviceList);

  const [serviceData, setServiceData] = useState([]);
  const [skillArr, setSkillArr] = useState([]);
  const [refresh, setRefresh] = useState(false);

  const onCheck = (id) => {
    const data = serviceList;
    const index = data.findIndex((item) => item.id === id);
    data[index].checked = !data[index].checked;
    setServiceData(data);
    setRefresh(!refresh);
    sendCheckedData(data);
  };

  const sendCheckedData = (data) => {
    let checkedSkills = '';
    data.forEach((element) => {
      if (element.checked) {
        checkedSkills = `${checkedSkills},${element.serviceName}`;
      }
      checkedSkills = checkedSkills.replace(/(^,)|(,$)/g, '');
      setSkillArr(checkedSkills);
    });
    dispatch(setSkills(checkedSkills));
  };

  const ItemView = ({ item }) => {
    return (
      <TouchableOpacity style={styles.cardContainer}>
        <Image style={styles.imageStyle} source={{ uri: item?.image }} />
        <Checkbox
          label={item?.serviceName}
          check={item?.checked}
          setCheck={() => onCheck(item.id)}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={serviceData.length == 0 ? serviceList : serviceData}
        numColumns={2}
        nestedScrollEnabled
        renderItem={ItemView}
        keyExtractor={(item) => item.id}
        extraData={refresh}
      />
    </View>
  );
};

export default Grid;
