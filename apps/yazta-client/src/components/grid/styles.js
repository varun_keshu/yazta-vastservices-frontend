import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    borderWidth: 0.5,
    borderColor: colors.pagination,
    borderRadius: 2,
    padding: 5 * ratioHeight,
  },
  cardContainer: {
    paddingVertical: 5 * ratioHeight,
    alignItems: 'center',
    width: '48%',
    borderColor: colors.appPrimary,
    borderWidth: 0.5,
    margin: '1%',
  },
  imageStyle: {
    height: 30 * ratioHeight,
    width: 30 * ratioWidth,
    resizeMode: 'contain',
  },
  title: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
    textAlign: 'center',
    marginVertical: 8 * ratioHeight,
  },
});

export default styles;
