import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import ButtonPrimary from '../../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import InputField from '../../../../../../libs/ui/src/helper/inputs/inputField';
import { AlertMessage } from '../../../../../../libs/ui/src/utils/alerts';
import {
  validateRequiredField,
  validateEmail,
  validatePhone,
  validateName,
} from '../../../../../../libs/ui/src/utils/validation';
import DocumentPick from '../../../../../../libs/ui/src/utils/documentPicker';
import { getUploadBody } from '../../../../../../libs/ui/src/utils/uploadFile';
import { uploadImage } from '../../../../../../libs/store/src/common/core/action';
import {
  getProfile,
  updateProfileAction,
} from '../../../../../../libs/store/src/client/user/action';
import DocumentPicker from 'react-native-document-picker';
import { useDispatch, useSelector } from 'react-redux';
import styles from './styles';

const ProfileFooter = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const clientData = useSelector((state) => state.clientUser.clientData);

  useEffect(() => {
    dispatch(getProfile(clientData.id ? clientData.id : ''));
    if (clientData !== initialState) {
      setProfileDetails(initialState);
    }
  }, [clientData]);

  const initialState = {
    name: clientData?.name ? clientData?.name : '',
    email: clientData?.email ? clientData?.email : '',
    address1: clientData?.address1 ? clientData?.address1 : '',
    address2: clientData?.address2 ? clientData?.address2 : '',
    phone: clientData?.phoneNo ? clientData?.phoneNo : '',
  };

  const [profileDetails, setProfileDetails] = useState(initialState);
  const [profileAvatar, setSingleFile] = useState(null);
  const [error, setError] = useState({ field: '', message: '' });

  const onSubmit = () => {
    const errorEntity = { ...onValidate() };
    // if (errorEntity.field == 'profileAvatar') {
    //   AlertMessage(errorEntity.message);
    // }
    if (Object.keys(errorEntity).length === 0) {
      setProfileDetails(initialState);
      let request = {
        name: profileDetails.name,
        email: profileDetails.email,
        address1: profileDetails.address1,
        address2: profileDetails.address2,
        phoneNo: profileDetails.phone,
        id: clientData.id ? clientData.id : '',
      };
      dispatch(updateProfileAction(request));
    }
  };

  const onValidate = () => {
    // const validateProfile = validateRequiredField(
    //   'profileAvatar',
    //   profileAvatar,
    //   'Profile Image'
    // );
    const validateUsername = validateName(profileDetails.name);
    const validateEmailId = validateEmail(profileDetails.email);
    const validateAddress1 = validateRequiredField(
      'address1',
      profileDetails.address1,
      'Address'
    );
    const validateAddress2 = validateRequiredField(
      'address2',
      profileDetails.address2,
      'Second Address'
    );
    const validatePhoneNo = validatePhone(profileDetails.phone);

    let errorEntity = {};
    // if (validateProfile.message) {
    //   errorEntity = validateProfile;
    // } else
    if (validateUsername.message) {
      errorEntity = validateUsername;
    } else if (validateEmailId.message) {
      errorEntity = validateEmailId;
    } else if (validateAddress1.message) {
      errorEntity = validateAddress1;
    } else if (validateAddress2.message) {
      errorEntity = validateAddress2;
    } else if (validatePhoneNo.message) {
      errorEntity = validatePhoneNo;
    }

    setError(errorEntity);
    return errorEntity;
  };

  const onChange = (field, value) => {
    setProfileDetails({ ...profileDetails, [field]: value });
  };

  const uploadProfile = (profileAvatar) => {
    const id = 96;
    const formData = getUploadBody(profileAvatar);
    dispatch(uploadImage(formData, id));
  };

  const selectFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });
      console.log('res --->: ', res);
      setSingleFile(res);
      // uploadProfile(res);
    } catch (err) {
      setSingleFile(null);
      if (DocumentPicker.isCancel(err)) {
        setSingleFile(null);
      } else {
        alert('Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  return (
    <>
      <View style={styles.subContainer}>
        <DocumentPick selectFile={selectFile} profileAvatar={profileAvatar} />
      </View>

      <InputField
        placeholder="Enter Name"
        fieldName="Name"
        value={profileDetails.name}
        onChange={(value) => onChange('name', value)}
        error={error.field === 'name' && error.message}
        astric
      />
      <InputField
        placeholder="Enter Email"
        fieldName="Email"
        value={profileDetails.email}
        onChange={(value) => onChange('email', value)}
        error={error.field === 'email' && error.message}
        astric
      />
      <InputField
        placeholder="Enter Address"
        fieldName="Address"
        value={profileDetails.address1}
        onChange={(value) => onChange('address1', value)}
        error={error.field === 'address1' && error.message}
        astric
      />
      <InputField
        placeholder="Enter Address2"
        fieldName="Address2"
        value={profileDetails.address2}
        onChange={(value) => onChange('address2', value)}
        error={error.field === 'address2' && error.message}
        astric
      />
      <InputField
        placeholder="Enter Phone"
        fieldName="Phone"
        value={profileDetails.phone}
        onChange={(value) => onChange('phone', value)}
        error={error.field === 'phone' && error.message}
        astric
      />
      <ButtonPrimary label="Submit" onPress={() => onSubmit()} />
    </>
  );
};

export default ProfileFooter;
