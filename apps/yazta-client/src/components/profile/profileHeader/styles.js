import { StyleSheet } from 'react-native';
import {
  colors,
  fonts,
  ratioWidth,
  ratioHeight,
} from '../../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  subContainer: {
    alignItems: 'center',
  },
  extraStyle: {
    paddingHorizontal: 10 * ratioWidth,
    paddingVertical: 8 * ratioHeight,
    borderRadius: 4,
    marginBottom: 20 * ratioHeight,
    flexDirection: 'row',
  },
  labelStyle: {
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font13,
  },
  upload: {
    color: colors.appDark,
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font12,
    marginTop: 8 * ratioHeight,
    marginBottom: 18 * ratioHeight,
  },
  iconStyle: {
    backgroundColor: colors.pagination,
    paddingHorizontal: 20 * ratioWidth,
    paddingVertical: 18 * ratioWidth,
    borderRadius: 50,
  },
  imageStyle: {
    borderRadius: 50 * ratioHeight,
    height: 75 * ratioHeight,
    width: 78 * ratioHeight,
  },
});

export default styles;
