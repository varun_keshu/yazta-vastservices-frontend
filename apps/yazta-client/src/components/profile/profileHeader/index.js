import React, { useState } from 'react';
import { Text, Image, View, TouchableOpacity } from 'react-native';
import styles from './styles';
import { colors } from '../../../../../../libs/ui/src/themes';
import Icon from '../../../../../../libs/ui/src/helper/icon/icons';
import { launchFile } from '../../../../../../libs/ui/src/utils/imagePicker';
import DocumentPicker from '../../../../../../libs/ui/src/utils/documentPicker';

const ProfileHeader = () => {
  const [profileAvatar, setProfile] = useState(null);

  const onImageReceived = (res) => {
    setProfile(res);
  };

  const selectFile = () => {
    launchFile(onImageReceived);
  };

  return (
    <View style={styles.subContainer}>
      <DocumentPicker />
    </View>
  );
};

export default ProfileHeader;
