import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from '../../../../libs/ui/src/helper/icon/icons';
import {
  colors,
  fonts,
  ratioWidth,
  ratioHeight,
} from '../../../../libs/ui/src/themes';
import moment from 'moment';

const DateContainer = (props) => {
  const {
    selectedDate,
    fieldName,
    placeholder,
    extraStyle,
    icon,
    type,
    onPress,
  } = props;

  return (
    <View>
      <Text style={styles.fieldName}>{fieldName}</Text>
      <TouchableOpacity
        onPress={onPress}
        style={[styles.container, extraStyle]}
      >
        <Text style={styles.inputBox}>
          {selectedDate
            ? moment(selectedDate).format('DD/MM/YYYY')
            : 'Select Date'}
        </Text>
        {icon && (
          <Icon
            name={icon}
            type={type}
            color={colors.pagination}
            size={20}
            extraStyles={styles.iconStyle}
          />
        )}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  fieldName: {
    fontSize: fonts.size.font12,
    fontFamily: fonts.type.montserratMedium,
    color: colors.pagination,
    marginBottom: 3 * ratioHeight,
    marginTop: 10 * ratioHeight,
  },
  container: {
    borderColor: colors.appPrimary,
    borderWidth: 1.2,
    borderRadius: 3,
    flexDirection: 'row',
    backgroundColor: colors.white,
    justifyContent: 'space-between',
  },
  inputBox: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
    paddingVertical: ratioHeight * 7,
    paddingHorizontal: ratioWidth * 10,
  },
  placeholder: {
    color: colors.textGrey,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
    paddingVertical: ratioHeight * 3,
    paddingHorizontal: ratioWidth * 10,
  },
  iconStyle: {
    marginRight: 5 * ratioWidth,
  },
});

export default DateContainer;
