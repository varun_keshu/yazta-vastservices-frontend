import React from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';
import {
  colors,
  fonts,
  ratioWidth,
  ratioHeight,
} from '../../../../libs/ui/src/themes';

const TextInputBox = (props) => {
  const {
    onChange,
    placeholder,
    value,
    numLines,
    fieldName,
    extraStyle,
    fieldStyle,
    keyboardType,
  } = props;

  return (
    <View>
      {fieldName && <Text style={styles.fieldName}>{fieldName}</Text>}
      <View style={[styles.container, extraStyle]}>
        <TextInput
          style={styles.inputBox}
          placeholder={placeholder}
          placeholderTextColor={colors.textGrey}
          value={value}
          onChangeText={onChange}
          numberOfLines={numLines}
          keyboardType={keyboardType}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  fieldName: {
    fontSize: fonts.size.font12,
    fontFamily: fonts.type.montserratMedium,
    color: colors.pagination,
    marginBottom: 3 * ratioHeight,
    marginTop: 10 * ratioHeight,
  },
  container: {
    borderColor: colors.appPrimary,
    borderWidth: 1,
    borderRadius: 3,
    backgroundColor: colors.white,
  },
  inputBox: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
    paddingVertical: ratioHeight * 3,
    paddingHorizontal: ratioWidth * 10,
  },
});

export default TextInputBox;
