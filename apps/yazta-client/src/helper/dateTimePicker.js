import React from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';

const DatePickerComponent = (props) => {
  const { value, mode, onDateChange } = props;
  return (
    <DateTimePicker
      testID="dateTimePicker"
      value={value}
      mode={mode}
      style={{ width: '100%' }}
      is24Hour={false}
      // display="default"
      onChange={onDateChange}
      display="calendar"
    />
  );
};

export default DatePickerComponent;
