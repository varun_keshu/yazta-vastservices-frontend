import React, { useEffect } from 'react';
import {
  StatusBar,
  SafeAreaView,
  ActivityIndicator,
  StyleSheet,
  LogBox,
} from 'react-native';
import AppRouter from '../navigation/appRouter';
import { colors } from '../../../../libs/ui/src/themes';
import { useSelector, useDispatch } from 'react-redux';
import FlashMessage from 'react-native-flash-message';
import asyncStore from '../../../../libs/store/src/asyncStore';
import { saveStoreToken } from '../../../../libs/store/src/common/auth/action';

const App = () => {
  const loader = useSelector((state) => state.auth.loader);
  const dispatch = useDispatch();

  useEffect(() => {
    initializeApp();
  }, []);

  const initializeApp = async () => {
    retrieveToken();
  };

  const retrieveToken = async () => {
    try {
      const token = await asyncStore.getToken('Token');
      if (token !== null) {
        dispatch(saveStoreToken(token));
      }
    } catch (error) {
      console.log('error :', error);
    }
  };

  LogBox.ignoreLogs(['Warning: ...']);
  LogBox.ignoreAllLogs();

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StatusBar backgroundColor={colors.appPrimary} barStyle="dark-content" />
      <AppRouter />
      <FlashMessage position="top" />
      {loader && (
        <ActivityIndicator
          size="large"
          color={colors.appPrimary}
          style={styles.loadingStyle}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  loadingStyle: {
    backgroundColor: colors.white,
    opacity: 0.5,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
  },
});

export default App;
