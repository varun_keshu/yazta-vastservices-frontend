import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useSelector } from 'react-redux';
import Login from '../screens/LoginScreen';
import Landing from '../screens/Landing';
import Register from '../screens/Register';
import ForgetPassword from '../screens/ForgetPassword';
import OtpVerification from '../screens/OtpVerification';
import ResetPassword from '../screens/ResetPassword';
import { navigationRef } from './navigationHelper';
import Congratulation from '../../../../libs/ui/src/screens/Congratulation';
import SideDrawer from './sideDrawer';
import HomeScreen from '../screens/HomeScreen';
import ProviderList from '../screens/ProviderList';
import ProviderDetail from '../screens/ProviderDetail';
import Arrangement from '../screens/Arrangement';
import FavoriteProvider from '../screens/FavoriteProvider';
import OfferDetailScreen from '../screens/OfferDetail';
import CouponList from '../screens/CouponList';
import Support from '../screens/Support';
import Faq from '../screens/Faq';
import ContactUs from '../screens/ContactUs';
import TermsConditions from '../screens/TermsConditions';
import Feedback from '../screens/Feedback';
import ChatDetail from '../screens/ChatDetail';
import ChatList from '../screens/ChatList';
import Settings from '../screens/Settings';
import General from '../screens/General';
import Help from '../screens/Help';
import Privacy from '../screens/Privacy';
import Notification from '../screens/Notification';

const Stack = createNativeStackNavigator();

const HomeStack = () => {
  return <SideDrawer />;
};

const MainStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Landing"
        component={Landing}
        options={{ headerShown: false }}
      />   
      <Stack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ForgetPassword"
        component={ForgetPassword}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="OtpVerification"
        component={OtpVerification}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ResetPassword"
        component={ResetPassword}
        options={{ headerShown: false }}
      />
    
      <Stack.Screen
        name="HomeStack"
        component={HomeStack}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Congratulation"
        component={Congratulation}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ProviderList"
        component={ProviderList}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ProviderDetail"
        component={ProviderDetail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Arrangement"
        component={Arrangement}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="FavoriteProvider"
        component={FavoriteProvider}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="OfferDetail"
        component={OfferDetailScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CouponList"
        component={CouponList}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Support"
        component={Support}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Faq"
        component={Faq}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ContactUs"
        component={ContactUs}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="TermsConditions"
        component={TermsConditions}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Feedback"
        component={Feedback}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ChatDetail"
        component={ChatDetail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ChatList"
        component={ChatList}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Settings"
        component={Settings}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="General"
        component={General}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Help"
        component={Help}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Privacy"
        component={Privacy}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Notification"
        component={Notification}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const AppRouter = () => {
  const userToken = useSelector((state) => state.auth.userToken);

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator  >    
          <Stack.Screen
            name="MainStack"
            component={MainStack}
            options={{ headerShown: false }}
          />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppRouter;
