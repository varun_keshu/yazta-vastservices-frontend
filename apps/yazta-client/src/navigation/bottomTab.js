import React from 'react';
import BottomNavigation from '../components/bottomNavigation';

const BottomTab = () => {
  return <BottomNavigation />;
};

export default BottomTab;
