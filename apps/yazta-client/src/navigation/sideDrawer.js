import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContent from '../components/drawerContent';
import BottomTab from './bottomTab';
import ProviderList from '../screens/ProviderList';
import ProviderDetail from '../screens/ProviderDetail';
import Arrangement from '../screens/Arrangement';
import FavoriteProvider from '../screens/FavoriteProvider';

const Drawer = createDrawerNavigator();

const MainStack = () => {
  return <BottomTab />;
};

const options = {
  headerShown: false,
};

const SideDrawer = () => {
  return (
    <Drawer.Navigator drawerContent={() => <DrawerContent />}>
      <Drawer.Screen
        key="MainStack"
        name="MainStack"
        component={MainStack}
        options={options}
      />
      <Drawer.Screen
        key="ProviderList"
        name="ProviderLists"
        component={ProviderList}
        options={options}
      />
      <Drawer.Screen
        key="ProviderDetail"
        name="ProviderDetails"
        component={ProviderDetail}
        options={options}
      />
      <Drawer.Screen
        key="Arrangement"
        name="Arrangement"
        component={Arrangement}
        options={options}
      />
      <Drawer.Screen
        key="FavoriteProvider"
        name="FavoriteProvider"
        component={FavoriteProvider}
        options={options}
      />
    </Drawer.Navigator>
  );
};

export default SideDrawer;
