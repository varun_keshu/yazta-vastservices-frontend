import * as React from 'react';
import { DrawerActions } from '@react-navigation/native';
import { CommonActions } from '@react-navigation/native';

export const navigationRef = React.createRef();

export function goToScreen(name, params = {}) {
  navigationRef.current?.navigate(name, params);
}

export function replaceStack(name) {
  navigationRef.current?.dispatch(
    StackActions.replace(name)
  )
}
export function app2StackReset(screenName) {
  navigationRef.current?.dispatch(
    CommonActions.reset({
      index:0,
      routes:[
        {name:screenName}
      ]
    })
  )
}

export const backPress = () => {
  navigationRef.current?.goBack();
};

export const openDrawer = () => {
  navigationRef.current?.dispatch(DrawerActions.openDrawer());
};

export const resetStack = (screen) => {
  navigationRef.current?.dispatch(
    CommonActions.reset({
      index: 0,
      routes: [{ name: screen }],
    })
  );
};
