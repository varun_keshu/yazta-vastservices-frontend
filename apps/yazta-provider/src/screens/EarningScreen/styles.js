import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  ratioWidth,
  fonts,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
    paddingHorizontal: 20 * ratioHeight,
  },
  titleBox: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'

  },
  titleTxt: {
    fontSize: fonts.size.font16,
    fontFamily: fonts.type.montserratMedium,
    color: colors.black,
  },
  icon: {
    paddingLeft: 10 * ratioHeight,
    alignSelf: 'flex-end',

  },
  topButton: {
borderWidth:1,
borderRadius:20,
backgroundColor:colors.white,
paddingHorizontal:20,
alignItems:'center',
width:150*ratioWidth,
marginVertical:20*ratioHeight,
borderColor:colors.textColor,
  },
  topActiveButton: {
borderWidth:1,
borderRadius:20,
backgroundColor:colors.appDark,
paddingHorizontal:20,
alignItems:'center',
width:150*ratioWidth,
marginVertical:20*ratioHeight,
borderColor:colors.textColor,
  },
  topActiveButtonTxt: {
    fontSize: fonts.size.font14,
    fontFamily: fonts.type.montserratMedium,
    color: colors.white,
    paddingVertical: 10,
  },
  topButtonTxt: {
    fontSize: fonts.size.font14,
    fontFamily: fonts.type.montserratMedium,
    color: colors.black,
    paddingVertical: 10,
    // paddingHorizontal:10,

  },
});

export default styles;
