import React from 'react';
import Container from '../../../../../libs/ui/src/component/container';
import OfferDetails from '../component/offerDetails';

const OfferDetailScreen = (props) => {
  return (
    <Container
      role="PROVIDER"
      showBack
      showHeader
      showMenu
      label="Offer Details"
    >
      <OfferDetails {...props} />
    </Container>
  );
};

export default OfferDetailScreen;
