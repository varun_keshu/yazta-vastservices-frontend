import React from 'react';
import {SectionList,Text} from 'react-native';
import Container from '../../../../libs/ui/src/component/container';
import NotificationCard from '../../../../../libs/ui/src/component/NotificationCard';
import { images } from '../../../../../libs/ui/src/themes';

import styles from './styles';


const  sections=[  
  {title: 'Monday, March 29', data: [
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 1},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 0},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 1},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 2},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 1},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 1},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 1},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 0},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 1},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 2},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 1},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 0},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 1},
    {image: `${images.cleaning}`,name:'Avi', date: "5 Aug 2021", time: "10:10 am", lastSeen: "1 hour ago", status: 2},

  ]},  
]  
const NotificationScreen = ({navigation}) => {



 function  _renderSwipebleList(item, index) {
    
    return (
      <NotificationCard
      imageLeft={item.image}
					description={item.name}
					
					onPress={() => { }}
          date={item.date}
          time={item.time}
          lastSeen={item.lastSeen}
          status={item.status}

       />
    );
  }
  return (
    <Container
      scrollEnabled
      showHeader
      label="Notifications"
      showMenu
      extraStyles={styles.container}
    >
    <SectionList
      sections={sections}
      keyExtractor={(item, index) => item + index}
      renderItem={({item, index}) =>  _renderSwipebleList(item, index)  }
    />
    </Container>
  );
};

export default NotificationScreen;
