import React from 'react';
import Walkthrough from '../../../../libs/ui/src/screens/Walkthrough';
import { Walk1, Walk2, Walk3 } from '../../../../libs/ui/src/themes/svgs';

const data = [
  {
    label: 'Lorem ipsum',
    content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr',
    image: <Walk1 height="80%" width="100%" />,
  },
  {
    label: 'Lorem ipsum',
    content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr',
    image: <Walk2 height="80%" width="100%" />,
  },
  {
    label: 'Lorem ipsum',
    content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr',
    image: <Walk3 height="80%" width="100%" />,
  },
];

const Landing = () => {
  return <Walkthrough data={data} />;
};

export default Landing;
