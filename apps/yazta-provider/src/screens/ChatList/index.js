import React from 'react';
import {SectionList,Text} from 'react-native';
import Container from '../../../../libs/ui/src/component/container';
import MessageCard from '../../../../../libs/ui/src/component/MessageCard';
import { images } from '../../../../../libs/ui/src/themes';

import styles from './styles';


const  sections=[  
  {title: 'Monday, March 29', data: [
    {image: `${images.cleaning}`,name:'Avi',lastMessage:'see you'},
    {image: `${images.cleaning}`,name:'Ellie',lastMessage:'Thank you'},
    {image: `${images.cleaning}`,name:'John',lastMessage:'Nice Job'},
  ]},
  {title: 'Monday, March 28', data: [
    {image: `${images.cleaning}`,name:'Avi',lastMessage:'see you'},
    {image: `${images.cleaning}`,name:'Ellie',lastMessage:'Thank you'},
    {image: `${images.cleaning}`,name:'John',lastMessage:'Nice Job'},
  ]},  
]  
const ChatList = ({navigation}) => {



 function  _renderSwipebleList(item, index) {
    
    return (
      <MessageCard
      imageLeft={item.image}
					description={item.lastMessage}
					dateTime={'4 min '}
          name={item.name}
					onPress={() => {navigation.navigate('ChatDetail') }}
          onInfoPress={() => { }}
          showInfo={true}
       />
    );
  }
  return (
    <Container
      scrollEnabled
      showHeader
      label="Chat List"
      showMenu
      extraStyles={styles.container}
    >
    <SectionList
      sections={sections}
      keyExtractor={(item, index) => item + index}
      renderItem={({item, index}) =>  _renderSwipebleList(item, index)  }
      renderSectionHeader={({ section: { title } }) => (  <Text style={styles.header}>{title}</Text> )}
    />
    </Container>
  );
};

export default ChatList;
