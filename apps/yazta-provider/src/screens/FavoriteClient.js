import React from 'react';
import Container from '../../../../libs/ui/src/component/container';
import FavoriteList from '../../../../libs/ui/src/screens/FavoriteList';

const FavoriteClient = () => {
  return (
    <Container showHeader showBack showMenu label="Favorite Clients">
      <FavoriteList role="PROVIDER" />
    </Container>
  );
};

export default FavoriteClient;
