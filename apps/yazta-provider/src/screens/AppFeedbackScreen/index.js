import React from 'react';
import Container from '../../../../../libs/ui/src/component/container';
import Feedback from '../../component/appFeedback';

const AppFeedbackScreen = () => {
  return (
    <Container showHeader showMenu label="App Feedback">
      <Feedback />
    </Container>
  );
};

export default AppFeedbackScreen;
