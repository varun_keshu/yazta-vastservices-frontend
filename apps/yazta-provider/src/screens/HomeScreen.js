import React, { useEffect } from 'react';
import Container from '../../../../../libs/ui/src/component/container';
import { saveProviderData } from '../../../../libs/store/src/provider/user/action';
import ProviderHome from '../component/providerHome';
import asyncStore from '../../../../libs/store/src/asyncStore';
import { useDispatch } from 'react-redux';

const HomeScreen = ({ navigation }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    retrieveData();
  }, []);

  const retrieveData = async () => {
    try {
      const userData = await asyncStore.getProviderData('providerData');
      if (userData !== null) {
        dispatch(saveProviderData(userData));
      }
    } catch (error) {
      console.log('retrieveData error :', error);
    }
  };

  return (
    <Container
      role="PROVIDER"
      showHeader
      showMenu
      label={'Virtual Office'}
      onNotification={() => {
        navigation.navigate('Notification');
      }}
    >
      <ProviderHome />
    </Container>
  );
};

export default HomeScreen;
