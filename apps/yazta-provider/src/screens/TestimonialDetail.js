import React from 'react';
import TestimonialDetails from '../component/cards/testimonialDetails';

const TestimonialDetail = (props) => {
  return <TestimonialDetails {...props} />;
};

export default TestimonialDetail;
