import React from 'react';
import Container from '../../../../../libs/ui/src/component/container';
import styles from './styles';
import ProfileHeader from '../../component/profile/profileHeader';
import ProfileFooter from '../../component/profile/profileFooter';

const Profile = () => {
  return (
    <Container
      scrollEnabled
      showHeader
      label="Profile"
      showBack
      showMenu
      role="PROVIDER"
      extraStyles={styles.container}
    >
      <ProfileHeader />
      <ProfileFooter />
    </Container>
  );
};

export default Profile;
