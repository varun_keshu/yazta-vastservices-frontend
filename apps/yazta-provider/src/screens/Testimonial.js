import React, { useEffect } from 'react';
import { testimonialListApi } from '../../../../libs/store/src/common/core/action';
import TestimonialCard from '../component/cards/testimonialCard';
import { useDispatch, useSelector } from 'react-redux';

const data = [
  {
    image: '',
    name: 'John Doe',
    description: 'It was a amazing experience when I used this app',
  },
  {
    image: '',
    name: 'John Doe',
    description: 'It was a amazing experience when I used this app',
  },
  {
    image: '',
    name: 'John Doe',
    description: 'It was a amazing experience when I used this app',
  },
  {
    image: '',
    name: 'Adele',
    description: 'It was a amazing experience when I used this app',
  },
  {
    image: '',
    name: 'Bruno',
    description: 'It was a amazing experience when I used this app',
  },
  {
    image: '',
    name: 'Tony',
    description: 'It was a amazing experience when I used this app',
  },
  {
    image: '',
    name: 'Tom',
    description: 'It was a amazing experience when I used this app',
  },
];

const Testimonial = () => {
  const dispatch = useDispatch();
  const testimonialList = useSelector((state) => state.core.testimonialList);

  useEffect(() => {
    dispatch(testimonialListApi());
  }, []);

  return <TestimonialCard data={testimonialList} />;
};

export default Testimonial;
