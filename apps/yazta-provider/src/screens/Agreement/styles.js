import { StyleSheet } from 'react-native';
import {
  colors,
  fonts,
  ratioHeight,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 30 * ratioHeight,
    paddingHorizontal: 20 * ratioHeight,
  },
  header: {
    textAlign: 'center',
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font20,
    color: colors.textPrimary,
    lineHeight: 25 * ratioHeight,
  },
  content: {
    textAlign: 'center',
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font15,
    color: colors.textPrimary,
    lineHeight: 22 * ratioHeight,
    marginVertical: 20 * ratioHeight,
  },
  button: {
    paddingHorizontal: 10 * ratioWidth,
    paddingVertical: 15 * ratioWidth,
    marginHorizontal: 10 * ratioWidth,
    borderRadius: 50,
  },
});

export default styles;
