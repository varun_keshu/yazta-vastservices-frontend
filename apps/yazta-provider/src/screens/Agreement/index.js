import React from 'react';
import Container from '../../../../../libs/ui/src/component/container';
import { Text } from 'react-native';
import styles from './styles';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import { useNavigation } from '@react-navigation/core';

const Agreement = () => {
  const navigation = useNavigation();
  return (
    <Container
      scrollEnabled
      showHeader
      label="Agreement"
      showBack
      showMenu
      extraStyles={styles.container}
    >
      <Text style={styles.header}>Congratulations!</Text>
      <Text style={styles.header}>You're in business!</Text>
      <Text style={styles.content}>
        You just accepted the final terms of the agreement, Per that agreement,
        your tasks include cooking and cleaning for an ESTIMATED 3hrs 30 min on
        03/28/2021 with a total payout of $70. Total payout is due regardless of
        how long it actually takes to complete the tasks
      </Text>
      <ButtonPrimary
        label="Back to Home Screen"
        extraStyle={styles.button}
        onPress={() => navigation.navigate('HomeStack')}
      />
    </Container>
  );
};

export default Agreement;
