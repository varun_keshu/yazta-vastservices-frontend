import { StyleSheet,Dimensions } from 'react-native';
import {
  colors,
  ratioHeight,
  ratioWidth,
  fonts,
} from '../../../../../libs/ui/src/themes';
const width = Dimensions.get('window').width;

const ITEM_WIDTH = Math.round(width);

const styles = StyleSheet.create({
  flex1: {
    backgroundColor: colors.appSecondary,
   flex:1,
  },
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
  },
  buttonStyle: {
    paddingHorizontal: 10 * ratioWidth,
    paddingVertical: 8 * ratioHeight,
    marginHorizontal: 20 * ratioWidth,
    marginTop: 22 * ratioHeight,
    borderRadius: 50,
  },
  header: {
    color: colors.pagination,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
    textAlign: 'center',
    textAlignVertical: 'center',
    marginVertical: 10 * ratioHeight,
    // backgroundColor:colors.appDark,
    minHeight:20*ratioHeight,
  },

  ///////   
  chatBox:{
    margin:10,
    marginBottom:0,

  },
    chatContainer:{
      flexDirection:'row',
     
    },
    infoBox:{
      marginLeft:20

    },
    descriptionBox:{
      backgroundColor:colors.textColor,
      padding:10*ratioHeight,
      borderTopRightRadius:15,
      borderBottomEndRadius:15,
      borderBottomLeftRadius:15,
      borderTopLeftRadius:-100,
      zIndex:1,
      maxWidth:ITEM_WIDTH
      
    },
    descriptionTxt:{
      fontSize:fonts.size.font14,
      color:colors.white,
      fontFamily:fonts.type.montserratMedium
    },
    timeTxt:{
      fontSize:fonts.size.font12,
      color:colors.grey,
    },
    imageContainer:{
  
    },
    userImg:{
      width: 65,
      height: 65,
          borderRadius:100,
          overflow:'hidden',
          backgroundColor:colors.appDark,
    },
  
  
    talkBubbleTriangleLeft: {
      position: "absolute",
      left: -20,
      top: 0,
      width: 0,
      height: 0,
      borderTopColor:colors.textColor,
      borderTopWidth: 0,
      borderRightWidth: 40,
      borderRightColor:colors.textColor,
      borderBottomWidth: 25,
      borderBottomColor: "transparent",
    },
    /////
    talkBubbleTriangleRight: {
      position: "absolute",
      right: -20,
      top: 0,
      width: 0,
      height: 0,
      borderTopColor:'transparent',
      borderTopWidth: 25,
      borderRightWidth: 40,
      borderRightColor:colors.pagination,
      borderBottomWidth: 0,
      borderBottomColor: "transparent",
    transform:[{rotate:'180deg'}]

    },
    senderChatContainer:{
      flexDirection:'row-reverse',
   

    },
    senderInfoBox:{
      marginRight:20

    },
    senderImageContainer:{
  
    },
    senderDescriptionBox:{
      backgroundColor:colors.pagination,
      padding:10*ratioHeight,
      borderTopRightRadius:0,
      borderBottomEndRadius:15, 
      borderBottomLeftRadius:15,
      borderTopLeftRadius:15,
      zIndex:1,
      maxWidth:ITEM_WIDTH
      
    },
    descriptionTxt:{
      fontSize:fonts.size.font14,
      color:colors.white,
      fontFamily:fonts.type.montserratMedium
    },
    timeTxt:{
      fontSize:fonts.size.font12,
      paddingTop:2*ratioHeight,
      color:colors.grey,
    },
    
  inputBox:{
    flexDirection:'row',
    marginHorizontal:20*ratioHeight,
    margin:10*ratioHeight,
    // marginBottom:30*ratioHeight,
    borderWidth:1,
    borderColor:colors.pagination,
    justifyContent:'space-between',
    alignItems:'center',
    paddingHorizontal:10*ratioHeight

  },
  inputField:{
    maxWidth:width-100
  }, 
  sendBtn:{

  },
  iconBox:{
   
  }, 
});

export default styles;
