import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
    paddingHorizontal: 20 * ratioHeight,
  },
  button: {
    paddingHorizontal: 10 * ratioWidth,
    marginHorizontal: 20 * ratioWidth,
  },
});

export default styles;
