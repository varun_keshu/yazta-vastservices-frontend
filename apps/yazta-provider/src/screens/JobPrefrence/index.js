import React from 'react';
import Container from '../../../../../libs/ui/src/component/container';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import Toggle from '../../../../../libs/ui/src/helper/toggle';
import Preferences from '../../component/preferences';
import Skills from '../../component/skills';
import Summary from '../../component/summary';
import styles from './styles';
import { useNavigation } from '@react-navigation/core';
import Radios from '../../helper/radios';
import { ratioWidth } from '../../../../../libs/ui/src/themes';

const JobPreference = () => {
  const navigation = useNavigation();

  const onNavigate = () => {
    navigation.navigate('Profile');
  };

  return (
    <Container
      scrollEnabled
      showHeader
      label="Skillset & Preferences"
      showBack
      showMenu
      extraStyles={styles.container}
    >
      <Toggle
        label1="Personal Details"
        label2="Job Preferences"
        onPress={() => navigation.navigate('Profile')}
        extraJobContainerStyle={{
          paddingHorizontal: 18 * ratioWidth,
        }}
        extraDetailContainerStyle={{
          paddingHorizontal: 18 * ratioWidth,
        }}
      />
      <Preferences />
      <Skills />
      <Radios
        title="Preferred time of day"
        option1="AM"
        option2="PM"
        option3="EITHER"
        iconName="stopwatch"
      />
      <Summary />
      <ButtonPrimary
        label="Submit & Proceed"
        extraStyle={styles.button}
        onPress={() => onNavigate()}
      />
    </Container>
  );
};

export default JobPreference;
