import React from 'react';
import Signup from '../../../../libs/ui/src/screens/Signup';

const Register = (props) => {
  return <Signup userRole="PROVIDER" {...props} />;
};

export default Register;
