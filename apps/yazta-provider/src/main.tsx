import React from 'react';
import { AppRegistry } from 'react-native';
import App from './app/App';
import { Provider } from 'react-redux';
import store from '../../../libs/store/src/configStore';

const RnApp = () => (
    <Provider store={store}>
        <App />
    </Provider>
)

AppRegistry.registerComponent('main', () => RnApp);