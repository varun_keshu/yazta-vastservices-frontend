import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContent from '../component/drawerContent';
import BottomTab from './bottomTab';
import OfferDetailScreen from '../screens/OfferDetailScreen';
import IncomingOfferScreen from '../screens/IncomingOfferScreen';
import Agreement from '../screens/Agreement';

const Drawer = createDrawerNavigator();

const MainStack = () => {
  return <BottomTab />;
};

const options = {
  headerShown: false,
};

const SideDrawer = () => {
  return (
    <Drawer.Navigator drawerContent={() => <DrawerContent />}>
      <Drawer.Screen
        key="MainStack"
        name="MainStack"
        component={MainStack}
        options={options}
      />
      <Drawer.Screen
        key="OfferDetailScreen"
        name="OfferDetailScreens"
        component={OfferDetailScreen}
        options={options}
      />
      <Drawer.Screen
        key="IncomingOffers"
        name="IncomingOffers"
        component={IncomingOfferScreen}
        options={options}
      />
      <Drawer.Screen
        key="Agreements"
        name="Agreements"
        component={Agreement}
        options={options}
      />
    </Drawer.Navigator>
  );
};

export default SideDrawer;
