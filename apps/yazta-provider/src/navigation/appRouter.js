import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useSelector } from 'react-redux';
import Login from '../screens/LoginScreen';
import Testimonial from '../screens/Testimonial';
import Landing from '../screens/Landing';
import TestimonialDetail from '../screens/TestimonialDetail';
import Register from '../screens/Register';
import ForgetPassword from '../screens/ForgetPassword';
import ResetPassword from '../screens/ResetPassword';
import OtpVerification from '../screens/OtpVerification';
import { navigationRef } from './navigationHelper';
import HomeScreen from '../screens/HomeScreen';
import UploadDocument from '../screens/UploadDocument';
import JobPreference from '../screens/JobPrefrence';
import Profile from '../screens/Profile';
import Congratulation from '../../../../libs/ui/src/screens/Congratulation';
import SideDrawer from './sideDrawer';
import OfferDetailScreen from '../screens/OfferDetailScreen';
import FavoriteClient from '../screens/FavoriteClient';
import IncomingOffer from '../screens/IncomingOfferScreen';
import Agreement from '../screens/Agreement';
import ChatDetail from '../screens/ChatDetail';
import EarningScreen from '../screens/EarningScreen';
import NotificationScreen from '../screens/NotificationScreen'

const Stack = createNativeStackNavigator();

const HomeStack = () => {
  return <SideDrawer />;
};

const MainStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Landing"
        component={Landing}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Testimonial"
        component={Testimonial}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="TestimonialDetail"
        component={TestimonialDetail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ForgetPassword"
        component={ForgetPassword}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ResetPassword"
        component={ResetPassword}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="OtpVerification"
        component={OtpVerification}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="HomeStack"
        component={HomeStack}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="UploadDocument"
        component={UploadDocument}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="JobPreference"
        component={JobPreference}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Congratulation"
        component={Congratulation}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="IncomingOffer"
        component={IncomingOffer}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="OfferDetailScreen"
        component={OfferDetailScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Agreement"
        component={Agreement}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="FavoriteClient"
        component={FavoriteClient}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ChatDetail"
        component={ChatDetail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Earning"
        component={EarningScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Notification"
        component={NotificationScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const AppRouter = () => {
  const userToken = useSelector((state) => state.auth.userToken);

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator>
          <Stack.Screen
            name="MainStack"
            component={MainStack}
            options={{ headerShown: false }}
          />
         
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppRouter;
