import React, { useState } from 'react';
import { View, Text } from 'react-native';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors } from '../../../../../libs/ui/src/themes';
import styles from './styles';

const RadioOptions = (props) => {
  const { option1, option2, option3 } = props;

  const [selected, setSelected] = useState('livein');

  return (
    <View style={styles.subContainer}>
      <View style={styles.innerContainer}>
        <View style={styles.container}>
          <Icon
            type="Fontisto"
            name={
              selected === 'livein' ? 'radio-btn-active' : 'radio-btn-passive'
            }
            size={16}
            color={colors.pagination}
            onPress={() => setSelected('livein')}
          />
          <Text style={styles.options}>{option1}</Text>
        </View>
        <View style={styles.container}>
          <Icon
            type="Fontisto"
            name={
              selected === 'commute' ? 'radio-btn-active' : 'radio-btn-passive'
            }
            size={16}
            color={colors.pagination}
            onPress={() => setSelected('commute')}
          />
          <Text style={styles.options}>{option2}</Text>
        </View>
      </View>
      <View style={styles.container}>
        <Icon
          type="Fontisto"
          name={
            selected === 'either' ? 'radio-btn-active' : 'radio-btn-passive'
          }
          size={16}
          color={colors.pagination}
          onPress={() => setSelected('either')}
        />
        <Text style={styles.options}>{option3}</Text>
      </View>
    </View>
  );
};

export default RadioOptions;
