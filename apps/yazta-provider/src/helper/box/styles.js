import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10 * ratioHeight,
  },
  header: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font15,
    marginLeft: 8 * ratioWidth,
    marginTop: 20 * ratioHeight,
  },
  iconStyle: {
    marginTop: 20 * ratioHeight,
  },
  subContainer: {
    borderWidth: 0.45,
    borderColor: colors.pagination,
    borderRadius: 3,
    padding: 12 * ratioWidth,
    backgroundColor: colors.white,
    marginLeft: 20 * ratioWidth,
    marginTop: 3 * ratioHeight,
  },
  content: {
    color: colors.grey,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font11,
    lineHeight: 16 * ratioHeight,
  },
  subContent: {
    color: colors.grey,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font11,
    marginLeft: 20 * ratioWidth,
    lineHeight: 16 * ratioHeight,
  },
});

export default styles;
