import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors } from '../../../../../libs/ui/src/themes';

const Box = (props) => {
  const { iconName, title, content, iconType, subHeading } = props;
  return (
    <View>
      <View style={styles.container}>
        {iconName && (
          <Icon
            type={iconType}
            name={iconName}
            size={22}
            color={colors.pagination}
            extraStyles={styles.iconStyle}
          />
        )}
        {title && <Text style={styles.header}>{title}</Text>}
      </View>
      {subHeading && <Text style={styles.subContent}>{subHeading}</Text>}
      {content && (
        <View style={styles.subContainer}>
          <Text style={styles.content}>{content}</Text>
        </View>
      )}
    </View>
  );
};

export default Box;
