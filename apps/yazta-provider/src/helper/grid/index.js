import React, { useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image } from 'react-native';
import { images } from '../../../../../libs/ui/src/themes';
import Checkbox from '../../../../../libs/ui/src/utils/checkbox';
import styles from './styles';

const serviceList = [
  {
    id: 1,
    image: `${images.cleaning}`,
    title: 'Cleaning',
    label: 'cleaning',
    checked: false,
  },
  {
    id: 2,
    image: `${images.babysit}`,
    title: 'BabySitting',
    label: 'babysit',
    checked: false,
  },
  {
    id: 3,
    image: `${images.cooking}`,
    title: 'Cooking',
    label: 'cooking',
    checked: false,
  },
  {
    id: 4,
    image: `${images.service}`,
    title: 'Full Service',
    label: 'service',
    checked: false,
  },
];

const Grid = () => {
  const [serviceData, setServiceData] = useState(serviceList);
  const [refresh, setRefresh] = useState(false);

  const onCheck = (id) => {
    const data = serviceData;
    const index = data.findIndex((item) => item.id === id);
    data[index].checked = !data[index].checked;
    setServiceData(data);
    setRefresh(!refresh);
  };

  const ItemView = ({ item }) => {
    return (
      <TouchableOpacity style={styles.cardContainer}>
        <Image style={styles.imageStyle} source={item.image} />
        <Checkbox
          label={item.title}
          check={item.checked}
          setCheck={() => onCheck(item.id)}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={serviceData || []}
        numColumns={2}
        nestedScrollEnabled
        renderItem={ItemView}
        keyExtractor={(item) => item.id}
        extraData={refresh}
      />
    </View>
  );
};

export default Grid;
