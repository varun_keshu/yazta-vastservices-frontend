import { StyleSheet } from 'react-native';
import { colors, fonts, ratioHeight } from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  tab: {
    backgroundColor: colors.appPrimary,
    height: 60,
    paddingBottom: 5 * ratioHeight,
  },
  screen: {
    flex: 1,
    backgroundColor: colors.appSecondary,
  },
  label: {
    fontSize: fonts.size.font11,
    fontFamily: fonts.type.montserratMedium,
    color: colors.textPrimary,
  },
});

export default styles;
