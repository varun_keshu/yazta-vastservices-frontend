export default [
  {
    name: 'Home',
    iconType: 'Feather',
    iconName: 'home',
  },
  {
    name: 'Jobs',
    iconType: 'Ionicons',
    iconName: 'search-sharp',
  },
  {
    name: 'Profile',
    iconType: 'FontAwesome',
    iconName: 'user-o',
  },
  {
    name: 'My Clients',
    iconType: 'Feather',
    iconName: 'list',
  },
  {
    name: 'Messages',
    iconType: 'AntDesign',
    iconName: 'message1',
  },
];
