import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { useDispatch } from 'react-redux';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import TripleToggle from '../../../../../libs/ui/src/helper/toggle/triple';
import OfferList from './OfferList';
import HistoryList from './historyList';
import JobList from './jobList';
import styles from './styles';
import Container from '../../../../../libs/ui/src/component/container';
import {
  getCurrentJobs,
  getNewOffers,
  getJobHistory,
} from '../../../../../libs/store/src/provider/jobs/action';

const IncomingOffer = () => {
  const [selectedLabel, setSelectedLabel] = useState('one');
  const dispatch = useDispatch();

  useEffect(() => {
    // dispatch(getCurrentJobs());
    // dispatch(getNewOffers());
    // dispatch(getJobHistory());
  }, []);

  const onClick = (current) => {
    setSelectedLabel(current);
  };

  return (
    <Container
      showHeader
      showMenu
      showBack
      label={
        selectedLabel == 'one'
          ? 'Current Jobs'
          : selectedLabel == 'two'
          ? 'New Offers'
          : 'Job History'
      }
    >
      <View style={styles.mainContainer}>
        <TripleToggle
          label1="Current Jobs"
          label2="New Offers"
          label3="Job History"
          onPress={onClick}
        />
        {selectedLabel == 'two' && (
          <>
            <Text style={styles.congrats}>Congratulations!</Text>
            <Text style={styles.congrats}>You have incoming offers!</Text>
          </>
        )}
        {selectedLabel == 'one' ? (
          <OfferList />
        ) : selectedLabel == 'two' ? (
          <JobList />
        ) : (
          <HistoryList />
        )}
        <ButtonPrimary
          label="Improve your odds at getting jobs"
          extraStyle={styles.buttonStyle}
        />
      </View>
    </Container>
  );
};

export default IncomingOffer;
