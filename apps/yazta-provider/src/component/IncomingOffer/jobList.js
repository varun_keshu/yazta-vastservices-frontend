import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors } from '../../../../../libs/ui/src/themes';
import styles from './styles';

const Item = [
  {
    id: 1,
    name: 'Alie',
    place: 'Socano, Saho',
    day: 'Monday-Wednesday',
    date: '9th to 14th Aug 2021',
    like: true,
    status: 'Commute',
    type: 'Recurring',
  },
  {
    id: 2,
    name: 'Miliene',
    place: 'Tinkal, Koki',
    day: 'Sunday, Aug 12,2021',
    date: '08:00-11:30',
    like: false,
    status: 'COMMUTE',
    type: '',
  },
  {
    id: 3,
    name: 'Olako',
    place: 'Tema',
    day: 'Monday-Friday',
    date: '9th to 14th Aug 2021',
    like: true,
    status: 'LIVE-IN (24/7)',
    type: 'Recurring',
  },
];

const JobList = () => {
  const navigation = useNavigation();

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() =>
          navigation.navigate('OfferDetailScreen', { type: item.type })
        }
      >
        <View style={styles.leftContainer}>
          <Text style={styles.name}>{item.name}</Text>
          <Text style={styles.name}>{item.place}</Text>
          <Text style={styles.day}>{item.day}</Text>
          <Text style={styles.day}>{item.date}</Text>
        </View>

        <View style={styles.rightContainer}>
          <Icon
            type={item.like ? 'AntDesign' : 'EvilIcons'}
            name="heart"
            size={item.like ? 20 : 28}
            color={colors.textColor}
          />
          <Text style={styles.status}>{item.status}</Text>
          {item.type ? <Text style={styles.type}>{item.type}</Text> : <></>}
          <Icon
            type="AntDesign"
            name="play"
            size={22}
            color={colors.textColor}
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <FlatList
      data={Item}
      keyExtractor={(item) => item.id}
      renderItem={renderItem}
    />
  );
};

export default JobList;
