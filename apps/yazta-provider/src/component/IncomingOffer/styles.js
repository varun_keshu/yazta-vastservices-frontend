import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  ratioWidth,
  fonts,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  mainContainer: { padding: 15 * ratioHeight },
  container: {
    backgroundColor: colors.white,
    borderWidth: 0.5,
    borderRadius: 6,
    borderColor: colors.pagination,
    paddingHorizontal: 15 * ratioWidth,
    paddingVertical: 10 * ratioHeight,
    marginTop: 10 * ratioHeight,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  congrats: {
    textAlign: 'center',
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font11,
    lineHeight: 14 * ratioHeight,
  },
  leftContainer: {
    width: '75%',
  },
  rightContainer: {
    width: '25%',
    alignItems: 'center',
  },
  name: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font13,
    lineHeight: 18 * ratioHeight,
  },
  status: {
    color: colors.textColor,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font10,
    marginVertical: 8 * ratioHeight,
  },
  type: {
    color: colors.white,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font10,
    backgroundColor: colors.pagination,
    paddingHorizontal: 8 * ratioHeight,
    paddingVertical: 3 * ratioHeight,
    marginBottom: 6 * ratioHeight,
    borderRadius: 20,

  },
  day: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font10,
    marginTop: 4 * ratioHeight,
  },
  buttonStyle: {
    borderRadius: 50,
    paddingHorizontal: 10 * ratioWidth,
    paddingVertical: 10 * ratioHeight,
  },
});

export default styles;
