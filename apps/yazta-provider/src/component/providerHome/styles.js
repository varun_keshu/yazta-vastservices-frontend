import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  fonts,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    padding: 10 * ratioWidth,
  },
  cardContainer: {
    flex: 1,
    backgroundColor: colors.background,
    margin: 5 * ratioHeight,
    paddingVertical: 25 * ratioHeight,
    paddingHorizontal: 12 * ratioHeight,
    borderRadius: 8,
  },
  label: {
    textAlign: 'center',
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font15,
    marginTop: 10 * ratioHeight,
  },
});

export default styles;
