import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors, ratioHeight } from '../../../../../libs/ui/src/themes';
import styles from './styles';

const Item = [
  {
    id: '1',
    name: 'New Offers',
    iconType: 'FontAwesome5',
    iconName: 'home',
    route: 'IncomingOffer',
  },
  {
    id: '2',
    name: 'Current Jobs',
    iconType: 'FontAwesome5',
    iconName: 'user-alt',
    route: 'IncomingOffer',
  },
  {
    id: '3',
    name: 'Job Preference',
    iconType: 'MaterialCommunityIcons',
    iconName: 'briefcase-account',
    route: 'JobPreference',
  },
  {
    id: '4',
    name: 'Earnings',
    iconType: 'FontAwesome5',
    iconName: 'hand-holding-usd',
    route: 'Earning',
  },
  {
    id: '5',
    name: 'Support',
    iconType: 'MaterialIcons',
    iconName: 'support-agent',
    route: 'IncomingOffer',
  },
  {
    id: '6',
    name: 'Invite Friends / Make Money',
    iconType: 'MaterialIcons',
    iconName: 'supervised-user-circle',
    route: 'IncomingOffer',
  },
];

const ProviderHome = () => {
  const navigation = useNavigation();

  const onNavigate = (route) => {
    if (route) {
      navigation.navigate(route);
    }
  };

  const renderItem = (item) => {
    let items = item.item;
    return (
      <TouchableOpacity
        style={styles.cardContainer}
        onPress={() => {
          onNavigate(items.route);
        }}
      >
        <Icon
          type={items.iconType}
          name={items.iconName}
          key={items.iconName}
          size={52 * ratioHeight}
          color={colors.pagination}
        />
        <Text style={styles.label}>{items.name}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        numColumns={2}
        keyExtractor={(item) => item.id}
        data={Item}
        renderItem={renderItem}
      />
    </View>
  );
};

export default ProviderHome;
