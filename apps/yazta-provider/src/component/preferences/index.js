import React from 'react';
import { View, Text } from 'react-native';
import Radios from '../../helper/radios';
import styles from './styles';

const Preferences = () => {
  return (
    <View>
      <Text style={styles.header}>Select Your Preferences</Text>
      <Radios
        title="Preferred living arrangement"
        option1="LIVE - IN (24/7)"
        option2="COMMUTE"
        option3="EITHER"
        iconName="houzz"
      />
      {/* <Radios
        title="Preferred time of day"
        option1="AM"
        option2="PM"
        option3="EITHER"
        iconName="stopwatch"
      /> */}
    </View>
  );
};

export default Preferences;
