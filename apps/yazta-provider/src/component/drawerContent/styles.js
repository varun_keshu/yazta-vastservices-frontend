import { StyleSheet } from 'react-native';
import {
  colors,
  fonts,
  ratioHeight,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: { flexDirection: 'row' },
  drawerContent: {
    flexDirection: 'row',
    marginTop: 15 * ratioHeight,
    width: '90%',
    alignItems: 'center',
    paddingLeft: 20 * ratioWidth,
  },
  avatar: {
    height: 70 * ratioWidth,
    width: 70 * ratioWidth,
    borderRadius: 50,
  },
  header: {
    fontSize: fonts.size.font18,
    fontFamily: fonts.type.montserratBold,
    color: colors.appDark,
    paddingHorizontal: 20 * ratioWidth,
    paddingTop: 20 * ratioWidth,
  },
  title: {
    fontSize: fonts.size.font16,
    fontFamily: fonts.type.montserratBold,
    paddingHorizontal: 10 * ratioWidth,
    color: colors.textPrimary,
    width: '60%',
    marginLeft: 15 * ratioWidth,
    textAlign: 'center',
    lineHeight: 20 * ratioHeight,
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 0.5,
    borderColor: colors.pagination,
    borderRadius: 4,
    paddingVertical: 15 * ratioHeight,
    paddingHorizontal: 12 * ratioWidth,
    marginHorizontal: 10 * ratioHeight,
    marginTop: 12 * ratioHeight,
    backgroundColor: '#FEFBF8',
  },
  sectionTitle: {
    fontSize: fonts.size.font16,
    fontFamily: fonts.type.montserratBold,
    color: colors.appDark,
    marginLeft: 15 * ratioWidth,
  },
  drawerLabel: {
    color: colors.appDark,
    fontSize: fonts.size.font15,
    fontFamily: fonts.type.montserratMedium,
    marginLeft: -10 * ratioWidth,
  },
  subcontainer: {
    marginBottom: 10 * ratioHeight,
    marginTop: 5 * ratioHeight,
  },
});

export default styles;
