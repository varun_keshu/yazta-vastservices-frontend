import React from 'react';
import { View, Text } from 'react-native';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import items from './items';
import { colors } from '../../../../../libs/ui/src/themes';
import Section from './section';
import UserSection from './userSection';
import styles from './styles';
import { useNavigation } from '@react-navigation/core';
import { useDispatch } from 'react-redux';
import { logoutUser } from '../../../../../libs/store/src/common/auth/action';

const DrawerContent = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const LogoutButton = () => {
    dispatch(logoutUser());
  };

  const onNavigate = (route) => {
     if (route === 'Logout') {
       LogoutButton();
     } else {
       navigation.navigate(`${route}`);
     }
  };

  return (
    <View style={styles.container}>
      <DrawerContentScrollView>
        <UserSection />
        <Section />
        {items.map((drawer) => (
          <DrawerItem
            icon={() => (
              <Icon
                type={drawer.iconType}
                name={drawer.iconName}
                size={22}
                color={colors.pagination}
              />
            )}
            label={() => <Text style={styles.drawerLabel}>{drawer.name}</Text>}
            onPress={() => onNavigate(drawer.route)}
          />
        ))}
      </DrawerContentScrollView>
    </View>
  );
};

export default DrawerContent;
