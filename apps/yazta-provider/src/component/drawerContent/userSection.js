import React from 'react';
import { View, Text, Image } from 'react-native';
import { images } from '../../../../../libs/ui/src/themes';
import styles from './styles';

const UserSection = () => {
  return (
    <View>
      <View style={styles.drawerContent}>
        <Image source={images.person} style={styles.avatar} />
        <Text style={styles.title}>Yalitza Hernandez</Text>
      </View>
      <Text style={styles.header}>Account</Text>
    </View>
  );
};

export default UserSection;
