export default [
  {
    name: 'Payment Methods',
    iconType: 'FontAwesome5',
    iconName: 'wallet',
    route: 'Home',
  },
  {
    name: 'Profile Details',
    iconType: 'FontAwesome5',
    iconName: 'user-tie',
    route: 'Home',
  },
  {
    name: 'Support',
    iconType: 'MaterialCommunityIcons',
    iconName: 'face-agent',
    route: 'Home',
  },
  {
    name: 'Earnings',
    iconType: 'FontAwesome5',
    iconName: 'hand-holding-usd',
    route: 'Earning',
  },
  {
    name: 'Settings',
    iconType: 'Ionicons',
    iconName: 'settings-sharp',
    route: 'Settings',
  },
  {
    name: 'Favorite Clients',
    iconType: 'FontAwesome',
    iconName: 'heart',
    route: 'FavoriteClient',
  },
  {
    name: 'Logout',
    iconType: 'FontAwesome5',
    iconName: 'power-off',
    route: 'Logout',
  },
];
