import React from 'react';
import { View, Text } from 'react-native';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors } from '../../../../../libs/ui/src/themes';
import styles from './styles';

const data = [
  {
    iconName: 'user',
    iconType: 'Entypo',
    label: 'Find a Job',
  },
  {
    iconName: 'user-tie',
    iconType: 'FontAwesome5',
    label: 'My Clients',
  },
];

const Section = () => {
  return (
    <View style={styles.subcontainer}>
      {data.map((item) => (
        <View style={styles.section}>
          <View style={styles.row}>
            <Icon
              type={item.iconType}
              name={item.iconName}
              size={22}
              color={colors.pagination}
            />
            <Text style={styles.sectionTitle}>{item.label}</Text>
          </View>
          <Icon type="AntDesign" name="play" size={22} color={colors.appDark} />
        </View>
      ))}
    </View>
  );
};

export default Section;
