import { StyleSheet } from 'react-native';
import {
  colors,
  fonts,
  ratioHeight,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 15 * ratioHeight,
    alignItems: 'center',
  },
  subContainer: {
    padding: 10 * ratioHeight,
    marginHorizontal: 30 * ratioWidth,
    marginTop: 80 * ratioHeight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 60 * ratioHeight,
    width: 60 * ratioHeight,
    borderRadius: 50 * ratioHeight,
  },
  box: {
    paddingVertical: 5 * ratioWidth,
    paddingHorizontal: 15 * ratioWidth,
    width: '80%',
  },
  name: {
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font14,
    color: colors.black,
    textAlign: 'center',
    marginTop: 5 * ratioHeight,
    marginBottom: 15 * ratioHeight,
  },
  desc: {
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font15,
    textAlign: 'center',
    color: colors.appDark,
    lineHeight: 18 * ratioHeight,
  },
  signUp: {
    backgroundColor: colors.textColor,
    paddingHorizontal: 100 * ratioWidth,
    paddingVertical: 8 * ratioHeight,
    borderRadius: 4,
    marginTop: 45 * ratioHeight,
  },
  label: {
    color: colors.white,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font15,
  },
  button: {
    marginTop: 45 * ratioHeight,
  },
});

export default styles;
