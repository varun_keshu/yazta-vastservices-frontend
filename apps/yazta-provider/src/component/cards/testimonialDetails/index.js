import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import Container from '../../../../libs/ui/src/component/container';
import ButtonPrimary from '../../../../libs/ui/src/helper/buttons/buttonPrimary';
import { images } from '../../../../libs/ui/src/themes';
import styles from './styles';

const TestimonialDetails = (props) => {
  const { route } = props;
  const { params } = route;
  const navigation = useNavigation();
  return (
    <Container
      showHeader
      showBack
      label="Testimonials"
      extraStyles={styles.container}
    >
      <View style={styles.subContainer}>
        <Image
          style={styles.image}
          source={params.image ? { uri: params.image } : images.avatar}
        />
        <View style={styles.box}>
          <Text style={styles.name}>{params?.name}</Text>
          <Text style={styles.desc}>{params?.description}</Text>
        </View>
        <ButtonPrimary
          label="Sign Up"
          extraStyle={styles.button}
          onPress={() => navigation.navigate('Register')}
        />
      </View>
    </Container>
  );
};

export default TestimonialDetails;
