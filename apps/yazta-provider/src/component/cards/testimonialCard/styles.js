import { StyleSheet } from 'react-native';
import {
  colors,
  fonts,
  ratioHeight,
  ratioWidth,
} from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 15 * ratioHeight,
  },
  subContainer: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    padding: 10 * ratioHeight,
    marginHorizontal: 30 * ratioWidth,
    marginTop: 12 * ratioHeight,
    borderWidth: 0.3,
    borderColor: colors.pagination,
    borderRadius: 3,
  },
  image: {
    height: 60 * ratioHeight,
    width: 60 * ratioHeight,
    borderRadius: 50 * ratioHeight,
  },
  imageContainer: {
    borderRadius: 50,
  },
  box: {
    paddingVertical: 5 * ratioWidth,
    paddingHorizontal: 15 * ratioWidth,
    width: '80%',
  },
  name: {
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font14,
    color: colors.black,
  },
  desc: {
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font11,
    color: colors.appDark,
    marginTop: 5 * ratioHeight,
  },
  buttonStyle: {
    marginHorizontal: 30 * ratioWidth,
    paddingHorizontal: 10 * ratioWidth,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

export default styles;
