import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { View, Image, Text, TouchableOpacity, ScrollView } from 'react-native';
import ButtonPrimary from '../../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import Container from '../../../../../libs/ui/src/component/container';
import { images } from '../../../../../libs/ui/src/themes';
import styles from './styles';

const TestimonialCard = (props) => {
  const { data } = props;
  const navigation = useNavigation();

  const onNavigate = () => {
    navigation.navigate('Register');
  };

  return (
    <Container showHeader label="Testimonials" extraStyles={styles.container}>
      <View style={{ flex: 0.8 }}>
        <ScrollView
          nestedScrollEnabled={true}
          showsVerticalScrollIndicator={false}
        >
          {data.map((item, index) => {
            return (
              <TouchableOpacity
                key={index}
                style={styles.subContainer}
                onPress={() =>
                  navigation.navigate('TestimonialDetail', {
                    name: item?.users_permissions_user?.name,
                    description: item.description,
                    image: item?.users_permissions_user?.image,
                  })
                }
              >
                <View style={styles.imageContainer}>
                  <Image
                    style={styles.image}
                    source={
                      item?.users_permissions_user?.image
                        ? { uri: item?.users_permissions_user?.image }
                        : images.avatar
                    }
                  />
                </View>
                <View style={styles.box}>
                  <Text style={styles.name}>
                    {item?.users_permissions_user?.name}
                  </Text>
                  <Text style={styles.desc}>{item?.description}</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>

      <View style={{ flex: 0.3 }}>
        <ButtonPrimary
          label="Proceed"
          extraStyle={styles.buttonStyle}
          showIcon
          iconType="AntDesign"
          iconName="arrowright"
          iconSize={24}
          onPress={() => onNavigate()}
        />
      </View>
    </Container>
  );
};

export default TestimonialCard;
