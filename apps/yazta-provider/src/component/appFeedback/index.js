import React from 'react';
import { View, Text } from 'react-native';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import styles from './styles';

const Feedback = () => {
  return (
    <View style={styles.container}>
      <View style={styles.upperContainer}>
        <Text style={styles.text}>
          No app is perfect, certainly not ours.However, we really want to make
          it so.Not to be dramatic but your voice really is essential. Literally
          anything you share with us will make your and everone else's app
          better. Thank you!
        </Text>
      </View>
      <View style={styles.middleContainer}>
        <Text style={styles.comment}> Please add a comment</Text>
      </View>
      <View style={styles.commentContainer}></View>
      <View>
        <ButtonPrimary
          label="Submit"
          extraStyle={styles.buttonStyle}
          labelStyle={styles.label}
        />
      </View>
      <View style={styles.bottomContainer}>
        <Text style={styles.inviteText}>Invite friends and make money!</Text>
      </View>
      <View>
        <Text style={styles.bottom}>SIEMON2021</Text>
      </View>
    </View>
  );
};

export default Feedback;
