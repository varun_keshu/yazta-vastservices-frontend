import React, { useState } from 'react';
import { View, Text } from 'react-native';
import Icon from '../../../../../libs/ui/src/helper/icon/icons';
import { colors } from '../../../../../libs/ui/src/themes';
import styles from './styles';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import { useNavigation } from '@react-navigation/core';
import GeneralPopUp from '../../../../../libs/ui/src/component/GeneralPopUp';

const OfferDetails = (props) => {
  const navigation = useNavigation();
  const jobType = props?.route?.params?.type;

  const [cancelVisible, setCancelVisible] = useState(false);
  const [declineVisible, setDeclineVisible] = useState(false);
  const [acceptVisible, setAcceptVisible] = useState(false);
  const [offerCancelVisible, setOfferCancelVisible] = useState(false);

  return (
    <>
      <View
        style={styles.container}
        style={
          cancelVisible || offerCancelVisible || acceptVisible || declineVisible
            ? styles.container1
            : styles.container
        }
      >
        <View style={styles.topContainer}>
          <Icon
            type="MaterialCommunityIcons"
            name="calendar-range"
            size={26}
            color={colors.white}
            extraStyles={styles.iconStyle}
          />
          <Text style={styles.date}>Monday 9th Aug, 2021, 1 Week</Text>
        </View>
        <Text style={styles.liveIn}>LIVE-IN</Text>

        <View style={styles.topContainer}>
          <Icon
            type="Feather"
            name="map-pin"
            size={20}
            color={colors.white}
            extraStyles={styles.iconStyle}
          />
          <Text style={styles.address}>
            Tennyson 323, Polanco, 41.40338, 2.17403
          </Text>
        </View>

        <View style={styles.middleContainerTop}>
          <View style={styles.seperator}>
            <Text style={styles.title}>Task type</Text>
            <Text style={styles.label}>Babysitting</Text>
          </View>
          <View style={styles.weekly}>
            <Text style={styles.title}>Weekly rate</Text>
            <Text style={styles.label}>$ 5000</Text>
          </View>
        </View>

        <View style={styles.middleContainerBottom}>
          <View style={styles.seperator}>
            <Text style={styles.title}>Expected job length</Text>
            <Text style={styles.label}>1 week</Text>
          </View>
          <View style={styles.weekly}>
            <Text style={styles.title}>Total payout </Text>
            <Text style={styles.label}>$ 5000</Text>
          </View>
        </View>
      </View>

      <View style={styles.bottomContainer}>
        <ButtonPrimary
          label="Accept Offer"
          extraStyle={styles.acceptStyle}
          onPress={() => setAcceptVisible(true)}
        />
        <ButtonPrimary
          label="Decline"
          extraStyle={styles.declineStyle}
          onPress={() =>
            jobType === 'Recurring'
              ? setCancelVisible(true)
              : setDeclineVisible(true)
          }
        />
      </View>

      <ButtonPrimary
        label="Chat for more details"
        extraStyle={styles.chatStyle}
        showRightIcon
        iconType="MaterialCommunityIcons"
        iconName="message-text"
        iconSize={25}
      />

      <GeneralPopUp
        isVisible={declineVisible}
        onRequestClose={() => {
          setDeclineVisible(false);
        }}
        OnRightPress={() => {
          setDeclineVisible(false);
        }}
        OnLeftPress={() => {
          setDeclineVisible(false), navigation.navigate('IncomingOffer');
        }}
        rightButtonTxt={'No'}
        leftButtonTxt={'Yes'}
        description={'Are you sure you want to cancel?'}
      />

      <GeneralPopUp
        isVisible={cancelVisible}
        onRequestClose={() => {
          setCancelVisible(false);
        }}
        OnRightPress={() => {
          setCancelVisible(false), navigation.navigate('IncomingOffer');
        }}
        OnLeftPress={() => {
          setCancelVisible(false), setOfferCancelVisible(true);
        }}
        rightButtonTxt={'Only the specific job'}
        leftButtonTxt={'Cancel the whole recurring arrangement'}
        description={'Are you sure you want to cancel?'}
      />

      <GeneralPopUp
        isVisible={offerCancelVisible}
        onRequestClose={() => {
          setOfferCancelVisible(false);
        }}
        OnRightPress={() => {
          setOfferCancelVisible(false);
        }}
        OnLeftPress={() => {
          setOfferCancelVisible(false), navigation.navigate('IncomingOffer');
        }}
        rightButtonTxt={'No'}
        leftButtonTxt={'Yes'}
        description={'Are sure you want to cancel the whole recurring thing?'}
      />

      <GeneralPopUp
        isVisible={acceptVisible}
        onRequestClose={() => {
          setAcceptVisible(false);
        }}
        OnRightPress={() => {
          setAcceptVisible(false);
        }}
        OnLeftPress={() => {
          setAcceptVisible(false), navigation.navigate('Agreement');
        }}
        rightButtonTxt={'No'}
        leftButtonTxt={'Yes'}
        description={
          'Your offer is binding upon acceptance. Are you sure you want to proceed?'
        }
      />
    </>
  );
};

export default OfferDetails;
