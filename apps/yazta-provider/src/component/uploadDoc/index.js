import React, { useState } from 'react';
import Container from '../../../../../libs/ui/src/component/container';
import styles from './styles';
import Accordion from '../../../../../libs/ui/src/helper/accordion';
import ButtonPrimary from '../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import AccordianIn from '../../../../../libs/ui/src/helper/accordionIn';
import { useNavigation } from '@react-navigation/core';
import DocumentPicker from 'react-native-document-picker';
import { getUploadFileBody } from '../../../../../libs/ui/src/utils/uploadFile';
import { uploadDocFile } from '../../../../../libs/store/src/provider/user/action';
import { useDispatch, useSelector } from 'react-redux';

const UploadDoc = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [file, setFile] = useState(null);
  const [photoIdFile, setPhotoIdFile] = useState(null);
  const [backgroundCheckFile, setBackgroundCheckFile] = useState(null);
  const [referenceFile, setReferenceFile] = useState(null);
  const [resumeFile, setResumeFile] = useState(null);

  const photoIdUrl = useSelector((state) => state.core.photoId);
  const referenceUrl = useSelector((state) => state.core.reference);
  const resumeUrl = useSelector((state) => state.core.resume);
  const backgroundCheckUrl = useSelector(
    (state) => state.core.backgroundCheck
  );

  console.log('resumeUrl :', resumeUrl.image);
  console.log('photoIdUrl :', photoIdUrl.image);
  console.log('referenceUrl :', referenceUrl.image);
  console.log('backgroundCheckUrl :', backgroundCheckUrl.image);

  const onNavigate = () => {
    navigation.navigate('Profile');
  };

  const uploadPhotoId = (file, fileType) => {
    const id = 97;
    const formData = getUploadFileBody(file);
    alert(JSON.stringify(formData))
    if (fileType == 'photoId') {
      setPhotoIdFile(formData);
    } else if (fileType == 'reference') {
      setReferenceFile(formData);

    } else if (fileType == 'resume') {
      setResumeFile(formData);

    } else {
      setBackgroundCheckFile(formData);
    }
    // dispatch(uploadDocFile(formData, id, fileType));
    // dispatch(uploadFile(formData, id, fileType));
  };
  function onSubmit() {
    const id = 97;
    let request = {
      'files': photoIdFile ? photoIdFile : null,
      'resume': resumeFile ? resumeFile : null,
      'references': referenceFile ? referenceFile : null,
      'backgroundCheck': backgroundCheckFile ? backgroundCheckFile : null,
    }
    dispatch(uploadDocFile(request, id,navigation));
  }
  const selectFile = async (fileType) => {
    try {
      const res = await DocumentPicker.pick({
        type: [
          DocumentPicker.types.doc,
          DocumentPicker.types.docx,
          DocumentPicker.types.pdf,
          DocumentPicker.types.images,
        ],
      });
      console.log('res --->: ', res);
      setFile(res);
      uploadPhotoId(res, fileType);
    } catch (err) {
      setFile(null);
      if (DocumentPicker.isCancel(err)) {
        setFile(null);
      } else {
        alert('Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  return (
    <Container
      scrollEnabled
      showHeader
      showBack
      showMenu
      label="Upload Documents"
      extraStyles={styles.container}
    >
      <Accordion
        label="Upload Photo Id"
        check={photoIdUrl.image ? true : false}
        onPress={() => selectFile('photoId')}
      />
      {photoIdUrl.image && <AccordianIn label="Image1" check />}

      <Accordion
        label="Upload References"
        check={referenceUrl.image ? true : false}
        onPress={() => selectFile('reference')}
      />
      {referenceUrl.image && <AccordianIn label="Image1" check />}

      <Accordion
        label="Upload Resume"
        check={resumeUrl.image ? true : false}
        onPress={() => selectFile('resume')}
      />
      {resumeUrl.image && <AccordianIn label="Image1" check />}

      <Accordion
        label="Upload Background Check"
        check={backgroundCheckUrl.image ? true : false}
        onPress={() => selectFile('backgroundCheck')}
      />
      {backgroundCheckUrl.image && <AccordianIn label="Image1" check />}

      <ButtonPrimary
        label="Submit"
        extraStyle={styles.buttonStyle}
        onPress={() => {
          onSubmit()
          // AlertMessage('Your document was successfully uploaded.', onNavigate)
        }
        }
      />
    </Container>
  );
};

export default UploadDoc;
