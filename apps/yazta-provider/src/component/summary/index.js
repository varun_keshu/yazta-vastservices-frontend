import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import Box from '../../helper/box';
import DropDown from '../../../../../libs/ui/src/utils/dropdown';

const Summary = () => {
  const paymentList = [
    { id: '1', label: 'Credit card', value: 'Credit card' },
    { id: '2', label: 'UPI', value: 'UPI' },
  ];

  const onPicker = (value, field) => {
    console.log('field ---->:', field);
    console.log('value ---->:', value);
  };

  return (
    <View style={styles.container}>
      <Box
        title="Expected Pay Rate"
        iconName="dollar-sign"
        content="$ 50"
        subHeading="Weekly"
        iconType="FontAwesome5"
      />
      <Box content="$ 100" subHeading="Hourly" />
      <Box title="Payment Method" />
      <View style={styles.paymentType}>
        <DropDown data={paymentList} onPick={onPicker} field="address" />
      </View>
    </View>
  );
};

export default Summary;
