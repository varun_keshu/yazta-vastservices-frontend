import { StyleSheet } from 'react-native';
import { ratioHeight, ratioWidth } from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    marginBottom: 20 * ratioHeight,
  },
  subContainer: {
    marginBottom: 10 * ratioHeight,
  },
  paymentType: { marginLeft: 20 * ratioWidth },
});

export default styles;
