import { StyleSheet } from 'react-native';
import {
  colors,
  fonts,
  ratioWidth,
  ratioHeight,
} from '../../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {},
  subContainer: {
    alignItems: 'center',
  },
  extraStyle: {
    paddingHorizontal: 10 * ratioWidth,
    paddingVertical: 8 * ratioHeight,
    borderRadius: 4,
    marginBottom: 20 * ratioHeight,
    flexDirection: 'row',
  },
  labelStyle: {
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font13,
  },
});

export default styles;
