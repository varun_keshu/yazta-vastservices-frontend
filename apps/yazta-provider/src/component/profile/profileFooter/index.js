import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import ButtonPrimary from '../../../../../../libs/ui/src/helper/buttons/buttonPrimary';
import InputField from '../../../../../../libs/ui/src/helper/inputs/inputField';
import { AlertMessage } from '../../../../../../libs/ui/src/utils/alerts';
import { useNavigation } from '@react-navigation/core';
import {
  validateEmail,
  validateRequiredField,
} from '../../../../../../libs/ui/src/utils/validation';
import DocumentPick from '../../../../../../libs/ui/src/utils/documentPicker';
import DocumentPicker from 'react-native-document-picker';
import styles from './styles';
import { getUploadBody } from '../../../../../../libs/ui/src/utils/uploadFile';
import { uploadImage } from '../../../../../../libs/store/src/common/core/action';
import {
  updateProfile,
  getProfile,
} from '../../../../../../libs/store/src/provider/user/action';
import { useDispatch, useSelector } from 'react-redux';

const ProfileFooter = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const providerData = useSelector((state) => state.providerUser.providerData);

  useEffect(() => {
    dispatch(getProfile(providerData.id ? providerData.id : ''));
  }, []);

  const initialState = {
    name: providerData.name ? providerData.name : '',
    email: providerData.email ? providerData.email : '',
    age: providerData.age ? providerData.age : '',
    gender: providerData.gender ? providerData.gender : '',
    qualification: providerData.qualification ? providerData.qualification : '',
    skill: providerData.skill ? providerData.skill : '',
    address1: providerData.address1 ? providerData.address1 : '',
    address2: providerData.address2 ? providerData.address2 : '',
    phone: providerData.phoneNo ? providerData.phoneNo : '',
    summary: providerData.summary_bio ? providerData.summary_bio : '',
  };

  const [profileDetails, setProfileDetails] = useState(initialState);
  const [profileAvatar, setSingleFile] = useState(null);
  const [error, setError] = useState({ field: '', message: '' });

  const onSubmit = () => {
    const errorEntity = { ...onValidate() };
    if (errorEntity.field == 'profileAvatar') {
      AlertMessage(errorEntity.message);
    }
    if (Object.keys(errorEntity).length === 0) {
      const payload = {
        id: providerData.id ? providerData.id : '',
        name: profileDetails.name,
        email: profileDetails.email,
        age: profileDetails.age,
        gender: profileDetails.gender,
        qualification: profileDetails.qualification,
        skill: profileDetails.skill,
        address1: profileDetails.address1,
        address2: profileDetails.address2,
        phone: profileDetails.phone,
        summary_bio: profileDetails.summary,
      };
      dispatch(updateProfile(payload));
      setProfileDetails(initialState);
    }
  };

  const onValidate = () => {
    const validateEmailId = validateEmail(profileDetails.email);
    const validateSkill = validateRequiredField(
      'skill',
      profileDetails.skill,
      'Skill'
    );
    const validateProfile = validateRequiredField(
      'profileAvatar',
      profileAvatar,
      'Profile Image'
    );
    let errorEntity = {};

    if (validateProfile.message) {
      errorEntity = validateProfile;
    } else if (validateEmailId.message) {
      errorEntity = validateEmailId;
    } else if (validateSkill.message) {
      errorEntity = validateSkill;
    }
    setError(errorEntity);
    return errorEntity;
  };

  const onChange = (field, value) => {
    setProfileDetails({ ...profileDetails, [field]: value });
  };

  const uploadProfile = (profileAvatar) => {
    const id = 97;
    const formData = getUploadBody(profileAvatar);
    dispatch(uploadImage(formData, id));
  };

  const selectFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });
      console.log('res --->: ', res);
      setSingleFile(res);
      uploadProfile(res);
    } catch (err) {
      setSingleFile(null);
      if (DocumentPicker.isCancel(err)) {
        setSingleFile(null);
      } else {
        alert('Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  return (
    <>
      <View style={styles.subContainer}>
        <DocumentPick selectFile={selectFile} profileAvatar={profileAvatar} />
        <ButtonPrimary
          label="Upload documents"
          extraStyle={styles.extraStyle}
          labelStyle={styles.labelStyle}
          showIcon
          iconType="MaterialIcons"
          iconName="cloud-upload"
          onPress={() => navigation.navigate('UploadDocument')}
        />
      </View>
      <InputField
        placeholder="Enter Name"
        value={profileDetails.name}
        onChange={(value) => onChange('name', value)}
        fieldName="Name"
      />
      <InputField
        placeholder="Enter Email"
        fieldName="Email"
        value={profileDetails.email}
        onChange={(value) => onChange('email', value)}
        error={error.field === 'email' && error.message}
        astric
      />
      <InputField
        placeholder="Enter Age"
        fieldName="Age"
        keyboardType="numeric"
        value={profileDetails.age}
        onChange={(value) => onChange('age', value)}
      />
      <InputField
        placeholder="Enter Gender"
        value={profileDetails.gender}
        onChange={(value) => onChange('gender', value)}
        fieldName="Gender"
      />
      <InputField
        placeholder="Enter Qualification"
        value={profileDetails.qualification}
        onChange={(value) => onChange('qualification', value)}
        fieldName="Qualification"
      />
      <InputField
        placeholder="Enter Skill"
        fieldName="Skill"
        value={profileDetails.skill}
        onChange={(value) => onChange('skill', value)}
        error={error.field === 'skill' && error.message}
        astric
      />
      <InputField
        placeholder="Enter Address"
        value={profileDetails.address1}
        onChange={(value) => onChange('address1', value)}
        fieldName="Address"
      />
      <InputField
        placeholder="Enter Address2"
        value={profileDetails.address2}
        onChange={(value) => onChange('address2', value)}
        fieldName="Address2"
      />
      <InputField
        placeholder="Enter Phone"
        fieldName="Phone"
        keyboardType="numeric"
        value={profileDetails.phone}
        onChange={(value) => onChange('phone', value)}
      />
      <InputField
        placeholder="Enter Summary"
        value={profileDetails.summary}
        onChange={(value) => onChange('summary', value)}
        fieldName="Summary / Bio"
      />
      <ButtonPrimary label="Submit" onPress={() => onSubmit()} />
    </>
  );
};

export default ProfileFooter;
