import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import Toggle from '../../../../../../libs/ui/src/helper/toggle';
import { useNavigation } from '@react-navigation/core';

const ProfileHeader = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.subContainer}>
      <Toggle
        label1="Personal Details"
        label2="Job Preferences"
        onPress={() => navigation.navigate('JobPreference')}
        profile
      />
    </View>
  );
};

export default ProfileHeader;
