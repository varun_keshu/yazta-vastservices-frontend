import React from 'react';
import { View, Text } from 'react-native';
import Grid from '../../helper/grid';
import styles from './styles';

const Skills = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Select Your Skills</Text>
      <Grid />
      <Text style={styles.footer}>Check all that apply</Text>
    </View>
  );
};

export default Skills;
