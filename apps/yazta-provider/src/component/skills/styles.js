import { StyleSheet } from 'react-native';
import { colors, ratioHeight, fonts } from '../../../../../libs/ui/src/themes';

const styles = StyleSheet.create({
  container: {
    marginTop: 15 * ratioHeight,
  },
  header: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font18,
    textAlign: 'center',
    marginBottom: 10 * ratioHeight,
  },
  footer: {
    color: colors.pagination,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font14,
    textAlign: 'center',
    marginVertical: 10 * ratioHeight,
  },
});

export default styles;
