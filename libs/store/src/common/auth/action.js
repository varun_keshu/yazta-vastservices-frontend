import * as Constant from '../constant';
import {
  userLogin,
  userRegistration,
  userOtpVerification,
  forgotPassword,
  resetPassword,
  googleLogin,
} from './service';
import { AlertMessage } from '../../../ui/src/utils/flashMessage';
import {
  goToScreen,
  replaceStacks,
  appStackReset,
} from '../../../../apps/yazta-provider/src/navigation/navigationHelper';
import {
  goToScreen as navigateScreen,
  replaceStack,
  app2StackReset,
} from '../../../../apps/yazta-client/src/navigation/navigationHelper';
import asyncStore from '../../asyncStore';

export const loader = (payload) => {
  return {
    type: Constant.SHOW_LOADER,
    payload,
  };
};

export const saveStoreToken = (data) => {
  return (dispatch) => {
    dispatch({
      type: Constant.SAVE_STORE_TOKEN,
      payload: data,
    });
  };
};

export const loginUser = (data) => {
  return (dispatch) => {
    dispatch(loader(true));
    userLogin({ identifier: data.email, password: data.password })
      .then(([response, status]) => {
        console.log('response here login : ', response);
        AlertMessage('User logged in sucessfully!');
        if (status === 200) {
          asyncStore.setToken(response.jwt);
          if (response.user.userRole === 'CUSTOMER') {
            asyncStore.setClientData(response.user);
            app2StackReset('HomeStack');
          } else {
            asyncStore.setProviderData(response.user);
            appStackReset('HomeStack');
          }
        }
        dispatch(loader(false));
        dispatch({
          type: Constant.USER_LOGIN,
          payload: response.user,
        });
      })
      .catch((error) => {
        console.log('error :', error);
        dispatch(loader(false));
        console.log('error :', error?.data[0]?.messages[0]?.message);
        AlertMessage('API Error!');
        // AlertMessage(
        //   error?.data[0]?.messages[0]?.message
        //     ? error?.data[0]?.messages[0]?.message
        //     : 'Api error'
        // );
      });
  };
};

export const registerUser = (data) => {
  return (dispatch) => {
    dispatch(loader(true));
    userRegistration(data)
      .then((response) => {
        console.log('response here registration', response);
        AlertMessage('Please verify your phone number!');
        dispatch(loader(false));
        if (data?.userRole === 'CUSTOMER') {
          navigateScreen('OtpVerification', {
            value: data.phoneNo,
            type: 'register',
          });
        } else {
          goToScreen('OtpVerification', {
            value: data.phoneNo,
            type: 'register',
          });
        }
      })
      .catch((error) => {
        console.log('error :', error);
        dispatch(loader(false));
        AlertMessage(error?.message ? error?.message : 'Api error');
        if (error.message == 'User Already exist!') {
          if (data?.userRole === 'CUSTOMER') {
            navigateScreen('Login');
          } else {
            goToScreen('Login');
          }
        }
      });
  };
};

export const forgetPassword = (data) => {
  console.log(' forgetPassword data :', data);
  return (dispatch) => {
    dispatch(loader(true));
    forgotPassword(data)
      .then(([response, status]) => {
        console.log('response here forgot Password', response);
        AlertMessage('Mail Sent!');

        if (status === 200) {
          goToScreen('ResetPassword');
          navigateScreen('ResetPassword');
        }
        dispatch(loader(false));
      })
      .catch((error) => {
        dispatch(loader(false));
        console.log('error :', error);
        AlertMessage('Email not valid!');
        // showToast(
        //   'danger',
        //   error?.message
        //     ? error?.message
        //     : error?.data[0]?.messages[0]?.message
        //     ? error?.data[0]?.messages[0]?.message
        //     : 'Api error'
        // );
      });
  };
};

export const changePassword = (data) => {
  console.log('changePassword data :', data);
  return (dispatch) => {
    dispatch(loader(true));
    resetPassword(data)
      .then(([response, status]) => {
        console.log('response here change Password', response);
        AlertMessage('Reset Password Successfully!');
        dispatch(loader(false));
        if (status === 200) {
          goToScreen('Login');
          navigateScreen('Login');
        }
      })
      .catch((error) => {
        dispatch(loader(false));
        console.log('error :', error?.data[0]?.messages[0]?.message);
        // showToast(
        //   'danger',
        //   error?.data[0]?.messages[0]?.message
        //     ? error?.data[0]?.messages[0]?.message
        //     : 'Invalid Code'
        // );
        AlertMessage(
          error?.data[0]?.messages[0]?.message
            ? error?.data[0]?.messages[0]?.message
            : 'Invalid Code'
        );
      });
  };
};

export const verifyOtp = (data) => {
  return (dispatch) => {
    dispatch(loader(true));
    userOtpVerification(data)
      .then((response) => {
        console.log('response here registration', response);
        AlertMessage('You have sucessfully verified!');
        navigateScreen('Login');
        goToScreen('Login');
        dispatch(loader(false));
      })
      .catch((error) => {
        dispatch(loader(false));
        AlertMessage(error?.message ? error?.message : 'Api error');
      });
  };
};

export const googleLoginUser = (data) => {
  return (dispatch) => {
    dispatch(loader(true));
    googleLogin({ accessToken: data.token, role: data.role })
      .then(([response, status]) => {
        console.log('response here google login : ', response);
        if (status === 201) {
          asyncStore.setToken(response.token);
          if (response.userRole === 'CUSTOMER' && data.role === 'CUSTOMER') {
            AlertMessage('User logged in sucessfully!');
            app2StackReset('HomeStack');
            dispatch({ type: Constant.USER_CLIENT_LOGIN, payload: response });
          } else if (
            response.userRole === 'PROVIDER' &&
            data.role === 'PROVIDER'
          ) {
            AlertMessage('User logged in sucessfully!');
            appStackReset('HomeStack');
            dispatch({ type: Constant.USER_LOGIN, payload: response.user });
          } else {
            AlertMessage(`You're already registered as ${response.userRole}.`);
          }
        }
        dispatch(loader(false));
      })
      .catch((error) => {
        console.log('error :', error);
        dispatch(loader(false));
        AlertMessage('API Error!');
        // AlertMessage(
        //   error?.data[0]?.messages[0]?.message
        //     ? error?.data[0]?.messages[0]?.message
        //     : 'Api error'
        // );
      });
  };
};

export const logoutUser = () => {
  return (dispatch) => {
    try {
      dispatch(loader(true));
      AlertMessage('Logout Successful!');
      asyncStore.removeDataBeforeLogout();
      app2StackReset('Login');
      dispatch(loader(false));
      dispatch({
        type: Constant.USER_LOGOUT,
        payload: '',
      });
    } catch (error) {
      console.log('logout user error', error);
      dispatch(loader(false));
    }
  };
};
