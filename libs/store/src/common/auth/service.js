import apiMethod from '../apiMethod';

export const userLogin = ({ identifier, password }) => {
  return apiMethod.post('auth/local', { identifier, password });
};

export const userRegistration = ({
  email,
  password,
  name,
  phoneNo,
  userRole,
}) => {
  return apiMethod.post('auth/local/register', {
    email,
    password,
    name,
    phoneNo,
    userRole,
  });
};

export const forgotPassword = ({ email }) => {
  return apiMethod.post('auth/forgot-password', { email });
};

export const resetPassword = (data) => {
  return apiMethod.post('auth/reset-password', data);
};

export const userOtpVerification = ({ phoneNo, otp }) => {
  return apiMethod.post('users-permissions/auth/verify-otp', { phoneNo, otp });
};

export const googleLogin = (data) => {
  return apiMethod.post('auth/google/register', data);
};
