import * as Constant from '../constant';

const initialState = {
  loader: false,
  photoId: {},
  reference: {},
  resume: {},
  backgroundCheck: {},
  testimonialList: [],
  serviceList: [],
};

const core = (state = initialState, action) => {
  switch (action.type) {
    case Constant.SHOW_LOADER:
      return {
        ...state,
        loader: action.payload,
      };
    case Constant.UPLOAD_PHOTO_ID:
      return {
        ...state,
        photoId: action.payload,
      };
    case Constant.UPLOAD_REFERNCE:
      return {
        ...state,
        reference: action.payload,
      };
    case Constant.UPLOAD_RESUME:
      return {
        ...state,
        resume: action.payload,
      };
    case Constant.UPLOAD_BACKGROUND_CHECK:
      return {
        ...state,
        backgroundCheck: action.payload,
      };
    case Constant.TESTIMONIAL_LIST:
      return {
        ...state,
        testimonialList: action.payload,
      };
    case Constant.SERVICE_LIST:
      return {
        ...state,
        serviceList: action.payload,
      };
    default:
      return state;
  }
};

export default core;
