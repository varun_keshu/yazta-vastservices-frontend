import apiMethod from '../apiMethod';

const Token = `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OTYsImlhdCI6MTY0MDk0Mjg3MCwiZXhwIjoxNjQzNTM0ODcwfQ.FzNEgNups5nWxfs_2PcgIH5FYU2owEhCcbLGkB3TY1M`;

export const testimonialList = () => {
  return apiMethod.get('testimonials');
};

export const uploadDocuments = (data, id) => {
  return apiMethod.post(`users/upload/${id}/`, data, null, Token);
};

export const profileDetail = (id) => {
  return apiMethod.get('provider-profiles/' + id);
};

export const profileUpdate = (data, id) => {
  return apiMethod.put(`provider-profiles/${id}`, data, null, Token);
};

export const servicesList = () => {
  return apiMethod.get('services');
};