import * as Constant from '../../constant';
import { testimonialList, uploadDocuments, servicesList } from './service';
import { AlertMessage } from '../../../ui/src/utils/flashMessage';
import { goToScreen as navigateScreen } from '../../../../apps/yazta-client/src/navigation/navigationHelper';
import { goToScreen } from '../../../../apps/yazta-provider/src/navigation/navigationHelper';

export const loader = (payload) => {
  return {
    type: Constant.SHOW_LOADER,
    payload,
  };
};

export const testimonialListApi = () => {
  return (dispatch) => {
    dispatch(loader(true));
    testimonialList()
      .then(([response, status]) => {
        if (status == 200) {
          dispatch({
            type: Constant.TESTIMONIAL_LIST,
            payload: response,
          });
        }
      })
      .catch((err) => {
        console.log('err,', err);
        AlertMessage(err);
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};

export const profileDetailApi = () => {
  return (dispatch) => {
    dispatch(loader(true));
    profileDetailApi()
      .then(([response, status]) => {
        if (status == 200) {
          dispatch({
            type: Constant.TESTIMONIAL_LIST,
            payload: response,
          });
        }
      })
      .catch((err) => {
        console.log('err,', err);
        AlertMessage(err);
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};

export const uploadImage = (data, id) => {
  return (dispatch) => {
    dispatch(loader(true));
    uploadDocuments(data, id)
      .then(([response, status]) => {
        console.log('response', response);
        if (status === 201) {
          AlertMessage('Your profile Image uploaded successfully!');
          // if (response.userRole === 'CUSTOMER') {
          //   navigateScreen('Congratulation');
          // } else {
          //   goToScreen('Congratulation');
          // }
        }
      })
      .catch((err) => {
        AlertMessage(err);
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};

export const uploadFile = (data, id, fileType) => {
  return (dispatch) => {
    dispatch(loader(true));
    uploadDocuments(data, id)
      .then(([response, status]) => {
        console.log('response', response);
        if (status === 201) {
          AlertMessage('Your file uploaded successfully!');
        }
        dispatch(loader(false));
        if (fileType == 'photoId') {
          dispatch({
            type: Constant.UPLOAD_PHOTO_ID,
            payload: response,
          });
        } else if (fileType == 'reference') {
          dispatch({
            type: Constant.UPLOAD_REFERNCE,
            payload: response,
          });
        } else if (fileType == 'resume') {
          dispatch({
            type: Constant.UPLOAD_RESUME,
            payload: response,
          });
        } else {
          dispatch({
            type: Constant.UPLOAD_BACKGROUND_CHECK,
            payload: response,
          });
        }
      })
      .catch((err) => {
        AlertMessage(err);
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};

export const deleteFile = (data, id, fileType) => {
  return (dispatch) => {
    dispatch(loader(true));
    uploadDocuments(data, id)
      .then(([response, status]) => {
        console.log('response', response);
        if (status === 201) {
          AlertMessage('Your file uploaded successfully!');
        }
        dispatch(loader(false));
        if (fileType == 'photoId') {
          dispatch({
            type: Constant.UPLOAD_PHOTO_ID,
            payload: response,
          });
        } else if (fileType == 'reference') {
          dispatch({
            type: Constant.UPLOAD_REFERNCE,
            payload: response,
          });
        } else if (fileType == 'resume') {
          dispatch({
            type: Constant.UPLOAD_RESUME,
            payload: response,
          });
        } else {
          dispatch({
            type: Constant.UPLOAD_BACKGROUND_CHECK,
            payload: response,
          });
        }
      })
      .catch((err) => {
        AlertMessage(err);
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};

export const getServiceList = () => {
  return (dispatch) => {
    dispatch(loader(true));
    servicesList()
      .then(([response, status]) => {
        // console.log('getServiceList response :', response);
        if (status == 200) {
          dispatch({
            type: Constant.SERVICE_LIST,
            payload: response,
          });
        }
        dispatch(loader(false));
      })
      .catch((err) => {
        dispatch(loader(false));
        console.log('err,', err);
        AlertMessage("API Error!");
      })
      
  };
};