import * as Constant from '../../constant';

const initialState = {
  arrangement: '',
  time: '',
  skills: '',
  recurring: '',
};

const booking = (state = initialState, action) => {
  switch (action.type) {
    case Constant.SET_ARRANGEMENT:
      return {
        ...state,
        arrangement: action.payload,
      };
    case Constant.SET_TIME:
      return {
        ...state,
        time: action.payload,
      };
    case Constant.SET_SKILLS:
      return {
        ...state,
        skills: action.payload,
      };
    case Constant.SET_RECURRING:
      return {
        ...state,
        recurring: action.payload,
      };
    default:
      return state;
  }
};

export default booking;
