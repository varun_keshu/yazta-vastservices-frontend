import * as Constant from '../../constant';

export const setArrangement = (payload) => {
  return {
    type: Constant.SET_ARRANGEMENT,
    payload,
  };
};

export const setTime = (payload) => {
  return {
    type: Constant.SET_TIME,
    payload,
  };
};

export const setSkills = (payload) => {
  return {
    type: Constant.SET_SKILLS,
    payload,
  };
};

export const setRecurring = (payload) => {
  return {
    type: Constant.SET_RECURRING,
    payload,
  };
};
