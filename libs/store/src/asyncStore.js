import AsyncStorage from '@react-native-async-storage/async-storage';

export default class GlobalAPI {
  Token = '';
  // UserData = {};
  ProviderData = {};
  ClientData = {};

  static setToken = async (token) => {
    console.log('token :', token);
    try {
      this.Token = token;
      await AsyncStorage.setItem('Token', JSON.stringify(token));
      console.log('Token saved successfully.');
    } catch (error) {
      console.log('Failed to save token in storage.', error);
    }
  };

  static getToken = async () => {
    try {
      const authTokenString = await AsyncStorage.getItem('Token');
      if (authTokenString != null) {
        const authToken = JSON.parse(authTokenString || '');
        this.Token = authToken;
        return this.Token;
      } else {
        return '';
      }
    } catch (error) {
      console.log('Failed to get token from storage.', error);
    }
  };

  // static setUserData = async (data) => {
  //   try {
  //     await AsyncStorage.setItem('userData', JSON.stringify(data));
  //     console.log('User Data Saved Successfully.');
  //   } catch (error) {
  //     console.log('Failed to save User Data in storage.', error);
  //   }
  // };

  // static getUserData = async () => {
  //   try {
  //     const authData = await AsyncStorage.getItem('userData');
  //     this.UserData = JSON.parse(authData);
  //     return this.UserData;
  //   } catch (error) {
  //     console.log('Failed to get User Data from storage.', error);
  //   }
  // };

  static setClientData = async (data) => {
    try {
      await AsyncStorage.setItem('clientData', JSON.stringify(data));
      console.log('Client Data Saved Successfully.');
    } catch (error) {
      console.log('Failed to save Client Data in storage.', error);
    }
  };

  static getClientData = async () => {
    try {
      const authData = await AsyncStorage.getItem('clientData');
      this.ClientData = JSON.parse(authData);
      return this.ClientData;
    } catch (error) {
      console.log('Failed to get Client Data from storage.', error);
    }
  };

  static setProviderData = async (data) => {
    try {
      await AsyncStorage.setItem('providerData', JSON.stringify(data));
      console.log('Provider Data Saved Successfully.');
    } catch (error) {
      console.log('Failed to save Provider Data in storage.', error);
    }
  };

  static getProviderData = async () => {
    try {
      const authData = await AsyncStorage.getItem('providerData');
      this.ProviderData = JSON.parse(authData);
      return this.ProviderData;
    } catch (error) {
      console.log('Failed to get Provider Data from storage.', error);
    }
  };

  static removeDataBeforeLogout = async () => {
    const keys = ['Token', 'providerData', 'clientData'];
    try {
      await AsyncStorage.multiRemove(keys);
      this.providerData = {};
      this.clientData = {};
      this.Token = '';
    } catch (e) {
      console.log('error async logout');
    }
  };
}
