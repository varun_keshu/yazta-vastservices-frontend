import { combineReducers } from 'redux';
import auth from './common/auth/reducer';
import core from './common/core/reducer';
import booking from './common/booking/reducer';
import clientUser from './client/user/reducer';
import providerUser from './provider/user/reducer';
import clientBooking from './client/booking/reducer';

const appReducer = combineReducers({
  auth,
  core,
  booking,
  clientUser,
  providerUser,
  clientBooking,
});

export default appReducer;
