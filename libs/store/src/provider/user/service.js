import apiMethod from '../apiMethod';

export const profileUpdate = (id, data) => {
  return apiMethod.put(`users/${id}`, data);
};

export const getProviderData = (id) => {
  return apiMethod.get(`users/${id}`);
};

export const uploadDocuments = (data, id) => {
  return apiMethod.post(`documents/${id}/`, data);
};
