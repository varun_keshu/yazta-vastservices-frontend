import * as Constant from '../../constant';

const initialState = {
  providerData: {},
  documentData:null,
  reference: {},
  resume: {},
  backgroundCheck: {},
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case Constant.SAVE_PROVIDER_DATA:
      return {
        ...state,
        providerData: action.payload,
      };

      case Constant.UPLOAD_DOCUMENT:
      return {
        ...state,
        documentData: action.payload,
      };
    
    default:
      return state;
  }
};

export default user;
