import * as Constant from '../../constant';
import { profileUpdate, getProviderData, uploadDocuments } from './service';
import { AlertMessage } from '../../../ui/src/utils/flashMessage';
import { loader } from '../../common/auth/action';
import asyncStore from '../../asyncStore';

export const saveProviderData = (data) => {
  return (dispatch) => {
    dispatch({
      type: Constant.SAVE_PROVIDER_DATA,
      payload: data,
    });
  };
};

export const updateProfile = (data) => {
  const id = data.id;
  return (dispatch) => {
    dispatch(loader(true));
    profileUpdate(id, data)
      .then(([response, status]) => {
        console.log('updateProfile response -->:', response);
        if (status == 200) {
          AlertMessage('Your Profile update successfully!');
          asyncStore.setProviderData(response);
        }
      })
      .catch((err) => {
        console.log('updateProfile err,', err);
        AlertMessage('API Error');
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};

export const getProfile = (id) => {
  return (dispatch) => {
    dispatch(loader(true));
    getProviderData(id)
      .then(([response, status]) => {
        console.log('getProfile response -->:', response);
        if (status == 200) {
          console.log('Provider Profile get successfully!');
          asyncStore.setProviderData(response);
        }
      })
      .catch((err) => {
        console.log('getProfile err,', err);
        AlertMessage('API Error');
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};

export const uploadDocFile = (data, id, navigation) => {
  return (dispatch) => {
    dispatch(loader(true));
    uploadDocuments(data, id)
      .then(([response, status]) => {
        console.log('response', response);
        if (status === 201) {
          AlertMessage('Your file uploaded successfully!');
          navigation.navigate('Profile');
          dispatch({
            type: Constant.UPLOAD_DOCUMENT,
            payload: response,
          });
        }
        dispatch(loader(false));
      })
      .catch((err) => {
        AlertMessage(err);
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};
