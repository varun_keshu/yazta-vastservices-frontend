import * as Constant from '../../constant';

const initialState = {
  currentJobsList: [],
  newOffersList: [],
  jobHistoryList: [],
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case Constant.CURRENT_JOBS_LIST:
      return {
        ...state,
        currentJobsList: action.payload,
      };
    case Constant.NEW_OFFERS_LIST:
      return {
        ...state,
        newOffersList: action.payload,
      };
    case Constant.JOB_HISTORY_LIST:
      return {
        ...state,
        jobHistoryList: action.payload,
      };
    default:
      return state;
  }
};

export default user;
