import * as Constant from '../../constant';
import { currentJobs, newOffers, jobHistory } from './service';
import { AlertMessage } from '../../../ui/src/utils/flashMessage';
import { loader } from '../../common/auth/action';

export const getCurrentJobs = () => {
  return (dispatch) => {
    dispatch(loader(true));
    currentJobs()
      .then(([response, status]) => {
        console.log('getCurrentJobs response -->:', response);
        if (status == 200) {
           dispatch({
             type: Constant.CURRENT_JOBS_LIST,
             payload: data,
           });
        }
      })
      .catch((err) => {
          console.log('getCurrentJobs err,', err);
          AlertMessage('API Error');
          dispatch(loader(false));
      })
  };
};

export const getJobHistory = () => {
  return (dispatch) => {
    dispatch(loader(true));
    jobHistory()
      .then(([response, status]) => {
        console.log('getJobHistory response -->:', response);
        if (status == 200) {
          dispatch({
            type: Constant.JOB_HISTORY_LIST,
            payload: data,
          });
        }
      })
      .catch((err) => {
        console.log('getJobHistory err,', err);
        AlertMessage('API Error');
        dispatch(loader(false));
      });
  };
};

export const getNewOffers = () => {
  return (dispatch) => {
    dispatch(loader(true));
    newOffers()
      .then(([response, status]) => {
        console.log('getNewOffers response -->:', response);
        if (status == 200) {
          dispatch({
            type: Constant.NEW_OFFERS_LIST,
            payload: data,
          });
        }
      })
      .catch((err) => {
        console.log('getNewOffers err,', err);
        AlertMessage('API Error');
        dispatch(loader(false));
      });
  };
};
