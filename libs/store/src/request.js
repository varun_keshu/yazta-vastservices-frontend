import axios from 'axios';
import { apiUrl } from './config';
import GlobalAPI from '../../store/src/asyncStore';

export default class Request {
  constructor(headers) {
    this.http = axios.create({
      baseURL: apiUrl,
      withCredentials: true,
    });

    this.http.interceptors.request.use((config) => {
      config.headers = {
        Authorization: GlobalAPI.Token ? `Bearer ${GlobalAPI.Token}` : '',
        'Content-Type': 'application/json',
        ...headers,
      };
      return config;
    });

    this.http.interceptors.response.use(
      function (response) {
        // console.log('resp here', response);
        return [response.data, response.status];
      },
      function (error) {
        if (error.response) {
          // console.log('here error here', error.response);
          if (error.response.status && error.response.status === 403) {
            // resetStack('login');
            return Promise.reject(error.response.data);
          } else {
            return Promise.reject(error.response.data);
          }
        } else if (error.request) {
          return Promise.reject(error.request);
        } else {
          return Promise.reject(error);
        }
      }
    );
    for (const method of ['get', 'post', 'put', 'delete']) {
      this[method] = this.http[method];
    }
  }
}
