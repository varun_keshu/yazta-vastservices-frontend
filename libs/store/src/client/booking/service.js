import apiMethod from '../../apiMethod';

export const providerDetail = (id) => {
  return apiMethod.get(`/users/${id}`);
};

export const findBestProvider = (data) => {
  return apiMethod.get(`/users?userRole=PROVIDER&${data}`);
}
