import * as Constant from '../../constant';

const initialState = {
  providerDetail: [],
  providerList: [],
  loader: false,
};

const clientBooking = (state = initialState, action) => {
  switch (action.type) {
    case Constant.PROVIDER_DETAIL:
      return {
        ...state,
        providerDetail: action.payload,
      };
    case Constant.PROVIDER_LIST:
      return {
        ...state,
        providerList: action.payload,
      };
    default:
      return state;
  }
};

export default clientBooking;
