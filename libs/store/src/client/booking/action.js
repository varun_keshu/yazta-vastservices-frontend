import * as Constant from '../../constant';
import { providerDetail, findBestProvider } from './service';
import { AlertMessage } from '../../../ui/src/utils/flashMessage';
import { loader } from '../../common/auth/action';

export const getProviderDetail = (id) => {
  return (dispatch) => {
    dispatch(loader(true));
    providerDetail(id)
      .then(([response, status]) => {
        console.log('getProviderDetail response -->:', response);
        if (status == 200) {
          dispatch({
            type: Constant.PROVIDER_DETAIL,
            payload: response,
          });
        }
      })
      .catch((err) => {
        console.log('getProviderDetail err,', err);
        AlertMessage('API Error');
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};

export const findProvider = (data) => {
const queryString = require('query-string');
const payload = queryString.stringify(data, { encode: false });
  return (dispatch) => {
    dispatch(loader(true));
    findBestProvider(payload)
      .then(([response, status]) => {
        // console.log('findBestProvider response -->:', response);
        if (status == 201) {
          dispatch({
            type: Constant.PROVIDER_LIST,
            payload: response,
          });
        }
      })
      .catch((err) => {
        console.log('findBestProvider err,', err);
        AlertMessage('API Error');
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};
