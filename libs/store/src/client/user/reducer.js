import * as Constant from '../../constant';

const initialState = {
  user: [],
  loader: false,
  clientData: {},
};

const clientUser = (state = initialState, action) => {
  switch (action.type) {
    case Constant.USER_DATA:
      return {
        ...state,
        userData: action.payload,
      };
    case Constant.SHOW_LOADER:
      return {
        ...state,
        loader: action.payload,
      };
    case Constant.SAVE_CLIENT_DATA:
      return {
        ...state,
        clientData: action.payload,
      };
    default:
      return state;
  }
};

export default clientUser;
