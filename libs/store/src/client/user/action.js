import * as Constant from '../../constant';
import { updateProfile, getClientData } from './service';
import asyncStore from '../../asyncStore';
import { AlertMessage } from '../../../../ui/src/utils/flashMessage';
import { goToScreen as navigateScreen } from '../../../../../apps/yazta-client/src/navigation/navigationHelper';

export const loader = (payload) => {
  return {
    type: Constant.SHOW_LOADER,
    payload,
  };
};

export const saveClientData = (data) => {
  return (dispatch) => {
    dispatch({
      type: Constant.SAVE_CLIENT_DATA,
      payload: data,
    });
  };
};

const onNavigate = () => {
  navigateScreen('Congratulation');
};

export const updateProfileAction = (request) => {
  let id = request.id;
  return (dispatch) => {
    dispatch(loader(true));
    updateProfile(id, request)
      .then(([response, status]) => {
        console.log('response here updateProfile', response);
        if (status === 200) {
          AlertMessage('Your Profile update successfully!');
          dispatch({
            type: Constant.SAVE_CLIENT_DATA,
            payload: response,
          });
          asyncStore.setClientData(response);
          AlertMessage('Your profile updated successfully.', onNavigate);
        }
        dispatch(loader(false));
      })
      .catch((error) => {
        console.log('error :', error);
        dispatch(loader(false));
        AlertMessage('Api error');
      });
  };
};

export const getProfile = (id) => {
  return (dispatch) => {
    dispatch(loader(true));
    getClientData(id)
      .then(([response, status]) => {
        console.log('getProfile Client response -->:', response);
        if (status == 200) {
          console.log('Client Profile get successfully!');
          asyncStore.setClientData(response);
        }
      })
      .catch((err) => {
        console.log('getProfile err,', err);
        AlertMessage('API Error');
      })
      .finally(() => {
        dispatch(loader(false));
      });
  };
};
