import apiMethod from '../../apiMethod';

export const updateProfile = (id, data) => {
  return apiMethod.put(`users/${id}`, data);
};

export const getClientData = (id) => {
  return apiMethod.get(`users/${id}`);
};
