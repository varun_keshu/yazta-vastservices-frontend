import { StyleSheet } from 'react-native';
import { colors, fonts, ratioHeight, ratioWidth } from '../../../themes';

const styles = StyleSheet.create({
  container: {
    borderBottomColor: colors.appDark,
    borderBottomWidth: 0.4,
    alignItems: 'center',
    marginTop: 20 * ratioHeight,
    marginHorizontal: 30 * ratioWidth,
  },
  subContainer: {
    backgroundColor: colors.appSecondary,
    paddingHorizontal: 10 * ratioWidth,
    paddingVertical: 10 * ratioHeight,
    marginBottom: -20,
    alignItems: 'center',
  },
  signUp: {
    fontSize: fonts.size.font13,
    fontFamily: fonts.type.montserratMedium,
    color: colors.textPrimary,
  },
  login: {
    fontSize: fonts.size.font13,
    fontFamily: fonts.type.montserratSemiBold,
    color: colors.pagination,
  },
  member: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

export default styles;
