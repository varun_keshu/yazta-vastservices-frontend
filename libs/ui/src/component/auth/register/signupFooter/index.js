import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { View, Text, TouchableOpacity } from 'react-native';
import SocialLogin from '../socialLogin';
import styles from './styles';

const SignupFooter = (props) => {
  const { label, tag, onNavigate, userRole } = props;
  const navigation = useNavigation();
  return (
    <>
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <Text style={styles.signUp}>OR SIGN UP WITH</Text>
        </View>
      </View>
      <SocialLogin userRole={userRole} />
      <View style={styles.member}>
        <Text style={styles.signUp}>{label}</Text>
        <TouchableOpacity onPress={onNavigate}>
          <Text style={styles.login}>{tag}</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default SignupFooter;
