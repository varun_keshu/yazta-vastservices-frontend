import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { colors } from '../../../themes';
import Icon from '../../../helper/icon/icons';
import styles from './styles';
import { googleLoginUser } from '../../../../../../store/src/common/auth/action';
import { useDispatch } from 'react-redux';
import { authorize } from 'react-native-app-auth';
// import OAuthManager from 'react-native-oauth';
// import { LoginButton, AccessToken } from 'react-native-fbsdk';

const SocialLogin = (props) => {
  const { userRole } = props;
  const dispatch = useDispatch();

  const openGoogle = async () => {
    let config;
    if (userRole === 'CUSTOMER') {
      config = {
        issuer: 'https://accounts.google.com',
        clientId:
          '338546627001-6vbjeudg6q64ckgvkoal8oqevev9eojm.apps.googleusercontent.com',
        redirectUrl:
          'com.googleusercontent.apps.338546627001-6vbjeudg6q64ckgvkoal8oqevev9eojm:/oauth2redirect/google',
        scopes: ['openid', 'profile', 'email'],
      };
    } else {
      config = {
        issuer: 'https://accounts.google.com',
        clientId:
          '338546627001-1m6n1iod5kk34pu4cuek8ctq2cjnjufp.apps.googleusercontent.com',
        redirectUrl:
          'com.googleusercontent.apps.338546627001-1m6n1iod5kk34pu4cuek8ctq2cjnjufp:/oauth2redirect/google',
        scopes: ['openid', 'profile', 'email'],
      };
    }

    const authState = await authorize(config);
    const token = authState.accessToken;
    return token;
  };

  const googleLogin = async () => {
    const access_token = await openGoogle();
    if (access_token) {
      dispatch(
        googleLoginUser({
          token: access_token,
          role: userRole,
        })
      );
    }
  };

  const fbLogin = async () => {};

  return (
    <View style={styles.container}>
      {/* <LoginButton
        onLoginFinished={(error, result) => {
          if (error) {
            console.log('login has error: ' + result.error);
          } else if (result.isCancelled) {
            console.log('login is cancelled.');
          } else {
            AccessToken.getCurrentAccessToken().then((data) => {
              console.log('data :', data);
              console.log(data.accessToken.toString());
            });
          }
        }}
        onLogoutFinished={() => console.log('logout.')}
      /> */}

      <TouchableOpacity style={styles.fbContainer} onPress={fbLogin}>
        <Icon type="Entypo" name="facebook" size={20} color={colors.fbColor} />
        <Text style={styles.facebook}>Facebook</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.gContainer} onPress={googleLogin}>
        <Icon
          type="MaterialCommunityIcons"
          name="gmail"
          size={20}
          color={colors.gmail}
        />
        <Text style={styles.gmail}>Gmail</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SocialLogin;
