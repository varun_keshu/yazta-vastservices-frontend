import { StyleSheet } from 'react-native';
import { colors, fonts, ratioHeight, ratioWidth } from '../../../themes';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 25 * ratioHeight,
  },
  fbContainer: {
    flexDirection: 'row',
    borderBottomColor: colors.fbColor,
    borderBottomWidth: 2,
    marginHorizontal: 5 * ratioWidth,
    paddingVertical: 2 * ratioWidth,
    paddingHorizontal: 8 * ratioWidth,
  },
  gContainer: {
    flexDirection: 'row',
    borderBottomColor: colors.gmail,
    borderBottomWidth: 2,
    marginHorizontal: 5 * ratioWidth,
    paddingVertical: 2 * ratioWidth,
    paddingHorizontal: 8 * ratioWidth,
  },
  facebook: {
    color: colors.fbColor,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font16,
    marginLeft: 5 * ratioWidth,
  },
  gmail: {
    color: colors.gmail,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font16,
    marginLeft: 5 * ratioWidth,
  },
});

export default styles;
