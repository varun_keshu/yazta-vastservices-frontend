import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { colors, ratioHeight } from '../themes';
import Header from './header';

const Container = (props) => {
  const {
    showHeader,
    label,
    children,
    extraStyles,
    scrollEnabled,
    showBack,
    showMenu,
    containerStyle,
    onNotification,
    role,
  } = props;
  return (
    <View>
      {showHeader && (
        <Header
          label={label}
          showBack={showBack}
          showMenu={showMenu}
          role={role}
          onNotification={onNotification}
        />
      )}
      {scrollEnabled ? (
        <ScrollView
          nestedScrollEnabled={true}
          showsVerticalScrollIndicator={false}
          style={[styles.containerView, extraStyles]}
          contentContainerStyle={[styles.contentContainer, containerStyle]}
        >
          <>{children}</>
        </ScrollView>
      ) : (
        <View style={[styles.containerView, extraStyles]}>{children}</View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  containerView: {
    backgroundColor: colors.appSecondary,
    height: '100%',
  },
  contentContainer: {
    paddingBottom: 90 * ratioHeight,
  },
});

export default Container;
