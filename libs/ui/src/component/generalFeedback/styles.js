import { StyleSheet } from 'react-native';
import { colors, ratioHeight, ratioWidth, fonts } from '../../themes';

const styles = StyleSheet.create({
  container: {
    padding: 30 * ratioWidth,
  },
  title: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font15,
    textAlign: 'center',
    lineHeight: 20 * ratioHeight,
    marginVertical: 10 * ratioHeight,
  },
  inputStyle: {
    borderWidth: 1,
    paddingHorizontal: 12 * ratioHeight,
    paddingVertical: 12 * ratioHeight,
    borderRadius: 8,
    width: '100%',
    backgroundColor: colors.white,
    borderColor: colors.pagination,
  },
  extraStyle: {
    borderBottomWidth: 0,
  },
  buttonStyle: {
    marginTop: 20 * ratioHeight,
    borderRadius: 50,
    paddingVertical: 10 * ratioHeight,
    marginHorizontal: 20 * ratioWidth,
  },
});

export default styles;
