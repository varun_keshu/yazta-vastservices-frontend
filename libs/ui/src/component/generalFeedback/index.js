import React from 'react';
import { Text } from 'react-native';
import ButtonPrimary from '../../helper/buttons/buttonPrimary';
import styles from './styles';
import Container from '../container';
import InputField from '../../helper/inputs/inputField';

const Feedback = () => {
  return (
    <Container
      scrollEnabled
      extraStyles={styles.container}
      showHeader
      showBack
      showMenu
      label="General Feedback"
    >
      <Text style={styles.title}>
        Please share anything that's on your mind
      </Text>
      <InputField
        inputExtraStyle={styles.inputStyle}
        extraStyle={styles.extraStyle}
        numLines={6}
        multiline
        placeholder="Your input is invaluable to us. Please don't hold back!"
      />
      <ButtonPrimary label="Submit" extraStyle={styles.buttonStyle} />
    </Container>
  );
};

export default Feedback;
