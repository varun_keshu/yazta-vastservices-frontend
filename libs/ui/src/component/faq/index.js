import React from 'react';
import Listing from '../../helper/listing';

const list = [
  { id: 1, label: 'Saves Time', route: 'Faq' },
  { id: 2, label: 'Earn Trust', route: 'Faq' },
  { id: 3, label: 'Lorem ipsum', route: 'Faq' },
  { id: 4, label: 'Lorem ipsum', route: 'Faq' },
];

const Support = () => {
  return (
    <Listing
      label="FAQ"
      listData={list}
      iconType="Ionicons"
      iconName="play"
    />
  );
};

export default Support;
