import React from 'react';
import { TouchableOpacity, Text, View, Image, FlatList } from 'react-native';
import { theme } from '../../../Constants/Theme';
import Icon from '../../helper/icon/icons';
import styles from './styles';
import { colors } from '../../themes'


export const BookingCard = (props) => {
    const [showList, setShow] = React.useState(false);



    const { color,name,nameLabel,type, serviceLabel,dateLabel,priceLabel,jobidLabel,rateBasisLabel,rating,date,serviceName,payRateType,price,jobId, containerStyle, onPress } = props;
    return (
      

        <TouchableOpacity onPress={()=>{onPress()}}  style={[styles.card2,containerStyle]}>
          <View style={styles.row}>
            <View style={styles.row2}>
              <Text style={styles.label} >{nameLabel+': '}</Text>
              <Text style={styles.valueText} >{name}</Text>
            </View>
           {rating!==undefined&&
            <View style={styles.row2}>
               {type===1? <>
          <Icon type={'FontAwesome'} name={'star'} color={color?color:colors.yellow} extraStyles={styles.staricon} size={20} />
              <Text style={styles.valueText} >{rating}</Text>
              </>:
              <>
                  <Text style={styles.valueText} >{rating+" "}</Text>
              <Icon type={'FontAwesome'} name={'star'} color={color?color:colors.yellow} extraStyles={styles.staricon} size={20} />
                  </>}
            </View>}
          </View>
         {type===1?
          <>
          <View style={styles.row}>
            <View style={styles.row2}>
              <Text style={styles.label} >{dateLabel+': '}</Text>
              <Text style={styles.valueText} >{date}</Text>
            </View>
            </View>
          <View style={styles.row}>
            <View style={styles.row2}>
              <Text style={styles.label} >{serviceLabel+': '}</Text>
              <Text style={styles.valueText} >{serviceName}</Text>
            </View>
            <View style={styles.row2}>
              <Text style={styles.label} >{rateBasisLabel+': '}</Text>
              <Text style={styles.valueText} >{payRateType}</Text>
            </View>
          </View>
          </>:
         <>
          <View style={styles.row}>
            <View style={styles.row3}>
              <Text style={styles.label2} >{dateLabel+': '}</Text>
              <Text style={styles.valueText2} >{date}</Text>
            </View>
            <View style={styles.row4}>
              <Text style={styles.label2} >{serviceLabel+': '}</Text>
              <Text style={styles.valueText2} >{serviceName}</Text>
            </View>
          </View>
          </>}
          <View style={[styles.row,styles.lastCard]}>
            <View style={styles.row2}>
              <Text style={styles.label} >{priceLabel+': '}</Text>
              <Text style={styles.valueText} >{price}</Text>
            </View>
            <View style={styles.row2}>
              <Text style={styles.label} >{jobidLabel+': '}</Text>
              <Text style={styles.valueText} >{jobId}</Text>
            </View>
          </View>
        </TouchableOpacity>
      )





}
export default BookingCard;


