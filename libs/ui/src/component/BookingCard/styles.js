import {StyleSheet,Dimensions} from 'react-native';

import {
	colors,
	ratioHeight,
	ratioWidth,
	fonts,
  } from '../../themes';

const width = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(width -250);
const styles = StyleSheet.create({
	card2: {
		backgroundColor: colors.white,
		borderRadius: 20,
		marginBottom: 20 * ratioHeight,
		marginHorizontal: 1 * ratioHeight,
		shadowColor: "#000",
		shadowOffset: {
		  width: 0,
		  height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
	
		elevation: 5,
	  },
	  row: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		borderBottomWidth: 0.50,
		borderBottomColor: colors.grey,
		paddingVertical: 10 * ratioHeight,
		marginHorizontal: 10 * ratioHeight,
	  },
	  row2: {
		flexDirection: 'row',
		justifyContent: 'flex-start',
	  },
	  row3: {
		flexDirection: 'row',
		// justifyContent: 'flex-start',
		width:90*ratioWidth,
		// backgroundColor:'red'
	  },
	  row4: {
		// flexDirection: 'row',
		// justifyContent: 'flex-start',
		width:90*ratioWidth,
		// backgroundColor:'red'
	  },
	  label: {
		fontSize: fonts.size.font11,
		fontFamily: fonts.type.montserratMedium,
		color: colors.grey,
	  },
	  valueText: {
		fontSize: fonts.size.font11,
		fontFamily: fonts.type.montserratMedium,
		color: colors.black,
	  },
	  label2: {
		fontSize: fonts.size.font11,
		fontFamily: fonts.type.montserratMedium,
		color: colors.grey,
	  },
	  valueText2: {
		fontSize: fonts.size.font11,
		fontFamily: fonts.type.montserratMedium,
		color: colors.black,
	  },
	  staricon: {
		marginRight: 5 * ratioWidth,
	  },
	  
	  lastCard: {
		borderBottomWidth: 0,
	
	  },

});
export default styles;

