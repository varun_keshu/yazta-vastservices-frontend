import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import Icon from '../../helper/icon/icons';
import colors from '../../themes/colors';

const Menu = (props) => {
  const { onPress ,onNotification} = props;
  return (
    <View style={styles.rightMenu}>
      <Icon type="Fontisto" name="bell" size={18} color={colors.black}   onPress={onNotification}/>
      <View style={styles.bellCountBg}>
        <Text style={styles.bellCount}>0</Text>
      </View>
      <Icon
        type="Entypo"
        name="menu"
        size={25}
        color={colors.black}
        extraStyles={styles.menuIcon}
        onPress={onPress}
      />
    </View>
  );
};

export default Menu;
