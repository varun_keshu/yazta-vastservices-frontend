import React from 'react';
import { View, Text } from 'react-native';
import { Logo } from '../../themes/svgs';
import styles from './styles';

const LogoIcon = (props) => {
  const { label } = props;
  return (
    <View style={styles.header}>
      <Logo height={30} width={30} />
      <Text style={styles.label}>{label}</Text>
    </View>
  );
};

export default LogoIcon;
