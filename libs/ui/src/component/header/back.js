import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import Icon from '../../helper/icon/icons';
import colors from '../../themes/colors';

const Back = (props) => {
  const { onPress } = props;
  return (
    <View style={styles.row}>
      <Icon
        type="Ionicons"
        name="chevron-back"
        size={28}
        color={colors.black}
        onPress={onPress}
      />
      <Text style={styles.display}>......</Text>
    </View>
  );
};

export default Back;
