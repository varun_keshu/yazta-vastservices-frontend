import { StyleSheet } from 'react-native';
import { colors, ratioHeight } from '../../themes';
import { fonts, ratioWidth } from '../themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FCDAA9',
    height: 68,
  },
  linearGradient: {
    flex: 1,
    paddingHorizontal: 12 * ratioWidth,
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10 * ratioHeight,
  },
  header: {
    alignItems: 'center',
  },
  label: {
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font15,
    color: colors.appDark,
  },
  rightMenu: {
    flexDirection: 'row',
  },
  menuIcon: {
    marginLeft: 6 * ratioWidth,
  },
  bellCountBg: {
    backgroundColor: colors.red,
    height: 16,
    width: 14,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: -5 * ratioWidth,
    marginTop: -1 * ratioHeight,
  },
  bellCount: {
    color: colors.white,
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font11,
  },
  display: { color: colors.appSecondary },
  row: { flexDirection: 'row' },
});

export default styles;
