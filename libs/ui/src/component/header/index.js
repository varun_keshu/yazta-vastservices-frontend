import React from 'react';
import { View, Text } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import LinearGradient from 'react-native-linear-gradient';
import constants from '../../themes/constants';
import styles from './styles';
import Back from './back';
import Menu from './menu';
import Logo from './logo';

import { openDrawer } from 'apps/yazta-provider/src/navigation/navigationHelper';
import { openDrawer as openDraw } from 'apps/yazta-client/src/navigation/navigationHelper';

const Header = (props) => {
  const { label, extraStyle, showBack,onNotification, showMenu, role } = props;
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        locations={[0, 0.6, 0.9]}
        colors={constants.gradiantColor}
        style={[styles.linearGradient, extraStyle]}
      >
        {showBack ? (
          <Back onPress={() => navigation.goBack()} />
        ) : (
          <Text style={styles.display}>.......</Text>
        )}
        <Logo label={label} />
        {showMenu ? (
          <Menu
            onPress={() => (role === 'PROVIDER' ? openDrawer() : openDraw())}
            onNotification={onNotification}
          />
        ) : (
          <View></View>
        )}
      </LinearGradient>
    </View>
  );
};

export default Header;
