import React from 'react';
import { TouchableOpacity, View, TextInput } from 'react-native';
import {
	colors,

} from '../../themes';
import Icon from '../../helper/icon/icons';
import styles from './styles';

const MessagerInput = (props) => {
    const {
      title,
      placeholder,
      value,
      buttonTitle,
      onChange,
      onPress,
      extraStyle,
    } = props;
    return (
      <View style={styles.inputBox}>
         <TextInput 
        style={styles.inputField}
        multiline={true}
        value={value}
        placeholder={placeholder}
        onChange={(value) => { onChange(value);  }}    
        />
        <TouchableOpacity  onPress={() => { onPress(); }} style={styles.sendBtn}  >
       <Icon type={'Ionicons'} name={'ios-send'} color={colors.pagination} extraStyles={styles.iconBox} size={25} />
        </TouchableOpacity>
      </View>
    );
  };
  
  export default MessagerInput;


