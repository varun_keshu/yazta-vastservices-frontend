import { StyleSheet, Dimensions } from 'react-native';

import {
	colors,
	ratioHeight,
	ratioWidth,
	fonts,
} from '../../themes';

const width = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(width - 250);
const styles = StyleSheet.create({
	inputBox:{
		flexDirection:'row',
		marginHorizontal:20*ratioHeight,
		margin:10*ratioHeight,
		// marginBottom:30*ratioHeight,
		borderWidth:1,
		borderColor:colors.pagination,
		justifyContent:'space-between',
		alignItems:'center',
		paddingHorizontal:10*ratioHeight
	
	  },
	  inputField:{
		maxWidth:width-100
	  }, 
	  sendBtn:{
	
	  },
	  iconBox:{
	   
	  }, 
});
export default styles;

