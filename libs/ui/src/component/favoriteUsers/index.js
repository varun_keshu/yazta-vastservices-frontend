import React from 'react';
import { View, Text, FlatList, Image } from 'react-native';
import Icon from '../../helper/icon/icons';
import { colors, images } from '../../themes';
import styles from './styles';

const Item = [
  {
    id: '1',
    name: 'John',
    skill: 'Cleaning',
  },
  {
    id: '2',
    name: 'Adele',
    skill: 'Cooking',
  },
  {
    id: '3',
    name: 'Job',
    skill: 'BabySitting',
  },
  {
    id: '4',
    name: 'Alie',
    skill: 'Cooking',
  },
  {
    id: '5',
    name: 'Jack',
    skill: 'Cooking, Cleaning',
  },
  {
    id: '6',
    name: 'Mony',
    skill: 'Cooking',
  },
];

const FavoriteClient = () => {
  const renderItem = (item) => {
    let items = item.item;
    return (
      <View style={styles.cardContainer}>
        <View style={styles.subContainer}>
          <Image style={styles.profileStyle} source={images.person} />
          <View style={styles.midContainer}>
            <Text style={styles.name}>{items.name}</Text>
            <Text style={styles.skill}>{items.skill}</Text>
          </View>
        </View>
        <Icon
          type="MaterialCommunityIcons"
          name="heart"
          size={26}
          color={colors.textColor}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={Item}
        keyExtractor={(item) => item.id}
        numColumns={1}
        renderItem={renderItem}
      />
    </View>
  );
};

export default FavoriteClient;
