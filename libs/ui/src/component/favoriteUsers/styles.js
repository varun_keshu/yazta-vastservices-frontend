import { StyleSheet } from 'react-native';
import { colors, ratioHeight, fonts, ratioWidth } from '../../themes';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 12 * ratioWidth,
    paddingVertical: 22 * ratioWidth,
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    elevation: 3,
    borderRadius: 4,
    marginHorizontal: 12 * ratioWidth,
    marginVertical: 12 * ratioWidth,
    paddingHorizontal: 15 * ratioWidth,
    paddingVertical: 8 * ratioWidth,
  },
  subContainer: {
    flexDirection: 'row',
  },
  midContainer: {
    marginLeft: 25 * ratioWidth,
    justifyContent: 'center',
  },
  profileStyle: {
    height: 45 * ratioHeight,
    width: 45 * ratioHeight,
    borderRadius: 50,
  },
  name: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font14,
  },
  skill: {
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font13,
    marginTop: 5 * ratioHeight,
  },
});

export default styles;
