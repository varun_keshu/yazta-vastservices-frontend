import React, { useState, useEffect } from 'react';
import { TouchableOpacity, Text, View, Modal } from 'react-native';
import Icon from '../../helper/icon/icons';
import { colors } from '../../themes';
import styles from './styles';

export const GeneralPopUp = (props) => {
  const [modalVisible, setModalVisible] = useState(false);

  const {
    isVisible,
    onRequestClose,
    onRef,
    OnRightPress,
    OnLeftPress,
    title,
    description,
    leftButtonTxt,
    rightButtonTxt,
    yesStyle,
    desc,
    showIcon,
  } = props;

  useEffect(() => {
    setModalVisible(isVisible);
  }, [isVisible]);

  return (
    <Modal
      animationOutTiming={700}
      animationInTiming={700}
      animationIn="slideInUp"
      animationOut="slideOutDown"
      transparent={true}
      visible={modalVisible}
      style={{ justifyContent: 'center' }}
      onRequestClose={() => {
        onRequestClose();
      }}
    >
      <View style={styles.modal}>
        <View style={styles.confirmModal}>
          <View style={styles.confirmModalContainer}>
            {showIcon && (
              <Icon
                type="MaterialCommunityIcons"
                name="check-circle"
                size={35}
                color={colors.pagination}
                extraStyles={styles.iconStyle}
              />
            )}
            <Text style={[styles.confirmDescriptionTxt, desc]}>
              {description}
            </Text>
          </View>
          <View style={styles.confirmRow}>
            <TouchableOpacity
              style={[styles.yesBtn, yesStyle]}
              onPress={() => {
                OnLeftPress();
              }}
            >
              <Text style={styles.acceptTxt}>{leftButtonTxt}</Text>
            </TouchableOpacity>
            {rightButtonTxt && (
              <TouchableOpacity
                style={styles.noBtn}
                onPress={() => {
                  OnRightPress();
                }}
              >
                <Text style={styles.noTxt}>{rightButtonTxt}</Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default GeneralPopUp;
