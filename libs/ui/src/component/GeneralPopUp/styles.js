import { StyleSheet, Dimensions } from 'react-native';

import { colors, ratioHeight, ratioWidth, fonts } from '../../themes';

const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
  confirmModal: {
    backgroundColor: colors.white,
    borderRadius: 16,
    margin: 20,
    elevation: 5,
  },
  confirmModalContainer: {
    padding: 15 * ratioHeight,
  },
  iconStyle: {
    paddingBottom: 10 * ratioHeight,
  },
  confirmRow: {
    flexDirection: 'row',
    borderTopColor: colors.pagination,
    borderTopWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  confirmDescriptionTxt: {
    fontSize: fonts.size.font14,
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratSemiBold,
    lineHeight: 20 * ratioHeight,
    textAlign: 'center',
    paddingHorizontal: 5 * ratioWidth,
  },
  yesBtn: {
    width: '50%',
    alignItems: 'center',
    height: 50 * ratioHeight,
    justifyContent: 'center',
  },
  noBtn: {
    width: '50%',
    alignItems: 'center',
    height: 50,
    justifyContent: 'center',
    borderLeftColor: colors.pagination,
    borderLeftWidth: 0.5,
  },
  acceptTxt: {
    fontSize: fonts.size.font13,
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratSemiBold,
    textAlign: 'center',
  },
  noTxt: {
    fontSize: fonts.size.font13,
    color: colors.appDark,
    fontFamily: fonts.type.montserratSemiBold,
  },
  modal: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default styles;
