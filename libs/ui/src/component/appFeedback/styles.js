import { StyleSheet } from 'react-native';
import {
  colors,
  ratioHeight,
  ratioWidth,
  fonts,
} from '../../themes';

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 30 * ratioWidth,
  },
  text: {
    color: colors.black,
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font12,
    textAlign: 'center',
    lineHeight: 20 * ratioHeight,
  },

  upperContainer: {
    marginTop: 20 * ratioHeight,
  },
  middleContainer: {
    marginTop: 20 * ratioHeight,
  },
  comment: {
    textAlign: 'center',
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font16,
  },
  commentContainer: {
    borderWidth: 1,
    paddingVertical: 40 * ratioHeight,
    borderRadius: 8,
    marginTop: 20 * ratioHeight,
    backgroundColor: colors.detailBackground,
    borderColor: colors.pagination,
  },
  buttonStyle: {
    borderRadius: 80,
    paddingVertical: 20 * ratioHeight,
    marginHorizontal: 20 * ratioWidth,
  },
  label: {
    color: colors.white,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font14,
  },
  bottomContainer: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: colors.pagination,
    marginTop: 20 * ratioHeight,
  },
  inviteText: {
    color: colors.appDark,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font16,
    paddingVertical: 18 * ratioHeight,

    textAlign: 'center',
  },
  bottom: {
    borderRadius: 4,
    borderStyle: 'dashed',
    borderWidth: 1,
    marginTop: 30 * ratioHeight,
    borderColor: colors.pagination,
    paddingVertical: 20 * ratioHeight,
    backgroundColor: colors.detailBackground,
    textAlign: 'center',
    color: colors.appDark,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font18,
    marginHorizontal: 50 * ratioWidth,
  },
});

export default styles;
