import { StyleSheet } from 'react-native';
import { colors, ratioHeight, fonts, ratioWidth } from '../../themes';

const styles = StyleSheet.create({
  container: {
    padding: 20 * ratioHeight,
  },
  contentContainerStyle: {
    alignItems: 'center',
    marginTop: 40 * ratioHeight,
  },
  cardContainer: {
    backgroundColor: colors.white,
    elevation: 10,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: colors.pagination,
    borderWidth: 1,
    height: 45 * ratioHeight,
    width: 45 * ratioHeight,
    marginVertical: 10 * ratioHeight,
  },
  titleTxt: {
    color: colors.textColor,
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font14,
    marginVertical: 30 * ratioHeight,
    paddingVertical: 12 * ratioHeight,
    borderTopColor: colors.pagination,
    borderTopWidth: 0.5,
    borderBottomColor: colors.pagination,
    borderBottomWidth: 0.5,
  },
});

export default styles;
