import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import Container from '../container';
import styles from './styles';
import Icon from '../../helper/icon/icons';
import { useNavigation } from '@react-navigation/core';

const iconList = [
  {
    id: 1,
    name: 'facebook',
    type: 'FontAwesome',
    color: '#604FFF',
  },
  {
    id: 2,
    name: 'whatsapp',
    type: 'FontAwesome',
    color: '#1BD741',
  },
  {
    id: 3,
    name: 'mail-read',
    type: 'Octicons',
    color: '#F82626',
  },
];

const Contact = (props) => {
  const navigation = useNavigation();

  const onNavigate = (route) => {
    navigation.navigate(route);
  };

  const ItemView = ({ item }) => {
    return (
      <TouchableOpacity style={[styles.cardContainer, {
          borderColor: `${item.color}`
      }]}>
        <Icon
          name={item.name}
          type={item.type}
          size={36}
          color={item.color}
        />
      </TouchableOpacity>
    );
  };

  return (
    <Container
      scrollEnabled
      extraStyles={styles.container}
      showHeader
      showBack
      showMenu
      label="Contact Us"
    >
      <View style={styles.contentContainerStyle}>
        <FlatList
          data={iconList || []}
          nestedScrollEnabled
          renderItem={ItemView}
          keyExtractor={(item) => item.id}
        />
        <Text style={styles.titleTxt}>
          Please choose from the options above
        </Text>
      </View>
    </Container>
  );
};

export default Contact;
