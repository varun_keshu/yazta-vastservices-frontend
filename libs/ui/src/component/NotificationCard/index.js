import React from 'react';
import { TouchableOpacity, Text, View, Image, FlatList } from 'react-native';
import { theme } from '../../../Constants/Theme';
import Icon from '../../helper/icon/icons';

import styles from './styles';

import { colors, images } from '../../themes'


export const NotificationCard = (props) => {
  const [showList, setShow] = React.useState(false);



  const { date,time,lastSeen, status,imageLeft, imgStyle, description, containerStyle, onPress, onInfoPress, showInfo } = props;
  return (
    <TouchableOpacity onPress={() => { onPress() }}
      style={[styles.mainContainer, containerStyle]}>
      <View style={styles.listRow}>
        <View style={styles.leftRow}>
          <View style={styles.imageContainer}>
            <Image source={imageLeft} style={[styles.userImg, imgStyle]} />
          </View>

        </View>
        <View style={styles.assignListContainer} >
                        <View style={styles.issueListContainer} >
                            <Text style={styles.description}>{description}</Text>
                            <Text style={styles.dateLable} >{date}</Text>
                        </View>
                        <View style={styles.issueListContainer}>
                        <View style={styles.row}>
                            <Text style={styles.dateLable}>{lastSeen}</Text>{status!==undefined&&
                            <Text style={[styles.statusLabel,status===0?styles.statusLabel1:status==1?styles.statusLabel2:status===2?styles.statusLabel3:{}]} >{status===0?'Request':status==1?'Cancelled':status===2?'Reviewed':''}</Text>
                            }</View>
                            <Text style={styles.dateLable}>{time}</Text>
                        </View>
                    </View>
        
      </View>


    </TouchableOpacity>
  );





}
export default NotificationCard;


