import {StyleSheet,Dimensions} from 'react-native';

import {
	colors,
	ratioHeight,
	ratioWidth,
	fonts,
  } from '../../themes';

const width = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(width -250);
const styles = StyleSheet.create({
	mainContainer: {
		flex: 1,
		marginVertical: 10,
		marginHorizontal: 20,
        backgroundColor:colors.white,
        borderRadius:15,
		padding:15,
        borderWidth:0.75,
        borderColor:colors.pagination,
		overflow:'hidden'

	},
	listRow:{		
		flexDirection: 'row',
		alignItems:'center',
	},
	leftRow	:{		
		flexDirection: 'row',
	},
	imageContainer:{
		alignItems: 'flex-start',
		justifyContent: 'center',

		// zIndex: 10,
	},
	userImg:{
		width: 65,
		height: 65,
        borderRadius:100,
        overflow:'hidden',
        backgroundColor:colors.appDark,
	},
	assignListContainer:{
	
	},
	description:{
		fontWeight: '700',
		color: colors.black,

	},
	dateLable:{
		color: colors.grey
	},
	iconBox: {
		backgroundColor: colors.textColor,
		height: 25 * ratioHeight,
		width: 25 * ratioHeight,
		borderRadius: 50 * ratioWidth,	
	  },
	  row:{
		  flexDirection:'row',
	  },

	  issueListContainer:{
		flexDirection:'row',
		justifyContent:'space-between',
		width:260,
		marginHorizontal:10,
		paddingVertical:5,
	  },
	  statusLabel:{
		  fontSize:fonts.size.font8,
		  paddingHorizontal:20,
		  paddingVertical:8,
		  borderRadius:20,
		  marginLeft:5,
	  },
	  statusLabel1:{
		  color:colors.black,
		  borderWidth:1,
		  borderColor:colors.pagination,
	  },
	  statusLabel3:{
		  backgroundColor:colors.pagination,
		  color:colors.white,
		  borderWidth:0,
	  },
	  statusLabel2:{
		  backgroundColor:colors.textColor,
		  color:colors.white,
		  borderWidth:0,
		},

});
export default styles;

