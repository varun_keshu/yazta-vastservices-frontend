import React from 'react';
import { TouchableOpacity, Text, View, FlatList } from 'react-native';
import styles from './DropdownStyle';
import Icon from 'react-native-vector-icons/Feather';

import { colors } from '../../themes';

export const Dropdown = (props) => {
  const [showList, setShow] = React.useState(false);
  const {
    mainContainer,
    data,
    selectedValue,
    isShowList,
    listItemStyle,
    onChangeText,
    inputContainerStyle,
    iconStyle,
    listStyle,
    setShowList,
    placeholder,
    placeholderStyle,
  } = props;

  return (
    <View style={[styles.mainContainer, mainContainer]}>
      <TouchableOpacity
        style={[styles.drodownContainer, inputContainerStyle]}
        onPress={() => {
          setShow(!showList), setShowList();
        }}
        {...props}
      >
        {selectedValue !== null ? (
          <Text style={styles.selectedTxt}>{selectedValue.label}</Text>
        ) : (
          <Text style={[styles.landPlaceHolder, placeholderStyle]}>
            {placeholder}
          </Text>
        )}
        <Icon name="chevron-down" style={[styles.downArrowIcon, iconStyle]} />
      </TouchableOpacity>
      {(showList === true || isShowList === true) && (
        <View style={[styles.dropdownList, listStyle]}>
          <FlatList
            data={data}
            style={[styles.w100]}
            extraData={data}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => {
              return (
                <TouchableOpacity
                  style={[styles.dropdownItems, listItemStyle]}
                  onPress={() => {
                    setShow(!showList);
                    setShowList();
                    onChangeText(item);
                  }}
                >
                  {selectedValue != null ? (
                    <>
                      <Text
                        style={[
                          item.value === selectedValue.value
                            ? { color: colors.black }
                            : { color: colors.grey },
                          styles.labelTxt,
                        ]}
                      >
                        {item.label}
                      </Text>
                      {/* {item.value === selectedValue.value && (
												<Icon name="check" style={styles.checkIcon} />
											)} */}
                    </>
                  ) : (
                    <Text style={[{ color: colors.black }, styles.labelTxt]}>
                      {item.label}
                    </Text>
                  )}
                </TouchableOpacity>
              );
            }}
          />
        </View>
      )}
    </View>
  );
};
export default Dropdown;

{
  /* <Molecules.Dropdown
							setShowList={() => { setScrollEnabled(!scrollEnabled) }}
							selectedValue={country}
							data={countryList}
							placeholder={ 'country_placeholder')}
							value={country}
							inputContainerStyle={styles.dropInputContainer}
							placeholderStyle={styles.placeholderStyle}
							iconStyle={styles.dropdownIcon}
							listStyle={styles.listStyle}
							onChangeText={item => { setCountry(item) }} /> */
}
