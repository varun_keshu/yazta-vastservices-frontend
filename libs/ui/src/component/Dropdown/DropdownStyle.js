import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../themes';

export default styles = StyleSheet.create({
  drodownContainer: {
    width: '100%',
  },
  touchableView: {
    width: '100%',
    justifyContent: 'center',
    borderBottomColor: 'lightgray',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 10,
  },
  selectedTxt: {
    fontSize: fonts.size.font14,
    fontFamily: fonts.type.montserratBold,
    color: colors.textPrimary,
  },
  landPlaceHolder: {
    fontSize: fonts.size.font14,
    color: colors.grey,
    fontFamily: fonts.type.montserratMedium,
  },
  downArrowIcon: {
    fontSize: 23,
    color: colors.black,
    position: 'absolute',
    right: 18,
  },
  dropdownList: {
    width: '100%',
    backgroundColor: colors.white,
    maxHeight: 120,
    borderColor: 'lightgray',
    borderWidth: 1,
    // elevation: 1,
    zIndex: 1,
    minHeight: 80,
    // position: 'absolute',
    // top: 50,
  },
  dropdownItems: {
    width: '100%',
    height: 39,
    backgroundColor: colors.white,
    alignItems: 'flex-start',
    justifyContent: 'center',
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    zIndex: 1,
    marginBottom: 1,
  },
  w100: {
    width: '100%',
  },
  checkIcon: {
    fontSize: 20,
    color: colors.black,
    position: 'absolute',
    right: 8,
  },
  mainContainer: {
    position: 'relative',
  },
  labelTxt: {
    marginLeft: 10,
    textAlign: 'center',
    fontFamily: fonts.type.montserratMedium,
    color: colors.textPrimary,
  },
});
