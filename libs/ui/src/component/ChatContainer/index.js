import React from 'react';
import { TouchableOpacity, Text, View, Image, FlatList } from 'react-native';
import { theme } from '../../../Constants/Theme';
import Icon from '../../helper/icon/icons';

import styles from './styles';

import { colors, images } from '../../themes'


export const ChatContainer = (props) => {
    const [showList, setShow] = React.useState(false);



    const { time, image, userType, description, containerStyle, onPress, onInfoPress, showInfo } = props;
    return (
        <View  style={styles.chatBox}>
        {userType===1?
            <View style={styles.chatContainer}  >
              <View style={styles.imageContainer}>
                <Image source={image} style={[styles.userImg,]} />
              </View>
              <View style={styles.infoBox}  >
                <View style={styles.descriptionBox}  >
                  <Text style={styles.descriptionTxt} numberOfLines={20} >{description}</Text>
                </View>
                <View style={styles.talkBubbleTriangleLeft} />
                <Text style={styles.timeTxt} >{time}</Text>
              </View>
            </View>
      :
            <View style={styles.senderChatContainer}  >
              <View style={styles.senderImageContainer}>
                <Image source={image} style={[styles.userImg,]} />
              </View>
              <View style={styles.senderInfoBox}  >
                <View style={styles.senderDescriptionBox}  >
                  <Text style={styles.descriptionTxt}      numberOfLines={20}>{description}</Text>
                </View>
                <View style={styles.talkBubbleTriangleRight} />
                <Text style={styles.timeTxt} >{time}</Text>
              </View>
            </View>
        }
            </View>
    );





}
export default ChatContainer;


