import {StyleSheet,Dimensions} from 'react-native';

import {
	colors,
	ratioHeight,
	ratioWidth,
	fonts,
  } from '../../themes';

const width = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(width -10);
const styles = StyleSheet.create({
	chatBox:{
		margin:10,
		marginBottom:0,
	
	  },
		chatContainer:{
		  flexDirection:'row',
		 
		},
		infoBox:{
		  marginLeft:20
	
		},
		descriptionBox:{
		  backgroundColor:colors.textColor,
		  padding:10*ratioHeight,
		  borderTopRightRadius:15,
		  borderBottomEndRadius:15,
		  borderBottomLeftRadius:15,
		  zIndex:1,
		  maxWidth:ITEM_WIDTH
		  
		},
		descriptionTxt:{
		  fontSize:fonts.size.font14,
		  color:colors.white,
		  fontFamily:fonts.type.montserratMedium
		},
		timeTxt:{
		  fontSize:fonts.size.font12,
		  color:colors.grey,
		},
		imageContainer:{
	  
		},
		userImg:{
		  width: 65,
		  height: 65,
			  borderRadius:100,
			  overflow:'hidden',
			  backgroundColor:colors.appDark,
		},
	  
	  
		talkBubbleTriangleLeft: {
		  position: "absolute",
		  left: -20,
		  top: 0,
		  width: 0,
		  height: 0,
		  borderTopColor:colors.textColor,
		  borderTopWidth: 0,
		  borderRightWidth: 40,
		  borderRightColor:colors.textColor,
		  borderBottomWidth: 25,
		  borderBottomColor: "transparent",
		},
		/////
		talkBubbleTriangleRight: {
		  position: "absolute",
		  right: -20,
		  top: 0,
		  width: 0,
		  height: 0,
		  borderTopColor:'transparent',
		  borderTopWidth: 25,
		  borderRightWidth: 40,
		  borderRightColor:colors.pagination,
		  borderBottomWidth: 0,
		  borderBottomColor: "transparent",
		transform:[{rotate:'180deg'}]
	
		},
		senderChatContainer:{
		  flexDirection:'row-reverse',
	   
	
		},
		senderInfoBox:{
		  marginRight:20
	
		},
		senderImageContainer:{
	  
		},
		senderDescriptionBox:{
		  backgroundColor:colors.pagination,
		  padding:10*ratioHeight,
		  borderTopRightRadius:0,
		  borderBottomEndRadius:15,
		  borderBottomLeftRadius:15,
		  borderTopLeftRadius:15,
		  zIndex:1,
		  maxWidth:ITEM_WIDTH
		  
		},
		descriptionTxt:{
		  fontSize:fonts.size.font14,
		  color:colors.white,
		  fontFamily:fonts.type.montserratMedium
		},
		timeTxt:{
		  fontSize:fonts.size.font12,
		  paddingTop:2*ratioHeight,
		  color:colors.grey,
		},
});
export default styles;

