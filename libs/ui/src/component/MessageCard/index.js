import React from 'react';
import { TouchableOpacity, Text, View, Image, FlatList } from 'react-native';
import { theme } from '../../../Constants/Theme';
import Icon from '../../helper/icon/icons';

import styles from './styles';

import { colors, images } from '../../themes'


export const MessageCard = (props) => {
    const [showList, setShow] = React.useState(false);



    const { name, imageLeft, imgStyle, description, containerStyle, onPress, onInfoPress, showInfo } = props;
    return (
        <TouchableOpacity onPress={() => { onPress() }}
            style={[styles.mainContainer, containerStyle]}>
            <View style={styles.listRow}>
                <View style={styles.leftRow}>
                    <View style={styles.imageContainer}>
                        <Image source={imageLeft} style={[styles.userImg, imgStyle]} />
                    </View>
                    <View style={styles.assignListContainer} >
                        <View style={styles.issueListContainer} onPress={() => { }}>
                            <Text style={styles.description}>{description}</Text>
                            <Text style={styles.dateLable}>{name}</Text>
                        </View>
                    </View>
                </View>
                {showInfo === true && (
                    <TouchableOpacity style={styles.infoButton} onPress={() => { onInfoPress() }} >
                        <Icon type={'AntDesign'} name={'info'} extraStyles={styles.iconBox} size={25} />
                    </TouchableOpacity>)}
            </View>


        </TouchableOpacity>
    );





}
export default MessageCard;


