import {StyleSheet,Dimensions} from 'react-native';

import {
	colors,
	ratioHeight,
	ratioWidth,
	fonts,
  } from '../../themes';

const width = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(width -250);
const styles = StyleSheet.create({
	mainContainer: {
		flex: 1,
		marginVertical: 10,
		marginHorizontal: 20,
        backgroundColor:colors.white,
        borderRadius:15,
        elevation:5,
		overflow:'hidden'

	},
	listRow:{		
		flexDirection: 'row',
		justifyContent:'space-between',
		alignItems:'center',
		paddingHorizontal:10*ratioHeight,
	},
	leftRow	:{		
		flexDirection: 'row',
	},
	imageContainer:{
		alignItems: 'flex-start',
		justifyContent: 'center',

		// zIndex: 10,
	},
	userImg:{
		width: 65,
		height: 65,
        borderRadius:100,
        overflow:'hidden',
        backgroundColor:colors.appDark,
	},
	assignListContainer:{
		minHeight: 100,
		maxWidth: ITEM_WIDTH,
		paddingLeft: 15*ratioHeight,
		justifyContent: 'center',  
	},
	description:{
		fontWeight: '700',
		color: colors.black,

	},
	dateLable:{
		color: colors.grey
	},
	iconBox: {
		backgroundColor: colors.textColor,
		height: 25 * ratioHeight,
		width: 25 * ratioHeight,
		borderRadius: 50 * ratioWidth,
		
	  },

	  infoButton:{
		  
	  },

});
export default styles;

