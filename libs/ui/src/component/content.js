import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { colors, ratioHeight, fonts } from '../../themes';
import Container from '../container';

const Content = (props) => {
  const { label, content } = props;
  return (
    <Container
      scrollEnabled
      extraStyles={styles.container}
      showHeader
      showBack
      showMenu
      label={label}
    >
      <Text style={styles.titleTxt}>{content}</Text>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20 * ratioHeight,
  },
  titleTxt: {
    color: colors.pagination,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font15,
    lineHeight: 22 * ratioHeight,
  },
});

export default Content;
