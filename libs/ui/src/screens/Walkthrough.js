import React from 'react';
import CarouselSlider from '../helper/carousel';

const Walkthrough = (props) => {
  const { skip, data } = props;
  return <CarouselSlider skip={skip} data={data} />;
};

export default Walkthrough;
