import React, { useState } from 'react';
import Container from '../../component/container';
import SignupFooter from '../../component/auth/register/signupFooter';
import ButtonPrimary from '../../helper/buttons/buttonPrimary';
import InputField from '../../helper/inputs/inputField';
import styles from './styles';
import { useDispatch } from 'react-redux';
import { registerUser } from '../../../../store/src/common/auth/action';
import {
  validateEmail,
  validateName,
  validatePassword,
  validatePhone,
} from '../../utils/validation';
import { useNavigation } from '@react-navigation/core';

const initialState = {
  name: '',
  email: '',
  phone: '',
  password: '',
};

const initialError = { field: '', message: '' };

const Signup = (props) => {
  const navigation = useNavigation();
  const { userRole, route } = props;
  const dispatch = useDispatch();

  const [registerDetail, setRegisterDetail] = useState(initialState);
  const [showPassword, setShowPassword] = useState(true);
  const [error, setError] = useState(initialError);

  const onRegister = () => {
    const errorEntity = { ...onValidate() };
    if (Object.keys(errorEntity).length === 0) {
      dispatch(
        registerUser({
          name: registerDetail.name,
          email: registerDetail.email,
          password: registerDetail.password,
          phoneNo: registerDetail.phone,
          userRole: props.userRole,
        })
      );
      setRegisterDetail(initialState);
    }
  };

  const onValidate = () => {
    const validateUsername = validateName(registerDetail.name);
    const validateEmailId = validateEmail(registerDetail.email);
    const validatePasswords = validatePassword(registerDetail.password);
    const validatePhoneNo = validatePhone(registerDetail.phone);

    let errorEntity = {};
    if (validateUsername.message) {
      errorEntity = validateUsername;
    } else if (validateEmailId.message) {
      errorEntity = validateEmailId;
    } else if (validatePasswords.message) {
      errorEntity = validatePasswords;
    } else if (validatePhoneNo.message) {
      errorEntity = validatePhoneNo;
    }
    setError(errorEntity);
    return errorEntity;
  };

  const onChange = (field, value) => {
    setRegisterDetail({ ...registerDetail, [field]: value });
  };

  const onNavigate = () => {
    navigation.navigate('Login');
    setRegisterDetail(initialState);
    setError(initialError);
  };

  return (
    <Container
      scrollEnabled
      showHeader
      label="Sign Up"
      extraStyles={styles.container}
    >
      <InputField
        placeholder="Enter Name"
        fieldName="Name"
        onChange={(value) => onChange('name', value)}
        value={registerDetail.name}
        error={error.field === 'name' && error.message}
        astric
      />
      <InputField
        placeholder="Enter Email"
        fieldName="Email"
        value={registerDetail.email}
        onChange={(value) => onChange('email', value)}
        error={error.field === 'email' && error.message}
        astric
      />
      <InputField
        placeholder="Enter Password"
        fieldName="Password"
        value={registerDetail.password}
        onChange={(value) => onChange('password', value)}
        error={error.field === 'password' && error.message}
        astric
        secureTextEntry={showPassword}
        showEyeIcon={showPassword}
        onEyeIconClick={() => setShowPassword(!showPassword)}
        showEyeWithLineIcon={!showPassword}
        onShowEyeWithLineIcon={() => setShowPassword(!showPassword)}
      />
      <InputField
        placeholder="Enter Phone"
        fieldName="phone"
        value={registerDetail.phone}
        onChange={(value) => onChange('phone', value)}
        error={error.field === 'phone' && error.message}
        astric
        keyboardType="phone-pad"
      />
      <ButtonPrimary label="Submit" onPress={onRegister} />
      <SignupFooter
        label="Already a member?"
        tag=" Log in here"
        onNavigate={() => onNavigate()}
        userRole={userRole}
      />
    </Container>
  );
};

export default Signup;
