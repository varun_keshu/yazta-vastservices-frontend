import { StyleSheet } from 'react-native';
import { colors, ratioHeight } from '../themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
    paddingHorizontal: 30 * ratioHeight,
  },
});

export default styles;
