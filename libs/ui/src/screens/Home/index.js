import React from 'react';
import { Text, View } from 'react-native';
import Container from '../../component/container';
import styles from './styles';
import { ratioHeight } from '../../themes';

const Home = (props) => {
  const { role } = props;

  return (
    <Container
      role={role}
      showHeader
      showMenu
      label="Home"
      extraStyles={styles.container}
    >
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 150 * ratioHeight,
        }}
      >
        <Text>Home Screen</Text>
      </View>
    </Container>
  );
};

export default Home;
