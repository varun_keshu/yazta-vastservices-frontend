/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import React, { useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useValidation } from 'react-native-form-validator';
import InputField from '../../helper/inputs/inputField';
import { AppLogo } from '../../themes/svgs';
import Container from '../../component/container';
import SignupFooter from '../../component/auth/register/signupFooter';
import ButtonPrimary from '../../helper/buttons/buttonPrimary';
import styles from './styles';
import { useNavigation } from '@react-navigation/core';
import { loginUser } from '../../../../store/src/common/auth/action';
import { useDispatch } from 'react-redux';
import validationMessages from '../../utils/validationMessages';

const Login = (props) => {
  const { userRole } = props;
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(true);

  const { validate, isFieldInError, getErrorsInField, getErrorMessages } =
    useValidation({
      state: { email, password },
      messages: validationMessages,
    });

  const onLogin = () => {
    validate({
      email: { required: true, email: true },
      password: { required: true },
    });
    if (getErrorMessages().length === 0 && email && password) {
      dispatch(loginUser({ email: email, password: password }));
      setEmail('');
      setPassword('');
    }
  };

  const onNavigate = () => {
    navigation.navigate('Register');
  };

  return (
    <Container extraStyles={styles.container}>
      <View style={styles.subContainer}>
        <AppLogo height={120} width={120} />
        <Text style={styles.header}>YAZTA</Text>
      </View>
      <InputField
        placeholder="Enter Email"
        fieldName="Email"
        onChange={(value) => setEmail(value)}
        value={email}
        astric
        error={isFieldInError('email') && getErrorsInField('email')[0]}
      />

      <InputField
        placeholder="Enter Password"
        fieldName="Password"
        value={password}
        secureTextEntry={showPassword}
        onChange={(value) => setPassword(value)}
        error={isFieldInError('password') && getErrorsInField('password')}
        astric
        showEyeIcon={showPassword}
        onEyeIconClick={() => setShowPassword(!showPassword)}
        showEyeWithLineIcon={!showPassword}
        onShowEyeWithLineIcon={() => setShowPassword(!showPassword)}
      />

      <TouchableOpacity
        style={styles.forgotContainer}
        onPress={() => navigation.navigate('ForgetPassword')}
      >
        <Text style={styles.forgot}>Forgot Password?</Text>
      </TouchableOpacity>
      <ButtonPrimary label="LOG IN" onPress={onLogin} />
      <SignupFooter
        label="Not a member yet?"
        tag=" Sign up here"
        onNavigate={() => onNavigate()}
        userRole={userRole}
      />
    </Container>
  );
};

export default Login;
