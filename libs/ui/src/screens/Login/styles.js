import { StyleSheet } from 'react-native';
import { ratioWidth, ratioHeight, fonts, colors } from '../../themes';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 40 * ratioWidth,
  },
  subContainer: {
    alignItems: 'center',
    marginTop: 60 * ratioHeight,
  },
  header: {
    fontSize: fonts.size.font20,
    fontFamily: fonts.type.montserratBold,
    color: colors.black,
    marginTop: 10 * ratioHeight,
    marginBottom: 15 * ratioHeight,
  },
  forgotContainer: { alignItems: 'flex-end' },
  forgot: {
    fontSize: fonts.size.font12,
    fontFamily: fonts.type.montserratRegular,
    color: colors.appDark,
    marginTop: 5 * ratioHeight,
  },
});

export default styles;
