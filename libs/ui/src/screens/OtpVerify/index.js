/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import Container from '../../component/container';
import ButtonPrimary from '../../helper/buttons/buttonPrimary';
import OtpField from '../../helper/inputs/otpField';
import styles from './styles';
import { verifyOtp } from '../../../../store/src/common/auth/action';
import { Text } from 'react-native';

const OtpVerify = (props) => {
  const [otp, setOtp] = useState();
  const dispatch = useDispatch();
  const [error, setError] = useState('');

  const onVerify = () => {
    console.log('otp', otp);
    if (otp && otp.length === 4 && props.type === 'register') {
      dispatch(verifyOtp({ phoneNo: props.value, otp }));
      setError('');
    } else {
      setError('Please fill otp!');
    }
    // navigation.navigate('ResetPassword');
  };

  return (
    <Container
      showHeader
      showBack
      label="OTP Verification"
      extraStyles={styles.container}
    >
      <OtpField
        fieldName="Verify"
        value={otp}
        onCodeFilled={(c) => setOtp(c)}
        fieldStyle={styles.fieldStyle}
      />
      <Text style={styles.errorStyle}>{error.length ? error : ''}</Text>
      <ButtonPrimary
        label="Verify"
        onPress={() => onVerify()}
        extraStyle={styles.buttonStyle}
      />
    </Container>
  );
};

export default OtpVerify;
