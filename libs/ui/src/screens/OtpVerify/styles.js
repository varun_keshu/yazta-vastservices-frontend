import { StyleSheet } from 'react-native';
import { colors, fonts, ratioWidth, ratioHeight } from '../../themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
    paddingHorizontal: 20 * ratioHeight,
  },
  inputStyle: {
    fontSize: fonts.size.font12,
  },
  fieldStyle: {
    fontSize: fonts.size.font18,
    paddingVertical: 15 * ratioHeight,
  },
  buttonStyle: {
    marginTop: 80 * ratioHeight,
  },
  errorStyle: {
    fontSize: fonts.size.font12,
    color: colors.red,
    fontFamily: fonts.type.montserratMedium,
    marginLeft: ratioWidth * 2,
  },
});

export default styles;
