/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import React, { useState } from 'react';
import Container from '../../component/container';
import ButtonPrimary from '../../helper/buttons/buttonPrimary';
import InputField from '../../helper/inputs/inputField';
import styles from './styles';
import { AlertMessage } from '../../utils/alerts';
import { forgetPassword } from '../../../../store/src/common/auth/action';
import { useDispatch } from 'react-redux';

const ForgotPassword = () => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState();
  const [error, setError] = useState('');

  const onNavigate = () => {
    dispatch(forgetPassword({ email: email }));
  };

  const onVerify = () => {
    if (!email) {
      setError('This field is required!');
      return;
    }
    setError('');
    AlertMessage('Check your email for verification instructions.', onNavigate);
  };

  return (
    <Container
      showHeader
      showBack
      label="Forgot Password"
      extraStyles={styles.container}
    >
      <InputField
        placeholder="Please enter your email"
        fieldName="Verify Email"
        onChange={(value) => setEmail(value)}
        inputExtraStyle={styles.inputStyle}
        fieldStyle={styles.fieldStyle}
        error={error}
      />
      <ButtonPrimary
        label="Verify"
        onPress={() => onVerify()}
        extraStyle={styles.buttonStyle}
      />
    </Container>
  );
};

export default ForgotPassword;
