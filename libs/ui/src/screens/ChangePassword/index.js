/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import React, { useState } from 'react';
import Container from '../../component/container';
import ButtonPrimary from '../../helper/buttons/buttonPrimary';
import InputField from '../../helper/inputs/inputField';
import styles from './styles';
import validationMessages from '../../utils/validationMessages';

import { changePassword } from '../../../../store/src/common/auth/action';
import { useDispatch } from 'react-redux';
import { useValidation } from 'react-native-form-validator';

const ChangePassword = () => {
  const dispatch = useDispatch();
  const [code, setCode] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setConfirmPassword] = useState('');
  const { validate, isFieldInError, getErrorsInField, getErrorMessages } =
    useValidation({
      state: { code, password, passwordConfirmation },
      messages: validationMessages,
    });

  const onVerify = () => {
    console.log('in verify');
    validate({
      code: { required: true },
      password: {
        required: true,
        minlength: 8,
        maxlength: 17,
        hasNumber: true,
        hasUpperCase: true,
        hasLowerCase: true,
        hasSpecialCharacter: true,
      },
      passwordConfirmation: {
        required: true,
        hasSpecialCharacter: true,
        equalPassword: password,
      },
    });
    if (
      getErrorMessages().length === 0 &&
      code &&
      password &&
      passwordConfirmation
    ) {
      dispatch(
        changePassword({
          code: code,
          password: password,
          passwordConfirmation: passwordConfirmation,
        })
      );
      setPassword('');
      setConfirmPassword('');
      setCode('');
    }
  };

  return (
    <Container
      showHeader
      showBack
      label="Reset Password"
      extraStyles={styles.container}
    >
      <InputField
        placeholder="New Password"
        fieldName="Please reset your password"
        onChange={(value) => setPassword(value)}
        inputExtraStyle={styles.inputStyle}
        fieldStyle={styles.fieldStyle}
        secureTextEntry
        value={password}
        error={isFieldInError('password') && getErrorsInField('password')[0]}
      />
      <InputField
        placeholder="Confirm Password"
        onChange={(value) => setConfirmPassword(value)}
        error={
          isFieldInError('passwordConfirmation') &&
          getErrorsInField('passwordConfirmation')[0]
        }
        inputExtraStyle={styles.inputStyle}
        fieldStyle={styles.fieldStyle}
        hideField={true}
        value={passwordConfirmation}
        secureTextEntry
      />
      <InputField
        placeholder="Code"
        onChange={(value) => setCode(value)}
        error={isFieldInError('code') && getErrorsInField('code')}
        inputExtraStyle={styles.inputStyle}
        fieldStyle={styles.fieldStyle}
        value={code}
        hideField={true}
      />
      <ButtonPrimary
        label="Submit"
        onPress={onVerify}
        extraStyle={styles.buttonStyle}
      />
    </Container>
  );
};

export default ChangePassword;
