import { StyleSheet } from 'react-native';
import { colors, fonts, ratioWidth, ratioHeight } from '../../themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
    paddingHorizontal: 20 * ratioHeight,
  },
  inputStyle: {
    fontSize: fonts.size.font12,
  },
  fieldStyle: {
    fontSize: fonts.size.font16,
    marginVertical: 10 * ratioHeight,
  },
  buttonStyle: {
    marginTop: 80 * ratioHeight,
  },
});

export default styles;
