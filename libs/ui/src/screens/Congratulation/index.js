import React from 'react';
import { Text, Image } from 'react-native';
import Container from '../../component/container';
import { images } from '../../themes';
import styles from './styles';

const Congratulation = () => {
  return (
    <Container
      scrollEnabled
      showHeader
      label="Profile Completed"
      showBack
      showMenu
      extraStyles={styles.container}
      containerStyle={styles.containerStyle}
    >
      <Image source={images.congrats} style={styles.imageStyle} />
      <Text style={styles.title}>
        Congratulations, your profile is completed!
      </Text>
    </Container>
  );
};

export default Congratulation;
