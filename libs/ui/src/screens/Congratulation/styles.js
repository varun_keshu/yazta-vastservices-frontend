import { StyleSheet } from 'react-native';
import { colors, fonts, ratioHeight, ratioWidth } from '../../themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.appSecondary,
    paddingVertical: 12 * ratioHeight,
    paddingHorizontal: 20 * ratioHeight,
  },
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10 * ratioHeight,
  },
  imageStyle: {
    height: 150 * ratioHeight,
    width: 150 * ratioWidth,
    resizeMode: 'contain',
  },
  title: {
    color: colors.appDark,
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font18,
    marginTop: 15 * ratioHeight,
    textAlign: 'center',
    lineHeight: 25 * ratioHeight,
  },
});

export default styles;
