import { StyleSheet } from 'react-native';
import { colors, fonts, ratioWidth, ratioHeight } from '../../../themes';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 0.3,
    borderColor: colors.appDark,
    paddingVertical: ratioWidth * 3,
    marginBottom: ratioHeight * 5,
  },
  heading: {
    marginTop: 20 * ratioHeight,
  },
  fieldName: {
    fontSize: fonts.size.font13,
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratMedium,
    marginTop: 5 * ratioHeight,
  },
  fieldContent: {
    fontSize: fonts.size.font11,
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratMedium,
    marginTop: 5 * ratioHeight,
  },
  underlineStyleBase: {
    backgroundColor: colors.white,
    borderRadius: 6,
    borderColor: colors.borderColor,
    borderWidth: 0.5,
    color: 'black'
  },
  otpContainer: { width: '80%', height: 110 * ratioHeight },
});

export default styles;
