import React from 'react';
import { View, Text, TextInput } from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import styles from './styles';

const OtpField = (props) => {
  const { fieldName, fieldStyle } = props;
  return (
    <View>
      <View style={styles.heading}>
        <Text style={[styles.fieldName, fieldStyle]}>{fieldName}</Text>
        <Text style={styles.fieldName}>
          Please enter the OTP (One Time Password)
        </Text>
        <Text style={styles.fieldContent}>
          Sent to your registered phone number
        </Text>
        <OTPInputView
          style={styles.otpContainer}
          pinCount={4}
          autoFocusOnLoad
          codeInputFieldStyle={styles.underlineStyleBase}
          code={props.value}
          editable
          onCodeChanged={(code) => {
            props.onCodeFilled(code);
            console.log(`Code is ${code}, you are good to go!`);
          }}
        />
        {/* <Text style={styles.fieldName}>Resend verification code</Text> */}
      </View>
    </View>
  );
};

export default OtpField;
