import { StyleSheet } from 'react-native';
import { colors, fonts, ratioWidth, ratioHeight } from '../../themes';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 0.3,
    borderColor: colors.appDark,
    paddingVertical: ratioWidth * 3,
    marginBottom: ratioHeight * 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  heading: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  fieldName: {
    fontSize: fonts.size.font14,
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratMedium,
    marginTop: 10 * ratioHeight,
  },
  astric: {
    color: colors.red,
    fontSize: fonts.size.font14,
    marginLeft: 3 * ratioWidth,
    marginTop: 6 * ratioHeight,
  },
  inputStyle: {
    color: colors.textColor,
    fontSize: fonts.size.font15,
    fontFamily: fonts.type.montserratRegular,
    paddingVertical: ratioHeight * 5,
    padding: 0,
    width: '90%',
  },
  errorStyle: {
    fontSize: fonts.size.font12,
    color: colors.red,
    fontFamily: fonts.type.montserratMedium,
    marginLeft: ratioWidth * 2,
  },
  eyeIcon: { marginTop: 8 * ratioHeight },
});

export default styles;
