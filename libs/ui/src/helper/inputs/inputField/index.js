import React from 'react';
import { View, Text, TextInput } from 'react-native';
import { colors } from '../../themes';
import styles from './styles';
import Icon from '../../../helper/icon/icons';

const InputField = (props) => {
  const {
    placeholder,
    placeholderColor,
    onChange,
    value,
    fieldName,
    multiline,
    numLines,
    error,
    extraStyle,
    editable,
    astric,
    inputExtraStyle,
    fieldStyle,
    hideField,
    keyboardType,
    secureTextEntry,
    showEyeIcon,
    onEyeIconClick,
    showEyeWithLineIcon,
    onShowEyeWithLineIcon,
  } = props;
  return (
    <View>
      {!hideField && (
        <View style={styles.heading}>
          <Text style={[styles.fieldName, fieldStyle]}>{fieldName}</Text>
          {astric && <Text style={styles.astric}>*</Text>}
        </View>
      )}
      <View style={[styles.container, extraStyle]}>
        <TextInput
          style={[styles.inputStyle, inputExtraStyle]}
          placeholder={placeholder}
          value={value}
          multiline={multiline ? multiline : false}
          numberOfLines={numLines}
          placeholderTextColor={
            placeholderColor ? placeholderColor : colors.textGrey
          }
          secureTextEntry={secureTextEntry ? true : false}
          onChangeText={onChange}
          editable={editable}
          keyboardType={keyboardType ? keyboardType : 'default'}
          autoComplete="off"
          textAlignVertical={'top'}
        />
        {showEyeWithLineIcon && (
          <View style={styles.eyeIcon}>
            <Icon
              type="Entypo"
              name="eye"
              size={20}
              color="black"
              onPress={onEyeIconClick}
            />
          </View>
        )}
        {showEyeIcon && (
          <Icon
            type="Entypo"
            name="eye-with-line"
            size={20}
            color="black"
            onPress={onShowEyeWithLineIcon}
          />
        )}
      </View>
      {error ? <Text style={styles.errorStyle}>{error}</Text> : <></>}
    </View>
  );
};

export default InputField;
