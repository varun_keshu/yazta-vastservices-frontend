import { StyleSheet } from 'react-native';
import { colors, ratioHeight, fonts, ratioWidth } from '../../themes';

const styles = StyleSheet.create({
  container: {
    padding: 20 * ratioHeight,
  },
  cardContainer: {
    backgroundColor: colors.detailBackground,
    borderRadius: 5,
    borderColor: colors.pagination,
    borderWidth: 0.5,
    marginVertical: 4 * ratioHeight,
    paddingHorizontal: 16 * ratioWidth,
    paddingVertical: 16 * ratioHeight,
  },
  titleBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  iconStyle: {
    backgroundColor: colors.textColor,
    height: 20 * ratioHeight,
    width: 20 * ratioHeight,
    borderRadius: 50,
  },
  titleTxt: {
    color: colors.pagination,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font16,
  },
});

export default styles;
