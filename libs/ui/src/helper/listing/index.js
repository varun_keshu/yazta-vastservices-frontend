import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import Container from '../../component/container';
import styles from './styles';
import Icon from '../icon/icons';
import { colors } from '../../themes';
import { useNavigation } from '@react-navigation/core';

const Listing = (props) => {
  const { label, listData, iconName, iconType } = props;
  const navigation = useNavigation();

  const onNavigate = (route) => {
  console.log('route :', route);
    navigation.navigate(route);
  };

  const ItemView = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.cardContainer}
        onPress={() => onNavigate(item.route)}
      >
        <View style={styles.titleBox}>
          <Text style={styles.titleTxt}>{item.label}</Text>
          <Icon
            name={iconName}
            type={iconType}
            size={15}
            color={colors.white}
            extraStyles={styles.iconStyle}
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Container
      scrollEnabled
      extraStyles={styles.container}
      showHeader
      showBack
      showMenu
      label={label}
    >
      <FlatList
        data={listData || []}
        nestedScrollEnabled
        renderItem={ItemView}
        keyExtractor={(item) => item.id}
        contentContainerStyle={styles.contentContainerStyle}
      />
    </Container>
  );
};

export default Listing;
