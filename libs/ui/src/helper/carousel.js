import React, { useState, useRef } from 'react';
import { StyleSheet } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Container from '../component/container';
import { colors, metrics, ratioHeight, ratioWidth } from '../themes';
import SkipButton from './buttons/skipButton';
import SliderCard from './cards/sliderCard';

const CarouselSlider = (props) => {
  const { skip, data } = props;

  const [activeSlide, setActiveSlide] = useState(0);
  let carouselRef = useRef();

  const _renderItem = ({ item, index }) => {
    return <SliderCard item={item} />;
  };

  const PaginationComponent = () => {
    return (
      <Pagination
        dotsLength={data?.length}
        activeDotIndex={activeSlide}
        dotStyle={styles.dotsStyles}
        inactiveDotStyle={styles.dotsInActiveStyles}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
        tappableDots={true}
        carouselRef={carouselRef}
        // renderDots={(index) => handleDots(index)}
      />
    );
  };

  return (
    <>
      <Container extraStyles={styles.container}>
        <SkipButton skip={skip} />
        <Carousel
          ref={carouselRef}
          data={data || []}
          firstItem={activeSlide}
          renderItem={_renderItem}
          onSnapToItem={(index) => setActiveSlide(index)}
          sliderWidth={metrics.screenWidth}
          itemWidth={metrics.screenWidth - 20}
          autoplay={true}
          autoplayDelay={3000}
          autoplayInterval={3000}
        />
        <PaginationComponent />
      </Container>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 12 * ratioWidth,
    backgroundColor: colors.appSecondary,
  },
  dotsInActiveStyles: {
    width: 20 * ratioHeight,
    height: 20 * ratioHeight,
    borderRadius: 16 * ratioHeight,
    borderColor: colors.pagination,
    borderWidth: 2.5,
    backgroundColor: colors.appSecondary,
  },
  dotsStyles: {
    width: 14 * ratioHeight,
    height: 14 * ratioHeight,
    borderRadius: 16 * ratioHeight,
    backgroundColor: colors.appDark,
  },
});

export default CarouselSlider;
