import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import Icon from '../../icon/icons';
import styles from './styles';
import { colors } from '../../themes';

const ButtonPrimary = (props) => {
  const {
    label,
    extraStyle,
    onPress,
    labelStyle,
    showIcon,
    iconType,
    iconName,
    iconSize,
    showRightIcon,
    iconColor
  } = props;

  return (
    <TouchableOpacity style={[styles.signUp, extraStyle]} onPress={onPress}>
      {showRightIcon && (
        <Icon
          type={iconType}
          name={iconName}
          size={iconSize ? iconSize : 20}
          color={iconColor?iconColor:colors.white}
          extraStyles={styles.iconRightStyle}
        />
      )}
      <Text style={[styles.label, labelStyle]}>{label}</Text>
      {showIcon && (
        <Icon
          type={iconType}
          name={iconName}
          size={iconSize ? iconSize : 20}
          color={iconColor?iconColor:colors.white}
          extraStyles={styles.iconStyle}
        />
      )}
    </TouchableOpacity>
  );
};

export default ButtonPrimary;
