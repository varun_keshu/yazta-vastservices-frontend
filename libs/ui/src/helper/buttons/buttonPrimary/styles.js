import { StyleSheet } from 'react-native';
import { colors, fonts, ratioWidth, ratioHeight } from '../../themes';

const styles = StyleSheet.create({
  signUp: {
    backgroundColor: colors.textColor,
    paddingHorizontal: 100 * ratioWidth,
    paddingVertical: 8 * ratioHeight,
    borderRadius: 4,
    marginTop: 25 * ratioHeight,
    alignItems: 'center',
  },
  label: {
    color: colors.white,
    fontFamily: fonts.type.montserratSemiBold,
    fontSize: fonts.size.font15,
  },
  iconStyle: {
    marginLeft: 8 * ratioWidth,
  },
  iconRightStyle: {
    marginHorizontal: 10 * ratioWidth,
    marginTop: 3 * ratioWidth,
  },
});

export default styles;
