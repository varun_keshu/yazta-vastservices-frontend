import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { colors, fonts, ratioWidth } from '../../themes';
import { useSelector } from 'react-redux';

const SkipButton = (props) => {
  const { skip } = props;
  const navigation = useNavigation();
  const userToken = useSelector((state) => state.auth.userToken);

  const onSkip = () => {
    if (userToken) {
      navigation.navigate('HomeStack');
    } else if (skip) {
      navigation.navigate(`${skip}`);
    } else {
      navigation.navigate('Testimonial');
    }
  };

  return (
    <TouchableOpacity style={styles.container} onPress={onSkip}>
      <Text style={styles.skipButton}>Skip</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: { alignItems: 'flex-end', padding: 10 * ratioWidth },
  skipButton: {
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font16,
    color: colors.appDark,
  },
});

export default SkipButton;
