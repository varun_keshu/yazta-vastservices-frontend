import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from '../icon/icons';
import { colors, fonts, ratioHeight, ratioWidth } from '../../themes';
import styles from './styles';

const AccordianIn = (props) => {
  const { label, check, onPress } = props;

  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          paddingVertical: 10 * ratioHeight,
        }}
      >
        <View style={{ flexDirection: 'row' }}>
          <Icon
            type="MaterialCommunityIcons"
            name="file-multiple-outline"
            size={15}
            color={colors.pagination}
            extraStyles={styles.iconExtra}
          />
          <Text
            style={{
              marginLeft: 15 * ratioWidth,
              color: colors.textColor,
              fontSize: fonts.size.font13,
              fontFamily: fonts.type.montserratMedium,
            }}
          >
            {label}
          </Text>
        </View>
        <Icon
          type="MaterialCommunityIcons"
          name="delete"
          size={16}
          color={check ? colors.textColor : colors.black}
          extraStyles={styles.iconExtra}
        />
      </View>
    </TouchableOpacity>
  );
};

export default AccordianIn;
