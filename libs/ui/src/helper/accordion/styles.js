import { StyleSheet } from 'react-native';
import {
  colors,
  fonts,
  ratioWidth,
  ratioHeight,
} from '../../themes';

const styles = StyleSheet.create({
  container: { width: '100%', flexDirection: 'row' },
  uploadContainer: {
    borderColor: colors.pagination,
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: colors.white,
    paddingVertical: 25 * ratioWidth,
    alignItems: 'center',
    width: '96%',
    zIndex: 1,
    marginTop: 12 * ratioHeight,
  },
  uploadText: {
    color: colors.textColor,
    fontSize: fonts.size.font16,
    fontFamily: fonts.type.montserratBold,
  },
  iconContainer: { zIndex: 2 },
  iconExtra: {
    marginLeft: -12 * ratioWidth,
    marginTop: 32 * ratioHeight,
    backgroundColor: colors.white,
  },
});

export default styles;
