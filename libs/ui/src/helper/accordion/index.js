import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from '../icon/icons';
import { colors } from '../../themes';
import styles from './styles';

const Accordion = (props) => {
  const { label, check, onPress } = props;
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.uploadContainer}>
        <Text
          style={[
            styles.uploadText,
            { color: check ? colors.textColor : colors.lightGrey },
          ]}
        >
          {label}
        </Text>
      </View>
      {check && (
        <View style={styles.iconContainer}>
          <Icon
            type="FontAwesome"
            name="check-circle"
            size={26}
            color={colors.textColor}
            extraStyles={styles.iconExtra}
          />
        </View>
      )}
    </TouchableOpacity>
  );
};

export default Accordion;
