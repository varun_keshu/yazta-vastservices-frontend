import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { colors, fonts, ratioHeight, ratioWidth } from '../../themes';

const SliderCard = (props) => {
  const { item, extraStyles } = props;
  const { label, image, content } = item;

  return (
    <>
      <View style={[styles.buttonContainer, extraStyles]}>
        {image}
        <Text style={styles.label}>{label}</Text>
        <Text style={styles.content}>{content}</Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 12 * ratioWidth,
  },
  label: {
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font16,
    color: colors.appDark,
    textAlign: 'center',
  },
  content: {
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font13,
    color: colors.appDark,
    textAlign: 'center',
    lineHeight: 20,
    marginHorizontal: 20 * ratioWidth,
  },
});

export default SliderCard;
