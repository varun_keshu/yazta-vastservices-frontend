import React, { useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

const TripleToggle = (props) => {
  const {
    label1,
    label2,
    label3,
    onPress,
    extraContainerStyle,
    extraTextStyle,
  } = props;

  const [selectedLabel, setSelectedLabel] = useState('one');

  const onClick = (current) => {
    setSelectedLabel(current);
    onPress(current)
  };

  return (
    <View style={[styles.container, extraContainerStyle]}>
      <TouchableOpacity
        onPress={() => onClick('one')}
        style={
          selectedLabel === 'one' ? styles.detailContainer : styles.jobContainer
        }
      >
        <Text style={selectedLabel === 'one' ? styles.detail : styles.job}>
          {label1}
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => onClick('two')}
        style={
          selectedLabel === 'two' ? styles.detailContainer : styles.jobContainer
        }
      >
        <Text style={selectedLabel === 'two' ? styles.detail : styles.job}>
          {label2}
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={
          selectedLabel === 'three'
            ? styles.detailContainer
            : styles.jobContainer
        }
        onPress={() => onClick('three')}
      >
        <Text style={selectedLabel === 'three' ? styles.detail : styles.job}>
          {label3}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default TripleToggle;
