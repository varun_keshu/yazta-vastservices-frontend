import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { ratioWidth } from '../../themes';
import styles from './styles';

const Toogle = (props) => {
  const {
    label1,
    label2,
    onPress,
    profile,
    extraJobContainerStyle,
    extraContainerStyle,
    extraDetailContainerStyle,
    extraTextStyle,
  } = props;

  return (
    <View style={[styles.container, extraContainerStyle]}>
      {profile ? (
        <>
          <TouchableOpacity style={styles.detailContainer}>
            <Text style={styles.detail}>{label1}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={onPress} style={styles.jobContainer}>
            <Text style={styles.job}>{label2}</Text>
          </TouchableOpacity>
        </>
      ) : (
        <>
          <TouchableOpacity
            onPress={onPress}
            style={[styles.jobContainer, extraJobContainerStyle]}
          >
            <Text style={[styles.job, extraTextStyle]}>{label1}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.detailContainer,
              {
                paddingHorizontal: 16.8 * ratioWidth,
              },
              extraDetailContainerStyle,
            ]}
          >
            <Text style={[styles.detail, extraTextStyle]}>{label2}</Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};

export default Toogle;
