import { StyleSheet } from 'react-native';
import { colors, fonts, ratioWidth, ratioHeight } from '../../themes';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: colors.appDark,
    paddingLeft: 0.2 * ratioWidth,
    paddingRight: 12 * ratioWidth,
    paddingVertical: 0.2 * ratioHeight,
    borderRadius: 40,
    alignItems: 'center',
    elevation: 5,
    marginTop: 8 * ratioHeight,
    marginBottom: 20 * ratioHeight,
  },
  detailContainer: {
    backgroundColor: colors.faded,
    paddingHorizontal: 8 * ratioWidth,
    paddingVertical: 8 * ratioHeight,
    borderRadius: 20,
    elevation: 5,
  },
  jobContainer: {
    backgroundColor: colors.appDark,
    paddingHorizontal: 8 * ratioWidth,
    paddingVertical: 8 * ratioHeight,
    borderRadius: 20,
  },
  detail: {
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font12,
    color: colors.textPrimary,
    paddingHorizontal: 10 * ratioWidth,
  },
  job: {
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font12,
    color: colors.white,
    paddingHorizontal: 8 * ratioWidth,
  },
  label3: {
    fontFamily: fonts.type.montserratBold,
    fontSize: fonts.size.font12,
    color: colors.white,
    paddingHorizontal: 10 * ratioWidth,
  },
});

export default styles;
