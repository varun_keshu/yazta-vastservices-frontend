export const getUploadBody = (profileAvatar) => {
  const formData = new FormData();
  formData.append('files', {
    uri: profileAvatar[0].uri,
    type: profileAvatar[0].type,
    name: profileAvatar[0].name,
  });
  formData.append('field', 'image');
  formData.append('path', 'image');
  return formData;
};

export const getUploadFileBody = (file) => {
  const formData = new FormData();
  formData.append('files', {
    uri: file[0].uri,
    type: file[0].type,
    name: file[0].name,
  });
  formData.append('field', 'document');
  formData.append('path', 'document');
  return formData;
};
