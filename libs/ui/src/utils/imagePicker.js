import { launchCamera, launchImageLibrary } from 'react-native-image-picker';

const imagePickerOptions = {
  mediaType: 'photo',
  quality: 1,
};

export const launchCam = async () => {
  launchCamera(imagePickerOptions, callback);
  const result = await launchCamera(imagePickerOptions);
};

export const launchFile = async (callback) => {
  launchImageLibrary(imagePickerOptions, callback);
  const result = await launchImageLibrary(imagePickerOptions);
  if (result.didCancel) {
    console.log('image error :');
  } else if (result.error || result.errorCode) {
    console.log('image error :');
  } else {
    callback(result.assets[0]);
  }
};
