import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { fonts, colors, ratioHeight, ratioWidth } from '../themes';

const Checkbox = (props) => {
  const { label, setCheck, check } = props;

  return (
    <View style={styles.container}>
      <CheckBox
        disabled={false}
        value={check}
        tintColors={{
          true: `${colors.appDark}`,
          false: `${colors.textGrey}`,
        }}
        onValueChange={setCheck}
      />
      <Text style={styles.label}>{label}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  label: {
    marginTop: 6 * ratioHeight,
    color: colors.textPrimary,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font12,
    textAlign: 'center',
    marginVertical: 8 * ratioHeight,
    textTransform: 'capitalize',
  },
  container: {
    flexDirection: 'row',
    marginHorizontal: 15 * ratioWidth,
    marginTop: 5 * ratioHeight,
  },
});

export default Checkbox;
