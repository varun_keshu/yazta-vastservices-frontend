import * as React from 'react';
import { DrawerActions } from '@react-navigation/native';

export const navigationRef = React.createRef();
console.log('navigationRef :', navigationRef);

export function goToScreen(name, params = {}) {
  console.log('name :', name);
  navigationRef.current?.navigate(name, params);
}

export const backPress = () => {
  navigationRef.current?.goBack();
};

export const openDrawer = () => {
  navigationRef.current?.dispatch(DrawerActions.openDrawer());
};
