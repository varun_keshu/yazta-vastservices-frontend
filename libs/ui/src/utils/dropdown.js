import React, { useState } from 'react';
import { StyleSheet, Text } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { colors, fonts, ratioHeight, ratioWidth } from '../themes';

const DropDown = (props) => {
  const { data, onPick, field, extraStyle, fieldName, placeholder } = props;

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);

  const onSelect = (selectData) => {
    console.log('selectData :', selectData);
  };

  onPick(value, field);

  return (
    <>
      {fieldName && <Text style={styles.fieldName}>{fieldName}</Text>}
      <DropDownPicker
        placeholder={placeholder ? placeholder : 'Select'}
        open={open}
        value={value}
        items={data}
        setOpen={setOpen}
        setValue={setValue}
        containerStyle={[styles.containerStyle, extraStyle]}
        style={styles.dropdownStyle}
        labelStyle={styles.labelStyle}
        textStyle={styles.labelStyle}
        listMode="SCROLLVIEW"
        dropDownDirection="BOTTOM"
        dropDownContainerStyle={styles.dropDownContainerStyle}
      />
    </>
  );
};

const styles = StyleSheet.create({
  dropdownStyle: {
    backgroundColor: '#fff',
    borderRadius: 3,
    borderColor: colors.pagination,
    borderWidth: 0.5,
    width: '100%',
  },
  containerStyle: {
    marginLeft: 5 * ratioWidth,
    borderRadius: 3,
    width: '98%',
    borderColor: colors.pagination,
    zIndex: 1,
  },
  labelStyle: {
    color: colors.grey,
    fontFamily: fonts.type.montserratRegular,
    fontSize: fonts.size.font11,
    lineHeight: 16 * ratioHeight,
  },
  dropDownContainerStyle: {
    borderRadius: 3,
    borderColor: colors.pagination,
    borderWidth: 0.5,
  },
  fieldName: {
    fontSize: fonts.size.font12,
    fontFamily: fonts.type.montserratMedium,
    color: colors.pagination,
    marginBottom: 3 * ratioHeight,
    marginTop: 10 * ratioHeight,
  },
});

export default DropDown;
