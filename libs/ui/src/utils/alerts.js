import { Alert } from 'react-native';

export const AlertMessage = (message, okPress) => {
  Alert.alert('', message, [
    {
      text: 'Cancel',
      onPress: () => console.log('Cancel Pressed'),
    },
    { text: 'OK', onPress: okPress },
  ]);
};
