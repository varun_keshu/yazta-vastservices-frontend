import React from 'react';
import { TouchableOpacity, View, Text, Image, StyleSheet } from 'react-native';
import { colors, fonts, ratioHeight, ratioWidth } from '../themes';
import Icon from '../helper/icon/icons';

const DocumentPick = (props) => {
  const { selectFile, profileAvatar } = props;

  return (
    <>
      <TouchableOpacity onPress={() => selectFile()}>
        {profileAvatar ? (
          <Image style={styles.imageStyle} source={profileAvatar} />
        ) : (
          <Icon
            type="Ionicons"
            name="md-person-sharp"
            size={45}
            color={colors.white}
            extraStyles={styles.iconStyle}
          />
        )}
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.label}>Select Image</Text>
          <Icon
            type="AntDesign"
            name="pluscircle"
            size={20}
            color={colors.appDark}
            extraStyles={styles.addStyle}
          />
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  label: {
    color: colors.appDark,
    fontFamily: fonts.type.montserratMedium,
    fontSize: fonts.size.font12,
    marginTop: 8 * ratioHeight,
  },
  imageStyle: {
    borderRadius: 50 * ratioHeight,
    height: 75 * ratioHeight,
    width: 78 * ratioHeight,
    marginLeft: 5 * ratioWidth,
  },
  iconStyle: {
    backgroundColor: colors.pagination,
    paddingHorizontal: 20 * ratioWidth,
    paddingVertical: 18 * ratioWidth,
    borderRadius: 50,
    height: 75 * ratioHeight,
    width: 78 * ratioHeight,
    marginLeft: 5 * ratioWidth,
  },
  addStyle: {
    marginTop: 7 * ratioHeight,
    marginLeft: 5 * ratioHeight,
  },
});

export default DocumentPick;
