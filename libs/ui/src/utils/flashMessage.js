import { showMessage } from 'react-native-flash-message';
import { Alert } from 'react-native';

export const showToast = (type, message, duration = 3000) => {
  showMessage({ type, message, duration });
};

export const AlertMessage = (message) => {
  Alert.alert('', message, [{ text: 'OK' }]);
};
