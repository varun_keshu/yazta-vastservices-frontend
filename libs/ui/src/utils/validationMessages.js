const validationMessages = {
  // English language - Used by default
  en: {
    numbers: 'The field must be a valid number.',
    email: 'The field must be a valid email address.',
    required: 'The field is required.',
    date: 'The field "{0}" must be a valid date ({1}).',
    minlength: 'The field length must be greater than {1}.',
    maxlength: 'The field length must be lower than {1}.',
    equalPassword: 'Passwords are different.',
    hasUpperCase: 'The field must contain a upper case.',
    hasLowerCase: 'The field must contain a lower case.',
    hasNumber: 'The field must contain a number.',
    hasSpecialCharacter: 'The field must contain a special character.',
  },
};

export default validationMessages;
