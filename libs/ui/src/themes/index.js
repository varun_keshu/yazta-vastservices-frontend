import colors from './colors';
import fonts from './fonts';
import metrics from './metrices';
import images from './images';

var heightRef = 627;
var widthRef = 360;

const ratioHeight = metrics.screenHeight / heightRef;
const ratioWidth = metrics.screenWidth / widthRef;

export {
  colors,
  metrics,
  fonts,
  heightRef,
  widthRef,
  ratioHeight,
  ratioWidth,
  images,
};
