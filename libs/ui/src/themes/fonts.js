import Metrics from './metrices';

const size = {
  font6: Metrics.screenWidth * (6 / 365),
  font8: Metrics.screenWidth * (8 / 365),
  font9: Metrics.screenWidth * (9 / 365),
  font10: Metrics.screenWidth * (10 / 365),
  font11: Metrics.screenWidth * 0.029,
  font12: Metrics.screenWidth * (12 / 365),
  font13: Metrics.screenWidth * 0.034,
  font14: Metrics.screenWidth * (14 / 365),
  font15: Metrics.screenWidth * 0.04,
  font16: Metrics.screenWidth * (16 / 365),
  font17: Metrics.screenWidth * 0.045,
  font18: Metrics.screenWidth * (18 / 365),
  font19: Metrics.screenWidth * 0.05,
  font20: Metrics.screenWidth * (20 / 365),
  font21: Metrics.screenWidth * 0.056,
  font23: Metrics.screenWidth * 0.061,
  font25: Metrics.screenWidth * 0.053,
  font27: Metrics.screenWidth * 0.072,
  font28: Metrics.screenWidth * (28 / 365),
  font30: Metrics.screenWidth * (30 / 365),
  font32: Metrics.screenWidth * 0.085,
  font42: Metrics.screenWidth * 0.112,
};

const weight = {
  full: '900',
  semi: '600',
  low: '400',
  bold: 'bold',
  normal: 'normal',
};

const type = {
  montserratMedium: 'Montserrat-Medium',
  montserratRegular: 'Montserrat-Regular',
  montserratSemiBold: 'Montserrat-SemiBold',
  montserratBold: 'Montserrat-Bold',
};

export default {
  size,
  weight,
  type,
};
