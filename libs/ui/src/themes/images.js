const images = {
  person: require('../assets/images/png/person.png'),
  avatar: require('../assets/images/png/avatar.png'),
  cooking: require('../assets/images/png/cooking.png'),
  babysit: require('../assets/images/png/babysit.png'),
  cleaning: require('../assets/images/png/cleaning.png'),
  service: require('../assets/images/png/service.png'),
  congrats: require('../assets/images/png/congrats.png'),
};

export default images;
