import colors from './colors';

const constants = {
  gradiantColor: [colors.appPrimary, colors.appTertiary, colors.appTertiary],
};

export default constants;
