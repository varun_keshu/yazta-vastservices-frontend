import Walk1 from '../assets/images/svgs/walk1.svg';
import Walk2 from '../assets/images/svgs/walk2.svg';
import Walk3 from '../assets/images/svgs/walk3.svg';
import Logo from '../assets/images/svgs/logo.svg';
import AppLogo from '../assets/images/svgs/appLogo.svg';

export { Walk1, Walk2, Walk3, Logo, AppLogo };
